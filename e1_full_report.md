# PostgreSQL Checkup. Project: 'postila_ru'. Database: 'postila_ru'
## Epoch number: '1'
NOTICE: while most reports describe the “current database”, some of them may contain cluster-wide information describing all databases in the cluster.

Last modified at:  2019-02-12 17:38:56 +0300


<a name="postgres-checkup_top"></a>
### Table of contents ###

[A001 System information](#postgres-checkup_A001)  
[A002 Version information](#postgres-checkup_A002)  
[A003 Postgres settings](#postgres-checkup_A003)  
[A004 Cluster information](#postgres-checkup_A004)  
[A005 Extensions](#postgres-checkup_A005)  
[A006 Postgres setting deviations](#postgres-checkup_A006)  
[A007 Altered settings](#postgres-checkup_A007)  
[A008 Disk usage and file system type](#postgres-checkup_A008)  
[D002 Useful Linux tools](#postgres-checkup_D002)  
[D004 pg_stat_statements and kcache settings](#postgres-checkup_D004)  
[F001 Autovacuum: Current settings](#postgres-checkup_F001)  
[F002 Autovacuum: Transaction wraparound check](#postgres-checkup_F002)  
[F003 Autovacuum: Dead tuples](#postgres-checkup_F003)  
[F004 Autovacuum: Heap bloat](#postgres-checkup_F004)  
[F005 Autovacuum: Index bloat](#postgres-checkup_F005)  
[F008 Autovacuum: Resource usage](#postgres-checkup_F008)  
[G001 Memory-related settings](#postgres-checkup_G001)  
[G002 Connections and current activity](#postgres-checkup_G002)  
[G003 Timeouts, locks, deadlocks](#postgres-checkup_G003)  
[H001 Invalid indexes](#postgres-checkup_H001)  
[H002 Unused/Rarely Used Indexes](#postgres-checkup_H002)  
[H003 Non indexed foreign keys (or with bad indexes)](#postgres-checkup_H003)  
[K001 Globally aggregated query metrics](#postgres-checkup_K001)  
[K002 Workload type ("first word" analysis)](#postgres-checkup_K002)  
[K003 Top-50 queries by total_time](#postgres-checkup_K003)  
[L001 Table sizes](#postgres-checkup_L001)  

---
<a name="postgres-checkup_A001"></a>
[Table of contents](#postgres-checkup_top)
# A001 System information #

## Observations ##


### Master (`db2`) ###

**System**

```
Linux db2 4.15.0-39-generic #42~16.04.1-Ubuntu SMP Wed Oct 24 17:09:54 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux
```


**CPU**

```
Architecture:          x86_64
CPU op-mode(s):        32-bit, 64-bit
Byte Order:            Little Endian
CPU(s):                12
On-line CPU(s) list:   0-11
Thread(s) per core:    2
Core(s) per socket:    6
Socket(s):             1
NUMA node(s):          1
Vendor ID:             GenuineIntel
CPU family:            6
Model:                 158
Model name:            Intel(R) Core(TM) i7-8700 CPU @ 3.20GHz
Stepping:              10
CPU MHz:               809.119
CPU max MHz:           4600.0000
CPU min MHz:           800.0000
BogoMIPS:              6384.00
Virtualization:        VT-x
L1d cache:             32K
L1i cache:             32K
L2 cache:              256K
L3 cache:              12288K
NUMA node0 CPU(s):     0-11
Flags:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx smx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp flush_l1d
```


**Memory**

```
MemTotal:       65888240 kB
MemFree:          422836 kB
MemAvailable:   38669876 kB
Buffers:           35928 kB
Cached:         55498084 kB
SwapCached:            0 kB
Active:         40955804 kB
Inactive:       16167556 kB
Active(anon):   18235696 kB
Inactive(anon):  1168304 kB
Active(file):   22720108 kB
Inactive(file): 14999252 kB
Unevictable:           0 kB
Mlocked:               0 kB
SwapTotal:       2095100 kB
SwapFree:        2095100 kB
Dirty:              1180 kB
Writeback:             0 kB
AnonPages:       1589772 kB
Mapped:         17249808 kB
Shmem:          17814228 kB
Slab:            1333332 kB
SReclaimable:    1258216 kB
SUnreclaim:        75116 kB
KernelStack:        6896 kB
PageTables:      1238648 kB
NFS_Unstable:          0 kB
Bounce:                0 kB
WritebackTmp:          0 kB
CommitLimit:    35039220 kB
Committed_AS:   20300148 kB
VmallocTotal:   34359738367 kB
VmallocUsed:           0 kB
VmallocChunk:          0 kB
HardwareCorrupted:     0 kB
AnonHugePages:         0 kB
ShmemHugePages:        0 kB
ShmemPmdMapped:        0 kB
CmaTotal:              0 kB
CmaFree:               0 kB
HugePages_Total:       0
HugePages_Free:        0
HugePages_Rsvd:        0
HugePages_Surp:        0
Hugepagesize:       2048 kB
DirectMap4k:    24430984 kB
DirectMap2M:    38402048 kB
DirectMap1G:     4194304 kB
```


**Disk**

```
Filesystem            Type      Size  Used Avail Use% Mounted on
udev                  devtmpfs   32G     0   32G   0% /dev
tmpfs                 tmpfs     6.3G  613M  5.7G  10% /run
/dev/md2              ext4       63G  5.1G   55G   9% /
tmpfs                 tmpfs      32G  3.1M   32G   1% /dev/shm
tmpfs                 tmpfs     5.0M     0  5.0M   0% /run/lock
tmpfs                 tmpfs      32G     0   32G   0% /sys/fs/cgroup
/dev/md1              ext3      488M  339M  124M  74% /boot
/dev/mapper/cachedev0 ext4      5.4T  1.1T  4.0T  22% /var/lib/postgresql
tmpfs                 tmpfs     6.3G     0  6.3G   0% /run/user/1001
tmpfs                 tmpfs     6.3G     0  6.3G   0% /run/user/1000
tmpfs                 tmpfs     6.3G     0  6.3G   0% /run/user/1003
```


**Virtualization**

```
   Static hostname: db2
         Icon name: computer-desktop
           Chassis: desktop
        Machine ID: 7ac6a537ee3941fe9cde4113ee161be6
           Boot ID: 1a5945054c4a4030b6591515ea6bcfa4
  Operating System: Ubuntu 16.04.5 LTS
            Kernel: Linux 4.15.0-39-generic
      Architecture: x86-64
```





### Replica servers: ###
    
#### Replica (`db3`) ####
        

**System**

```
Linux db3 4.15.0-39-generic #42~16.04.1-Ubuntu SMP Wed Oct 24 17:09:54 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux
```


**CPU**

```
Architecture:          x86_64
CPU op-mode(s):        32-bit, 64-bit
Byte Order:            Little Endian
CPU(s):                12
On-line CPU(s) list:   0-11
Thread(s) per core:    2
Core(s) per socket:    6
Socket(s):             1
NUMA node(s):          1
Vendor ID:             GenuineIntel
CPU family:            6
Model:                 158
Model name:            Intel(R) Core(TM) i7-8700 CPU @ 3.20GHz
Stepping:              10
CPU MHz:               4300.009
CPU max MHz:           4600.0000
CPU min MHz:           800.0000
BogoMIPS:              6384.00
Virtualization:        VT-x
L1d cache:             32K
L1i cache:             32K
L2 cache:              256K
L3 cache:              12288K
NUMA node0 CPU(s):     0-11
Flags:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx smx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp flush_l1d
```


**Memory**

```
MemTotal:       65888260 kB
MemFree:          518544 kB
MemAvailable:   37130232 kB
Buffers:           24112 kB
Cached:         54090308 kB
SwapCached:          552 kB
Active:         47455796 kB
Inactive:       10340752 kB
Active(anon):   19969620 kB
Inactive(anon):  1541180 kB
Active(file):   27486176 kB
Inactive(file):  8799572 kB
Unevictable:           0 kB
Mlocked:               0 kB
SwapTotal:       2095100 kB
SwapFree:        2093552 kB
Dirty:               844 kB
Writeback:             0 kB
AnonPages:       3681316 kB
Mapped:         17241316 kB
Shmem:          17827808 kB
Slab:            1123656 kB
SReclaimable:    1056476 kB
SUnreclaim:        67180 kB
KernelStack:        8704 kB
PageTables:       720144 kB
NFS_Unstable:          0 kB
Bounce:                0 kB
WritebackTmp:          0 kB
CommitLimit:    35039228 kB
Committed_AS:   22322724 kB
VmallocTotal:   34359738367 kB
VmallocUsed:           0 kB
VmallocChunk:          0 kB
HardwareCorrupted:     0 kB
AnonHugePages:         0 kB
ShmemHugePages:        0 kB
ShmemPmdMapped:        0 kB
CmaTotal:              0 kB
CmaFree:               0 kB
HugePages_Total:       0
HugePages_Free:        0
HugePages_Rsvd:        0
HugePages_Surp:        0
Hugepagesize:       2048 kB
DirectMap4k:     1126812 kB
DirectMap2M:    36540416 kB
DirectMap1G:    29360128 kB
```


**Disk**

```
Filesystem            Type      Size  Used Avail Use% Mounted on
udev                  devtmpfs   32G     0   32G   0% /dev
tmpfs                 tmpfs     6.3G  629M  5.7G  10% /run
/dev/md2              ext4       63G  4.5G   56G   8% /
tmpfs                 tmpfs      32G  420K   32G   1% /dev/shm
tmpfs                 tmpfs     5.0M     0  5.0M   0% /run/lock
tmpfs                 tmpfs      32G     0   32G   0% /sys/fs/cgroup
/dev/md1              ext3      488M  134M  328M  29% /boot
/dev/mapper/cachedev0 ext4      5.4T  1.1T  4.0T  22% /var/lib/postgresql
tmpfs                 tmpfs     6.3G     0  6.3G   0% /run/user/1001
tmpfs                 tmpfs     6.3G     0  6.3G   0% /run/user/1003
```


**Virtualization**

```
   Static hostname: db3
         Icon name: computer-desktop
           Chassis: desktop
        Machine ID: 665fee6b7e4f4326bf6dfea7ab1f65d7
           Boot ID: 0ba0d4e478be4ac8b0a36ad5907c2eb7
  Operating System: Ubuntu 16.04.5 LTS
            Kernel: Linux 4.15.0-39-generic
      Architecture: x86-64
```

        

## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_A002"></a>
[Table of contents](#postgres-checkup_top)
# A002 Version information #

## Observations ##


### Master (`db2`) ###

```
PostgreSQL 9.6.11 on x86_64-pc-linux-gnu (Ubuntu 9.6.11-1.pgdg16.04+1), compiled by gcc (Ubuntu 5.4.0-6ubuntu1~16.04.10) 5.4.0 20160609, 64-bit
```




### Replica servers: ###

#### Replica (`db3`) ####


```
PostgreSQL 9.6.11 on x86_64-pc-linux-gnu (Ubuntu 9.6.11-1.pgdg16.04+1), compiled by gcc (Ubuntu 5.4.0-6ubuntu1~16.04.10) 5.4.0 20160609, 64-bit
```


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_A003"></a>
[Table of contents](#postgres-checkup_top)
# A003 Postgres settings #

## Observations ##


### Master (`db2`) ###  
&#9660;&nbsp;Category | Setting | Value | Unit | Pretty value
---------|---------|-------|------|--------------
Autovacuum|[autovacuum](https://postgresqlco.nf/en/doc/param/autovacuum) | on |  | 
Autovacuum|[autovacuum_analyze_scale_factor](https://postgresqlco.nf/en/doc/param/autovacuum_analyze_scale_factor) | 0.05 |  | 
Autovacuum|[autovacuum_analyze_threshold](https://postgresqlco.nf/en/doc/param/autovacuum_analyze_threshold) | 50 |  | 
Autovacuum|[autovacuum_freeze_max_age](https://postgresqlco.nf/en/doc/param/autovacuum_freeze_max_age) | 200000000 |  | 
Autovacuum|[autovacuum_max_workers](https://postgresqlco.nf/en/doc/param/autovacuum_max_workers) | 10 |  | 
Autovacuum|[autovacuum_multixact_freeze_max_age](https://postgresqlco.nf/en/doc/param/autovacuum_multixact_freeze_max_age) | 400000000 |  | 
Autovacuum|[autovacuum_naptime](https://postgresqlco.nf/en/doc/param/autovacuum_naptime) | 60 | s  | 
Autovacuum|[autovacuum_vacuum_cost_delay](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_cost_delay) | 20 | ms  | 
Autovacuum|[autovacuum_vacuum_cost_limit](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_cost_limit) | -1 |  | 
Autovacuum|[autovacuum_vacuum_scale_factor](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_scale_factor) | 0.01 |  | 
Autovacuum|[autovacuum_vacuum_threshold](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_threshold) | 50 |  | 
Client Connection Defaults / Locale and Formatting|[client_encoding](https://postgresqlco.nf/en/doc/param/client_encoding) | UTF8 |  | 
Client Connection Defaults / Locale and Formatting|[DateStyle](https://postgresqlco.nf/en/doc/param/DateStyle) | ISO,  MDY |  | 
Client Connection Defaults / Locale and Formatting|[default_text_search_config](https://postgresqlco.nf/en/doc/param/default_text_search_config) | public.ru |  | 
Client Connection Defaults / Locale and Formatting|[extra_float_digits](https://postgresqlco.nf/en/doc/param/extra_float_digits) | 0 |  | 
Client Connection Defaults / Locale and Formatting|[IntervalStyle](https://postgresqlco.nf/en/doc/param/IntervalStyle) | postgres |  | 
Client Connection Defaults / Locale and Formatting|[lc_collate](https://postgresqlco.nf/en/doc/param/lc_collate) | en_US.UTF-8 |  | 
Client Connection Defaults / Locale and Formatting|[lc_ctype](https://postgresqlco.nf/en/doc/param/lc_ctype) | en_US.UTF-8 |  | 
Client Connection Defaults / Locale and Formatting|[lc_messages](https://postgresqlco.nf/en/doc/param/lc_messages) | C |  | 
Client Connection Defaults / Locale and Formatting|[lc_monetary](https://postgresqlco.nf/en/doc/param/lc_monetary) | en_US.UTF-8 |  | 
Client Connection Defaults / Locale and Formatting|[lc_numeric](https://postgresqlco.nf/en/doc/param/lc_numeric) | en_US.UTF-8 |  | 
Client Connection Defaults / Locale and Formatting|[lc_time](https://postgresqlco.nf/en/doc/param/lc_time) | en_US.UTF-8 |  | 
Client Connection Defaults / Locale and Formatting|[server_encoding](https://postgresqlco.nf/en/doc/param/server_encoding) | UTF8 |  | 
Client Connection Defaults / Locale and Formatting|[TimeZone](https://postgresqlco.nf/en/doc/param/TimeZone) | UTC |  | 
Client Connection Defaults / Locale and Formatting|[timezone_abbreviations](https://postgresqlco.nf/en/doc/param/timezone_abbreviations) | Default |  | 
Client Connection Defaults / Other Defaults|[dynamic_library_path](https://postgresqlco.nf/en/doc/param/dynamic_library_path) | $libdir |  | 
Client Connection Defaults / Other Defaults|[gin_fuzzy_search_limit](https://postgresqlco.nf/en/doc/param/gin_fuzzy_search_limit) | 0 |  | 
Client Connection Defaults / Other Defaults|[tcp_keepalives_count](https://postgresqlco.nf/en/doc/param/tcp_keepalives_count) | 0 |  | 
Client Connection Defaults / Other Defaults|[tcp_keepalives_idle](https://postgresqlco.nf/en/doc/param/tcp_keepalives_idle) | 0 | s  | 
Client Connection Defaults / Other Defaults|[tcp_keepalives_interval](https://postgresqlco.nf/en/doc/param/tcp_keepalives_interval) | 0 | s  | 
Client Connection Defaults / Shared Library Preloading|[local_preload_libraries](https://postgresqlco.nf/en/doc/param/local_preload_libraries) |  |  | 
Client Connection Defaults / Shared Library Preloading|[session_preload_libraries](https://postgresqlco.nf/en/doc/param/session_preload_libraries) |  |  | 
Client Connection Defaults / Shared Library Preloading|[shared_preload_libraries](https://postgresqlco.nf/en/doc/param/shared_preload_libraries) | pg_stat_statements, pg_stat_kcache, auto_explain |  | 
Client Connection Defaults / Statement Behavior|[bytea_output](https://postgresqlco.nf/en/doc/param/bytea_output) | hex |  | 
Client Connection Defaults / Statement Behavior|[check_function_bodies](https://postgresqlco.nf/en/doc/param/check_function_bodies) | on |  | 
Client Connection Defaults / Statement Behavior|[default_tablespace](https://postgresqlco.nf/en/doc/param/default_tablespace) |  |  | 
Client Connection Defaults / Statement Behavior|[default_transaction_deferrable](https://postgresqlco.nf/en/doc/param/default_transaction_deferrable) | off |  | 
Client Connection Defaults / Statement Behavior|[default_transaction_isolation](https://postgresqlco.nf/en/doc/param/default_transaction_isolation) | read committed |  | 
Client Connection Defaults / Statement Behavior|[default_transaction_read_only](https://postgresqlco.nf/en/doc/param/default_transaction_read_only) | off |  | 
Client Connection Defaults / Statement Behavior|[gin_pending_list_limit](https://postgresqlco.nf/en/doc/param/gin_pending_list_limit) | 4096 | kB  | 4.00 MiB
Client Connection Defaults / Statement Behavior|[idle_in_transaction_session_timeout](https://postgresqlco.nf/en/doc/param/idle_in_transaction_session_timeout) | 600000 | ms  | 
Client Connection Defaults / Statement Behavior|[lock_timeout](https://postgresqlco.nf/en/doc/param/lock_timeout) | 0 | ms  | 
Client Connection Defaults / Statement Behavior|[search_path](https://postgresqlco.nf/en/doc/param/search_path) | "$user",  public |  | 
Client Connection Defaults / Statement Behavior|[session_replication_role](https://postgresqlco.nf/en/doc/param/session_replication_role) | origin |  | 
Client Connection Defaults / Statement Behavior|[statement_timeout](https://postgresqlco.nf/en/doc/param/statement_timeout) | 120000 | ms  | 
Client Connection Defaults / Statement Behavior|[temp_tablespaces](https://postgresqlco.nf/en/doc/param/temp_tablespaces) |  |  | 
Client Connection Defaults / Statement Behavior|[transaction_deferrable](https://postgresqlco.nf/en/doc/param/transaction_deferrable) | off |  | 
Client Connection Defaults / Statement Behavior|[transaction_isolation](https://postgresqlco.nf/en/doc/param/transaction_isolation) | read committed |  | 
Client Connection Defaults / Statement Behavior|[transaction_read_only](https://postgresqlco.nf/en/doc/param/transaction_read_only) | off |  | 
Client Connection Defaults / Statement Behavior|[vacuum_freeze_min_age](https://postgresqlco.nf/en/doc/param/vacuum_freeze_min_age) | 50000000 |  | 
Client Connection Defaults / Statement Behavior|[vacuum_freeze_table_age](https://postgresqlco.nf/en/doc/param/vacuum_freeze_table_age) | 150000000 |  | 
Client Connection Defaults / Statement Behavior|[vacuum_multixact_freeze_min_age](https://postgresqlco.nf/en/doc/param/vacuum_multixact_freeze_min_age) | 5000000 |  | 
Client Connection Defaults / Statement Behavior|[vacuum_multixact_freeze_table_age](https://postgresqlco.nf/en/doc/param/vacuum_multixact_freeze_table_age) | 150000000 |  | 
Client Connection Defaults / Statement Behavior|[xmlbinary](https://postgresqlco.nf/en/doc/param/xmlbinary) | base64 |  | 
Client Connection Defaults / Statement Behavior|[xmloption](https://postgresqlco.nf/en/doc/param/xmloption) | content |  | 
Connections and Authentication / Connection Settings|[bonjour](https://postgresqlco.nf/en/doc/param/bonjour) | off |  | 
Connections and Authentication / Connection Settings|[bonjour_name](https://postgresqlco.nf/en/doc/param/bonjour_name) |  |  | 
Connections and Authentication / Connection Settings|[listen_addresses](https://postgresqlco.nf/en/doc/param/listen_addresses) | * |  | 
Connections and Authentication / Connection Settings|[max_connections](https://postgresqlco.nf/en/doc/param/max_connections) | 1000 |  | 
Connections and Authentication / Connection Settings|[port](https://postgresqlco.nf/en/doc/param/port) | 5432 |  | 
Connections and Authentication / Connection Settings|[superuser_reserved_connections](https://postgresqlco.nf/en/doc/param/superuser_reserved_connections) | 3 |  | 
Connections and Authentication / Connection Settings|[unix_socket_directories](https://postgresqlco.nf/en/doc/param/unix_socket_directories) | /var/run/postgresql |  | 
Connections and Authentication / Connection Settings|[unix_socket_group](https://postgresqlco.nf/en/doc/param/unix_socket_group) |  |  | 
Connections and Authentication / Connection Settings|[unix_socket_permissions](https://postgresqlco.nf/en/doc/param/unix_socket_permissions) | 0777 |  | 
Connections and Authentication / Security and Authentication|[authentication_timeout](https://postgresqlco.nf/en/doc/param/authentication_timeout) | 60 | s  | 
Connections and Authentication / Security and Authentication|[db_user_namespace](https://postgresqlco.nf/en/doc/param/db_user_namespace) | off |  | 
Connections and Authentication / Security and Authentication|[krb_caseins_users](https://postgresqlco.nf/en/doc/param/krb_caseins_users) | off |  | 
Connections and Authentication / Security and Authentication|[krb_server_keyfile](https://postgresqlco.nf/en/doc/param/krb_server_keyfile) | FILE:/etc/postgresql-common/krb5.keytab |  | 
Connections and Authentication / Security and Authentication|[password_encryption](https://postgresqlco.nf/en/doc/param/password_encryption) | on |  | 
Connections and Authentication / Security and Authentication|[row_security](https://postgresqlco.nf/en/doc/param/row_security) | on |  | 
Connections and Authentication / Security and Authentication|[ssl](https://postgresqlco.nf/en/doc/param/ssl) | on |  | 
Connections and Authentication / Security and Authentication|[ssl_ca_file](https://postgresqlco.nf/en/doc/param/ssl_ca_file) |  |  | 
Connections and Authentication / Security and Authentication|[ssl_cert_file](https://postgresqlco.nf/en/doc/param/ssl_cert_file) | /etc/ssl/certs/ssl-cert-snakeoil.pem |  | 
Connections and Authentication / Security and Authentication|[ssl_ciphers](https://postgresqlco.nf/en/doc/param/ssl_ciphers) | HIGH:MEDIUM:+3DES:!aNULL |  | 
Connections and Authentication / Security and Authentication|[ssl_crl_file](https://postgresqlco.nf/en/doc/param/ssl_crl_file) |  |  | 
Connections and Authentication / Security and Authentication|[ssl_ecdh_curve](https://postgresqlco.nf/en/doc/param/ssl_ecdh_curve) | prime256v1 |  | 
Connections and Authentication / Security and Authentication|[ssl_key_file](https://postgresqlco.nf/en/doc/param/ssl_key_file) | /etc/ssl/private/ssl-cert-snakeoil.key |  | 
Connections and Authentication / Security and Authentication|[ssl_prefer_server_ciphers](https://postgresqlco.nf/en/doc/param/ssl_prefer_server_ciphers) | on |  | 
Customized Options|[auto_explain.log_analyze](https://postgresqlco.nf/en/doc/param/auto_explain.log_analyze) | on |  | 
Customized Options|[auto_explain.log_buffers](https://postgresqlco.nf/en/doc/param/auto_explain.log_buffers) | on |  | 
Customized Options|[auto_explain.log_format](https://postgresqlco.nf/en/doc/param/auto_explain.log_format) | text |  | 
Customized Options|[auto_explain.log_min_duration](https://postgresqlco.nf/en/doc/param/auto_explain.log_min_duration) | -1 | ms  | 
Customized Options|[auto_explain.log_nested_statements](https://postgresqlco.nf/en/doc/param/auto_explain.log_nested_statements) | on |  | 
Customized Options|[auto_explain.log_timing](https://postgresqlco.nf/en/doc/param/auto_explain.log_timing) | off |  | 
Customized Options|[auto_explain.log_triggers](https://postgresqlco.nf/en/doc/param/auto_explain.log_triggers) | on |  | 
Customized Options|[auto_explain.log_verbose](https://postgresqlco.nf/en/doc/param/auto_explain.log_verbose) | on |  | 
Customized Options|[auto_explain.sample_rate](https://postgresqlco.nf/en/doc/param/auto_explain.sample_rate) | 1 |  | 
Customized Options|[pg_stat_kcache.linux_hz](https://postgresqlco.nf/en/doc/param/pg_stat_kcache.linux_hz) | 1000000 |  | 
Customized Options|[pg_stat_statements.max](https://postgresqlco.nf/en/doc/param/pg_stat_statements.max) | 500 |  | 
Customized Options|[pg_stat_statements.save](https://postgresqlco.nf/en/doc/param/pg_stat_statements.save) | on |  | 
Customized Options|[pg_stat_statements.track](https://postgresqlco.nf/en/doc/param/pg_stat_statements.track) | all |  | 
Customized Options|[pg_stat_statements.track_utility](https://postgresqlco.nf/en/doc/param/pg_stat_statements.track_utility) | off |  | 
Developer Options|[allow_system_table_mods](https://postgresqlco.nf/en/doc/param/allow_system_table_mods) | off |  | 
Developer Options|[ignore_checksum_failure](https://postgresqlco.nf/en/doc/param/ignore_checksum_failure) | off |  | 
Developer Options|[ignore_system_indexes](https://postgresqlco.nf/en/doc/param/ignore_system_indexes) | off |  | 
Developer Options|[post_auth_delay](https://postgresqlco.nf/en/doc/param/post_auth_delay) | 0 | s  | 
Developer Options|[pre_auth_delay](https://postgresqlco.nf/en/doc/param/pre_auth_delay) | 0 | s  | 
Developer Options|[trace_notify](https://postgresqlco.nf/en/doc/param/trace_notify) | off |  | 
Developer Options|[trace_recovery_messages](https://postgresqlco.nf/en/doc/param/trace_recovery_messages) | log |  | 
Developer Options|[trace_sort](https://postgresqlco.nf/en/doc/param/trace_sort) | off |  | 
Developer Options|[zero_damaged_pages](https://postgresqlco.nf/en/doc/param/zero_damaged_pages) | off |  | 
Error Handling|[exit_on_error](https://postgresqlco.nf/en/doc/param/exit_on_error) | off |  | 
Error Handling|[restart_after_crash](https://postgresqlco.nf/en/doc/param/restart_after_crash) | on |  | 
File Locations|[config_file](https://postgresqlco.nf/en/doc/param/config_file) | /etc/postgresql/9.6/main/postgresql.conf |  | 
File Locations|[data_directory](https://postgresqlco.nf/en/doc/param/data_directory) | /var/lib/postgresql/9.6/main |  | 
File Locations|[external_pid_file](https://postgresqlco.nf/en/doc/param/external_pid_file) | /var/run/postgresql/9.6-main.pid |  | 
File Locations|[hba_file](https://postgresqlco.nf/en/doc/param/hba_file) | /etc/postgresql/9.6/main/pg_hba.conf |  | 
File Locations|[ident_file](https://postgresqlco.nf/en/doc/param/ident_file) | /etc/postgresql/9.6/main/pg_ident.conf |  | 
Lock Management|[deadlock_timeout](https://postgresqlco.nf/en/doc/param/deadlock_timeout) | 1000 | ms  | 
Lock Management|[max_locks_per_transaction](https://postgresqlco.nf/en/doc/param/max_locks_per_transaction) | 64 |  | 
Lock Management|[max_pred_locks_per_transaction](https://postgresqlco.nf/en/doc/param/max_pred_locks_per_transaction) | 64 |  | 
Preset Options|[block_size](https://postgresqlco.nf/en/doc/param/block_size) | 8192 |  | 
Preset Options|[data_checksums](https://postgresqlco.nf/en/doc/param/data_checksums) | off |  | 
Preset Options|[debug_assertions](https://postgresqlco.nf/en/doc/param/debug_assertions) | off |  | 
Preset Options|[integer_datetimes](https://postgresqlco.nf/en/doc/param/integer_datetimes) | on |  | 
Preset Options|[max_function_args](https://postgresqlco.nf/en/doc/param/max_function_args) | 100 |  | 
Preset Options|[max_identifier_length](https://postgresqlco.nf/en/doc/param/max_identifier_length) | 63 |  | 
Preset Options|[max_index_keys](https://postgresqlco.nf/en/doc/param/max_index_keys) | 32 |  | 
Preset Options|[segment_size](https://postgresqlco.nf/en/doc/param/segment_size) | 131072 | 8kB  | 1.00 GiB
Preset Options|[server_version](https://postgresqlco.nf/en/doc/param/server_version) | 9.6.11 |  | 
Preset Options|[server_version_num](https://postgresqlco.nf/en/doc/param/server_version_num) | 90611 |  | 
Preset Options|[wal_block_size](https://postgresqlco.nf/en/doc/param/wal_block_size) | 8192 |  | 
Preset Options|[wal_segment_size](https://postgresqlco.nf/en/doc/param/wal_segment_size) | 2048 | 8kB  | 16.00 MiB
Process Title|[cluster_name](https://postgresqlco.nf/en/doc/param/cluster_name) | 9.6/main |  | 
Process Title|[update_process_title](https://postgresqlco.nf/en/doc/param/update_process_title) | on |  | 
Query Tuning / Genetic Query Optimizer|[geqo](https://postgresqlco.nf/en/doc/param/geqo) | on |  | 
Query Tuning / Genetic Query Optimizer|[geqo_effort](https://postgresqlco.nf/en/doc/param/geqo_effort) | 5 |  | 
Query Tuning / Genetic Query Optimizer|[geqo_generations](https://postgresqlco.nf/en/doc/param/geqo_generations) | 0 |  | 
Query Tuning / Genetic Query Optimizer|[geqo_pool_size](https://postgresqlco.nf/en/doc/param/geqo_pool_size) | 0 |  | 
Query Tuning / Genetic Query Optimizer|[geqo_seed](https://postgresqlco.nf/en/doc/param/geqo_seed) | 0 |  | 
Query Tuning / Genetic Query Optimizer|[geqo_selection_bias](https://postgresqlco.nf/en/doc/param/geqo_selection_bias) | 2 |  | 
Query Tuning / Genetic Query Optimizer|[geqo_threshold](https://postgresqlco.nf/en/doc/param/geqo_threshold) | 12 |  | 
Query Tuning / Other Planner Options|[constraint_exclusion](https://postgresqlco.nf/en/doc/param/constraint_exclusion) | partition |  | 
Query Tuning / Other Planner Options|[cursor_tuple_fraction](https://postgresqlco.nf/en/doc/param/cursor_tuple_fraction) | 0.1 |  | 
Query Tuning / Other Planner Options|[default_statistics_target](https://postgresqlco.nf/en/doc/param/default_statistics_target) | 1000 |  | 
Query Tuning / Other Planner Options|[force_parallel_mode](https://postgresqlco.nf/en/doc/param/force_parallel_mode) | off |  | 
Query Tuning / Other Planner Options|[from_collapse_limit](https://postgresqlco.nf/en/doc/param/from_collapse_limit) | 1 |  | 
Query Tuning / Other Planner Options|[join_collapse_limit](https://postgresqlco.nf/en/doc/param/join_collapse_limit) | 1 |  | 
Query Tuning / Planner Cost Constants|[cpu_index_tuple_cost](https://postgresqlco.nf/en/doc/param/cpu_index_tuple_cost) | 0.005 |  | 
Query Tuning / Planner Cost Constants|[cpu_operator_cost](https://postgresqlco.nf/en/doc/param/cpu_operator_cost) | 0.0025 |  | 
Query Tuning / Planner Cost Constants|[cpu_tuple_cost](https://postgresqlco.nf/en/doc/param/cpu_tuple_cost) | 0.01 |  | 
Query Tuning / Planner Cost Constants|[effective_cache_size](https://postgresqlco.nf/en/doc/param/effective_cache_size) | 6291456 | 8kB  | 48.00 GiB
Query Tuning / Planner Cost Constants|[min_parallel_relation_size](https://postgresqlco.nf/en/doc/param/min_parallel_relation_size) | 1024 | 8kB  | 8.00 MiB
Query Tuning / Planner Cost Constants|[parallel_setup_cost](https://postgresqlco.nf/en/doc/param/parallel_setup_cost) | 1000 |  | 
Query Tuning / Planner Cost Constants|[parallel_tuple_cost](https://postgresqlco.nf/en/doc/param/parallel_tuple_cost) | 0.1 |  | 
Query Tuning / Planner Cost Constants|[random_page_cost](https://postgresqlco.nf/en/doc/param/random_page_cost) | 1 |  | 
Query Tuning / Planner Cost Constants|[seq_page_cost](https://postgresqlco.nf/en/doc/param/seq_page_cost) | 1 |  | 
Query Tuning / Planner Method Configuration|[enable_bitmapscan](https://postgresqlco.nf/en/doc/param/enable_bitmapscan) | on |  | 
Query Tuning / Planner Method Configuration|[enable_hashagg](https://postgresqlco.nf/en/doc/param/enable_hashagg) | on |  | 
Query Tuning / Planner Method Configuration|[enable_hashjoin](https://postgresqlco.nf/en/doc/param/enable_hashjoin) | on |  | 
Query Tuning / Planner Method Configuration|[enable_indexonlyscan](https://postgresqlco.nf/en/doc/param/enable_indexonlyscan) | on |  | 
Query Tuning / Planner Method Configuration|[enable_indexscan](https://postgresqlco.nf/en/doc/param/enable_indexscan) | on |  | 
Query Tuning / Planner Method Configuration|[enable_material](https://postgresqlco.nf/en/doc/param/enable_material) | on |  | 
Query Tuning / Planner Method Configuration|[enable_mergejoin](https://postgresqlco.nf/en/doc/param/enable_mergejoin) | on |  | 
Query Tuning / Planner Method Configuration|[enable_nestloop](https://postgresqlco.nf/en/doc/param/enable_nestloop) | on |  | 
Query Tuning / Planner Method Configuration|[enable_seqscan](https://postgresqlco.nf/en/doc/param/enable_seqscan) | off |  | 
Query Tuning / Planner Method Configuration|[enable_sort](https://postgresqlco.nf/en/doc/param/enable_sort) | on |  | 
Query Tuning / Planner Method Configuration|[enable_tidscan](https://postgresqlco.nf/en/doc/param/enable_tidscan) | on |  | 
Replication|[track_commit_timestamp](https://postgresqlco.nf/en/doc/param/track_commit_timestamp) | off |  | 
Replication / Master Server|[synchronous_standby_names](https://postgresqlco.nf/en/doc/param/synchronous_standby_names) |  |  | 
Replication / Master Server|[vacuum_defer_cleanup_age](https://postgresqlco.nf/en/doc/param/vacuum_defer_cleanup_age) | 0 |  | 
Replication / Sending Servers|[max_replication_slots](https://postgresqlco.nf/en/doc/param/max_replication_slots) | 5 |  | 
Replication / Sending Servers|[max_wal_senders](https://postgresqlco.nf/en/doc/param/max_wal_senders) | 5 |  | 
Replication / Sending Servers|[wal_keep_segments](https://postgresqlco.nf/en/doc/param/wal_keep_segments) | 32 |  | 
Replication / Sending Servers|[wal_sender_timeout](https://postgresqlco.nf/en/doc/param/wal_sender_timeout) | 60000 | ms  | 
Replication / Standby Servers|[hot_standby](https://postgresqlco.nf/en/doc/param/hot_standby) | on |  | 
Replication / Standby Servers|[hot_standby_feedback](https://postgresqlco.nf/en/doc/param/hot_standby_feedback) | on |  | 
Replication / Standby Servers|[max_standby_archive_delay](https://postgresqlco.nf/en/doc/param/max_standby_archive_delay) | 30000 | ms  | 
Replication / Standby Servers|[max_standby_streaming_delay](https://postgresqlco.nf/en/doc/param/max_standby_streaming_delay) | 30000 | ms  | 
Replication / Standby Servers|[wal_receiver_status_interval](https://postgresqlco.nf/en/doc/param/wal_receiver_status_interval) | 10 | s  | 
Replication / Standby Servers|[wal_receiver_timeout](https://postgresqlco.nf/en/doc/param/wal_receiver_timeout) | 60000 | ms  | 
Replication / Standby Servers|[wal_retrieve_retry_interval](https://postgresqlco.nf/en/doc/param/wal_retrieve_retry_interval) | 5000 | ms  | 
Reporting and Logging / What to Log|[application_name](https://postgresqlco.nf/en/doc/param/application_name) | psql |  | 
Reporting and Logging / What to Log|[debug_pretty_print](https://postgresqlco.nf/en/doc/param/debug_pretty_print) | on |  | 
Reporting and Logging / What to Log|[debug_print_parse](https://postgresqlco.nf/en/doc/param/debug_print_parse) | off |  | 
Reporting and Logging / What to Log|[debug_print_plan](https://postgresqlco.nf/en/doc/param/debug_print_plan) | off |  | 
Reporting and Logging / What to Log|[debug_print_rewritten](https://postgresqlco.nf/en/doc/param/debug_print_rewritten) | off |  | 
Reporting and Logging / What to Log|[log_autovacuum_min_duration](https://postgresqlco.nf/en/doc/param/log_autovacuum_min_duration) | 0 | ms  | 
Reporting and Logging / What to Log|[log_checkpoints](https://postgresqlco.nf/en/doc/param/log_checkpoints) | on |  | 
Reporting and Logging / What to Log|[log_connections](https://postgresqlco.nf/en/doc/param/log_connections) | on |  | 
Reporting and Logging / What to Log|[log_disconnections](https://postgresqlco.nf/en/doc/param/log_disconnections) | on |  | 
Reporting and Logging / What to Log|[log_duration](https://postgresqlco.nf/en/doc/param/log_duration) | off |  | 
Reporting and Logging / What to Log|[log_error_verbosity](https://postgresqlco.nf/en/doc/param/log_error_verbosity) | default |  | 
Reporting and Logging / What to Log|[log_hostname](https://postgresqlco.nf/en/doc/param/log_hostname) | off |  | 
Reporting and Logging / What to Log|[log_line_prefix](https://postgresqlco.nf/en/doc/param/log_line_prefix) | %t [%p]: [%l-1] db=%d, user=%u  |  | 
Reporting and Logging / What to Log|[log_lock_waits](https://postgresqlco.nf/en/doc/param/log_lock_waits) | on |  | 
Reporting and Logging / What to Log|[log_replication_commands](https://postgresqlco.nf/en/doc/param/log_replication_commands) | on |  | 
Reporting and Logging / What to Log|[log_statement](https://postgresqlco.nf/en/doc/param/log_statement) | none |  | 
Reporting and Logging / What to Log|[log_temp_files](https://postgresqlco.nf/en/doc/param/log_temp_files) | 0 | kB  | 0.00 bytes
Reporting and Logging / What to Log|[log_timezone](https://postgresqlco.nf/en/doc/param/log_timezone) | localtime |  | 
Reporting and Logging / When to Log|[client_min_messages](https://postgresqlco.nf/en/doc/param/client_min_messages) | notice |  | 
Reporting and Logging / When to Log|[log_min_duration_statement](https://postgresqlco.nf/en/doc/param/log_min_duration_statement) | 500 | ms  | 
Reporting and Logging / When to Log|[log_min_error_statement](https://postgresqlco.nf/en/doc/param/log_min_error_statement) | notice |  | 
Reporting and Logging / When to Log|[log_min_messages](https://postgresqlco.nf/en/doc/param/log_min_messages) | notice |  | 
Reporting and Logging / Where to Log|[event_source](https://postgresqlco.nf/en/doc/param/event_source) | PostgreSQL |  | 
Reporting and Logging / Where to Log|[log_destination](https://postgresqlco.nf/en/doc/param/log_destination) | stderr |  | 
Reporting and Logging / Where to Log|[log_directory](https://postgresqlco.nf/en/doc/param/log_directory) | pg_log |  | 
Reporting and Logging / Where to Log|[log_file_mode](https://postgresqlco.nf/en/doc/param/log_file_mode) | 0600 |  | 
Reporting and Logging / Where to Log|[log_filename](https://postgresqlco.nf/en/doc/param/log_filename) | postgresql-%Y-%m-%d_%H%M%S.log |  | 
Reporting and Logging / Where to Log|[logging_collector](https://postgresqlco.nf/en/doc/param/logging_collector) | on |  | 
Reporting and Logging / Where to Log|[log_rotation_age](https://postgresqlco.nf/en/doc/param/log_rotation_age) | 1440 | min  | 
Reporting and Logging / Where to Log|[log_rotation_size](https://postgresqlco.nf/en/doc/param/log_rotation_size) | 102400 | kB  | 100.00 MiB
Reporting and Logging / Where to Log|[log_truncate_on_rotation](https://postgresqlco.nf/en/doc/param/log_truncate_on_rotation) | off |  | 
Reporting and Logging / Where to Log|[syslog_facility](https://postgresqlco.nf/en/doc/param/syslog_facility) | local0 |  | 
Reporting and Logging / Where to Log|[syslog_ident](https://postgresqlco.nf/en/doc/param/syslog_ident) | postgres |  | 
Reporting and Logging / Where to Log|[syslog_sequence_numbers](https://postgresqlco.nf/en/doc/param/syslog_sequence_numbers) | on |  | 
Reporting and Logging / Where to Log|[syslog_split_messages](https://postgresqlco.nf/en/doc/param/syslog_split_messages) | on |  | 
Resource Usage / Asynchronous Behavior|[backend_flush_after](https://postgresqlco.nf/en/doc/param/backend_flush_after) | 0 | 8kB  | 0.00 bytes
Resource Usage / Asynchronous Behavior|[effective_io_concurrency](https://postgresqlco.nf/en/doc/param/effective_io_concurrency) | 1 |  | 
Resource Usage / Asynchronous Behavior|[max_parallel_workers_per_gather](https://postgresqlco.nf/en/doc/param/max_parallel_workers_per_gather) | 0 |  | 
Resource Usage / Asynchronous Behavior|[max_worker_processes](https://postgresqlco.nf/en/doc/param/max_worker_processes) | 8 |  | 
Resource Usage / Asynchronous Behavior|[old_snapshot_threshold](https://postgresqlco.nf/en/doc/param/old_snapshot_threshold) | -1 | min  | 
Resource Usage / Background Writer|[bgwriter_delay](https://postgresqlco.nf/en/doc/param/bgwriter_delay) | 200 | ms  | 
Resource Usage / Background Writer|[bgwriter_flush_after](https://postgresqlco.nf/en/doc/param/bgwriter_flush_after) | 64 | 8kB  | 512.00 KiB
Resource Usage / Background Writer|[bgwriter_lru_maxpages](https://postgresqlco.nf/en/doc/param/bgwriter_lru_maxpages) | 100 |  | 
Resource Usage / Background Writer|[bgwriter_lru_multiplier](https://postgresqlco.nf/en/doc/param/bgwriter_lru_multiplier) | 2 |  | 
Resource Usage / Cost-Based Vacuum Delay|[vacuum_cost_delay](https://postgresqlco.nf/en/doc/param/vacuum_cost_delay) | 0 | ms  | 
Resource Usage / Cost-Based Vacuum Delay|[vacuum_cost_limit](https://postgresqlco.nf/en/doc/param/vacuum_cost_limit) | 200 |  | 
Resource Usage / Cost-Based Vacuum Delay|[vacuum_cost_page_dirty](https://postgresqlco.nf/en/doc/param/vacuum_cost_page_dirty) | 20 |  | 
Resource Usage / Cost-Based Vacuum Delay|[vacuum_cost_page_hit](https://postgresqlco.nf/en/doc/param/vacuum_cost_page_hit) | 1 |  | 
Resource Usage / Cost-Based Vacuum Delay|[vacuum_cost_page_miss](https://postgresqlco.nf/en/doc/param/vacuum_cost_page_miss) | 10 |  | 
Resource Usage / Disk|[temp_file_limit](https://postgresqlco.nf/en/doc/param/temp_file_limit) | -1 | kB  | 
Resource Usage / Kernel Resources|[max_files_per_process](https://postgresqlco.nf/en/doc/param/max_files_per_process) | 1000 |  | 
Resource Usage / Memory|[autovacuum_work_mem](https://postgresqlco.nf/en/doc/param/autovacuum_work_mem) | -1 | kB  | 
Resource Usage / Memory|[dynamic_shared_memory_type](https://postgresqlco.nf/en/doc/param/dynamic_shared_memory_type) | posix |  | 
Resource Usage / Memory|[huge_pages](https://postgresqlco.nf/en/doc/param/huge_pages) | try |  | 
Resource Usage / Memory|[maintenance_work_mem](https://postgresqlco.nf/en/doc/param/maintenance_work_mem) | 2097152 | kB  | 2.00 GiB
Resource Usage / Memory|[max_prepared_transactions](https://postgresqlco.nf/en/doc/param/max_prepared_transactions) | 0 |  | 
Resource Usage / Memory|[max_stack_depth](https://postgresqlco.nf/en/doc/param/max_stack_depth) | 2048 | kB  | 2.00 MiB
Resource Usage / Memory|[replacement_sort_tuples](https://postgresqlco.nf/en/doc/param/replacement_sort_tuples) | 150000 |  | 
Resource Usage / Memory|[shared_buffers](https://postgresqlco.nf/en/doc/param/shared_buffers) | 2097152 | 8kB  | 16.00 GiB
Resource Usage / Memory|[temp_buffers](https://postgresqlco.nf/en/doc/param/temp_buffers) | 8192 | 8kB  | 64.00 MiB
Resource Usage / Memory|[track_activity_query_size](https://postgresqlco.nf/en/doc/param/track_activity_query_size) | 1024 |  | 
Resource Usage / Memory|[work_mem](https://postgresqlco.nf/en/doc/param/work_mem) | 65536 | kB  | 64.00 MiB
Statistics / Monitoring|[log_executor_stats](https://postgresqlco.nf/en/doc/param/log_executor_stats) | off |  | 
Statistics / Monitoring|[log_parser_stats](https://postgresqlco.nf/en/doc/param/log_parser_stats) | off |  | 
Statistics / Monitoring|[log_planner_stats](https://postgresqlco.nf/en/doc/param/log_planner_stats) | off |  | 
Statistics / Monitoring|[log_statement_stats](https://postgresqlco.nf/en/doc/param/log_statement_stats) | off |  | 
Statistics / Query and Index Statistics Collector|[stats_temp_directory](https://postgresqlco.nf/en/doc/param/stats_temp_directory) | /dev/shm |  | 
Statistics / Query and Index Statistics Collector|[track_activities](https://postgresqlco.nf/en/doc/param/track_activities) | on |  | 
Statistics / Query and Index Statistics Collector|[track_counts](https://postgresqlco.nf/en/doc/param/track_counts) | on |  | 
Statistics / Query and Index Statistics Collector|[track_functions](https://postgresqlco.nf/en/doc/param/track_functions) | all |  | 
Statistics / Query and Index Statistics Collector|[track_io_timing](https://postgresqlco.nf/en/doc/param/track_io_timing) | on |  | 
Version and Platform Compatibility / Other Platforms and Clients|[transform_null_equals](https://postgresqlco.nf/en/doc/param/transform_null_equals) | off |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[array_nulls](https://postgresqlco.nf/en/doc/param/array_nulls) | on |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[backslash_quote](https://postgresqlco.nf/en/doc/param/backslash_quote) | safe_encoding |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[default_with_oids](https://postgresqlco.nf/en/doc/param/default_with_oids) | off |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[escape_string_warning](https://postgresqlco.nf/en/doc/param/escape_string_warning) | off |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[lo_compat_privileges](https://postgresqlco.nf/en/doc/param/lo_compat_privileges) | off |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[operator_precedence_warning](https://postgresqlco.nf/en/doc/param/operator_precedence_warning) | off |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[quote_all_identifiers](https://postgresqlco.nf/en/doc/param/quote_all_identifiers) | off |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[sql_inheritance](https://postgresqlco.nf/en/doc/param/sql_inheritance) | on |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[standard_conforming_strings](https://postgresqlco.nf/en/doc/param/standard_conforming_strings) | on |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[synchronize_seqscans](https://postgresqlco.nf/en/doc/param/synchronize_seqscans) | on |  | 
Write-Ahead Log / Archiving|[archive_command](https://postgresqlco.nf/en/doc/param/archive_command) | envdir /etc/wal-g.d/env /usr/local/bin/wal-g wal-push %p |  | 
Write-Ahead Log / Archiving|[archive_mode](https://postgresqlco.nf/en/doc/param/archive_mode) | on |  | 
Write-Ahead Log / Archiving|[archive_timeout](https://postgresqlco.nf/en/doc/param/archive_timeout) | 120 | s  | 
Write-Ahead Log / Checkpoints|[checkpoint_completion_target](https://postgresqlco.nf/en/doc/param/checkpoint_completion_target) | 0.7 |  | 
Write-Ahead Log / Checkpoints|[checkpoint_flush_after](https://postgresqlco.nf/en/doc/param/checkpoint_flush_after) | 32 | 8kB  | 256.00 KiB
Write-Ahead Log / Checkpoints|[checkpoint_timeout](https://postgresqlco.nf/en/doc/param/checkpoint_timeout) | 300 | s  | 
Write-Ahead Log / Checkpoints|[checkpoint_warning](https://postgresqlco.nf/en/doc/param/checkpoint_warning) | 30 | s  | 
Write-Ahead Log / Checkpoints|[max_wal_size](https://postgresqlco.nf/en/doc/param/max_wal_size) | 64 | 16MB  | 1.00 GiB
Write-Ahead Log / Checkpoints|[min_wal_size](https://postgresqlco.nf/en/doc/param/min_wal_size) | 5 | 16MB  | 80.00 MiB
Write-Ahead Log / Settings|[commit_delay](https://postgresqlco.nf/en/doc/param/commit_delay) | 0 |  | 
Write-Ahead Log / Settings|[commit_siblings](https://postgresqlco.nf/en/doc/param/commit_siblings) | 5 |  | 
Write-Ahead Log / Settings|[fsync](https://postgresqlco.nf/en/doc/param/fsync) | on |  | 
Write-Ahead Log / Settings|[full_page_writes](https://postgresqlco.nf/en/doc/param/full_page_writes) | on |  | 
Write-Ahead Log / Settings|[synchronous_commit](https://postgresqlco.nf/en/doc/param/synchronous_commit) | off |  | 
Write-Ahead Log / Settings|[wal_buffers](https://postgresqlco.nf/en/doc/param/wal_buffers) | 2048 | 8kB  | 16.00 MiB
Write-Ahead Log / Settings|[wal_compression](https://postgresqlco.nf/en/doc/param/wal_compression) | off |  | 
Write-Ahead Log / Settings|[wal_level](https://postgresqlco.nf/en/doc/param/wal_level) | replica |  | 
Write-Ahead Log / Settings|[wal_log_hints](https://postgresqlco.nf/en/doc/param/wal_log_hints) | off |  | 
Write-Ahead Log / Settings|[wal_sync_method](https://postgresqlco.nf/en/doc/param/wal_sync_method) | fdatasync |  | 
Write-Ahead Log / Settings|[wal_writer_delay](https://postgresqlco.nf/en/doc/param/wal_writer_delay) | 1000 | ms  | 
Write-Ahead Log / Settings|[wal_writer_flush_after](https://postgresqlco.nf/en/doc/param/wal_writer_flush_after) | 128 | 8kB  | 1.00 MiB

  
---
<a name="postgres-checkup_A004"></a>
[Table of contents](#postgres-checkup_top)
# A004 Cluster information #

## Observations ##


### Master (`db2`) ###

 Indicator | Value
-----------|-------
Postgres Version | PostgreSQL&nbsp;9.6.11&nbsp;on&nbsp;x86_64-pc-linux-gnu&nbsp;(Ubuntu&nbsp;9.6.11-1.pgdg16.04+1),&nbsp;compiled&nbsp;by&nbsp;gcc&nbsp;(Ubuntu&nbsp;5.4.0-6ubuntu1~16.04.10)&nbsp;5.4.0&nbsp;20160609,&nbsp;64-bit
Config file | /etc/postgresql/9.6/main/postgresql.conf
Role | Master
Replicas | async/streaming:&nbsp;5.9.142.204
Started At | 2019-01-22&nbsp;22:42:26+00
Uptime | 20&nbsp;days&nbsp;15:55:52
Checkpoints | 20206
Forced Checkpoints | 5.0%
Checkpoint MB/sec | 0.140521
Database Name | postila_ru
Database Size | 653&nbsp;GB
Stats Since | 2018-12-05&nbsp;17:44:47+00
Stats Age | 68&nbsp;days&nbsp;20:53:32
Installed Extensions | btree_gin&nbsp;1.0,&nbsp;dblink&nbsp;1.1,&nbsp;file_fdw&nbsp;1.0,&nbsp;hstore&nbsp;1.4,&nbsp;pageinspect&nbsp;1.5pg_buffercache&nbsp;1.0,&nbsp;pg_prewarm&nbsp;1.1,&nbsp;pg_repack&nbsp;1.4.4,&nbsp;pg_stat_kcache&nbsp;2.1.1,&nbsp;pg_stat_statements&nbsp;1.4pgcrypto&nbsp;1.1,&nbsp;pgstattuple&nbsp;1.2,&nbsp;plpgsql&nbsp;1.0,&nbsp;plpython2u&nbsp;1.0,&nbsp;plsh&nbsp;2postgres_fdw&nbsp;1.0,&nbsp;rum&nbsp;1.2,&nbsp;tablefunc&nbsp;1.0,&nbsp;uuid-ossp&nbsp;1.0
Cache Effectiveness | 94.79%
Successful Commits | 99.97%
Conflicts | 0
Temp Files: total size | 525&nbsp;GB
Temp Files: total number of files | 1197
Temp Files: avg file size | 449&nbsp;MB
Deadlocks | 0

#### Databases sizes ####
Database | &#9660;&nbsp;Size
---------|------
postila_ru | 652.97 GiB
vegas | 85.87 GiB
nevada8 | 12.30 GiB
nevada5 | 12.28 GiB
nevada6 | 12.25 GiB
nevada7 | 12.25 GiB
nevada2 | 12.24 GiB
nevada4 | 12.22 GiB
nevada3 | 12.22 GiB
nevada1 | 12.19 GiB
nevada | 10.25 GiB
foto_prikolisti_com | 9.53 GiB
kotomail_ru | 9.06 GiB
prikolisti_com | 8.85 GiB
demotivator_prikolisti_com | 7.22 GiB
fffail_ru | 5.64 GiB
_4vkusa_ru | 5.57 GiB
postfix | 2.01 GiB
redash | 1.25 GiB
test | 1.16 GiB
fotominuta_ru | 829.22 MiB
startupturbo_com | 707.79 MiB
nombox_co | 438.10 MiB
strakhovm_ru | 281.36 MiB
postgresmen_ru | 134.48 MiB
svoi_simplead_ru | 134.00 MiB
startupturbo_com_old | 118.29 MiB
tks_f | 77.46 MiB
tpl_drupal | 34.15 MiB
postgres | 7.21 MiB
test2 | 7.18 MiB
template1 | 7.18 MiB
dba | 7.18 MiB
vividcortex | 7.18 MiB
template0 | 6.94 MiB

### Replica servers: ###
  
#### Replica (`db3`) ####
    
 Indicator | Value
-----------|-------
Postgres Version | PostgreSQL&nbsp;9.6.11&nbsp;on&nbsp;x86_64-pc-linux-gnu&nbsp;(Ubuntu&nbsp;9.6.11-1.pgdg16.04+1),&nbsp;compiled&nbsp;by&nbsp;gcc&nbsp;(Ubuntu&nbsp;5.4.0-6ubuntu1~16.04.10)&nbsp;5.4.0&nbsp;20160609,&nbsp;64-bit
Config file | /etc/postgresql/9.6/main/postgresql.conf
Role | Replica&nbsp;(delay:&nbsp;00:00:01;&nbsp;paused:&nbsp;false)
Replicas | 
Started At | 2019-01-25&nbsp;21:36:09+00
Uptime | 17&nbsp;days&nbsp;17:01:22
Checkpoints | 18004
Forced Checkpoints | 46.0%
Checkpoint MB/sec | 0.037865
Database Name | postila_ru
Database Size | 653&nbsp;GB
Stats Since | 2019-01-25&nbsp;21:36:14+00
Stats Age | 17&nbsp;days&nbsp;17:01:17
Installed Extensions | btree_gin&nbsp;1.0,&nbsp;dblink&nbsp;1.1,&nbsp;file_fdw&nbsp;1.0,&nbsp;hstore&nbsp;1.4,&nbsp;pageinspect&nbsp;1.5pg_buffercache&nbsp;1.0,&nbsp;pg_prewarm&nbsp;1.1,&nbsp;pg_repack&nbsp;1.4.4,&nbsp;pg_stat_kcache&nbsp;2.1.1,&nbsp;pg_stat_statements&nbsp;1.4pgcrypto&nbsp;1.1,&nbsp;pgstattuple&nbsp;1.2,&nbsp;plpgsql&nbsp;1.0,&nbsp;plpython2u&nbsp;1.0,&nbsp;plsh&nbsp;2postgres_fdw&nbsp;1.0,&nbsp;rum&nbsp;1.2,&nbsp;tablefunc&nbsp;1.0,&nbsp;uuid-ossp&nbsp;1.0
Cache Effectiveness | 96.55%
Successful Commits | 100.00%
Conflicts | 251
Temp Files: total size | 0&nbsp;bytes
Temp Files: total number of files | 0
Temp Files: avg file size | 
Deadlocks | 0


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_A005"></a>
[Table of contents](#postgres-checkup_top)
# A005 Extensions #

## Observations ##


### Master (`db2`) ###
&#9660;&nbsp;Database | Extension name | Installed version | Default version | Is old
---------|----------------|-------------------|-----------------|--------
_4vkusa_ru | pg_repack | 1.4.2 | 1.4.4 | OLD
_4vkusa_ru | pg_stat_statements | 1.2 | 1.4 | OLD
_4vkusa_ru | plpgsql | 1.0 | 1.0 | <no value>
dba | pg_stat_statements | 1.4 | 1.4 | <no value>
dba | plpgsql | 1.0 | 1.0 | <no value>
demotivator_prikolisti_com | pg_repack | 1.4.1 | 1.4.4 | OLD
demotivator_prikolisti_com | pg_stat_statements | 1.2 | 1.4 | OLD
demotivator_prikolisti_com | plpgsql | 1.0 | 1.0 | <no value>
fffail_ru | pg_repack | 1.4.1 | 1.4.4 | OLD
fffail_ru | pg_stat_statements | 1.2 | 1.4 | OLD
fffail_ru | plpgsql | 1.0 | 1.0 | <no value>
foto_prikolisti_com | pg_repack | 1.4.1 | 1.4.4 | OLD
foto_prikolisti_com | pg_stat_statements | 1.2 | 1.4 | OLD
foto_prikolisti_com | plpgsql | 1.0 | 1.0 | <no value>
fotominuta_ru | plpgsql | 1.0 | 1.0 | <no value>
kotomail_ru | pg_repack | 1.4.1 | 1.4.4 | OLD
kotomail_ru | pg_stat_statements | 1.2 | 1.4 | OLD
kotomail_ru | plpgsql | 1.0 | 1.0 | <no value>
nevada | pg_repack | 1.4.2 | 1.4.4 | OLD
nevada | pg_stat_statements | 1.4 | 1.4 | <no value>
nevada | plpgsql | 1.0 | 1.0 | <no value>
nevada1 | pg_stat_statements | 1.2 | 1.4 | OLD
nevada1 | plpgsql | 1.0 | 1.0 | <no value>
nevada2 | pg_repack | 1.4.1 | 1.4.4 | OLD
nevada2 | pg_stat_statements | 1.2 | 1.4 | OLD
nevada2 | plpgsql | 1.0 | 1.0 | <no value>
nevada3 | pg_repack | 1.4.1 | 1.4.4 | OLD
nevada3 | pg_stat_statements | 1.2 | 1.4 | OLD
nevada3 | plpgsql | 1.0 | 1.0 | <no value>
nevada4 | pg_repack | 1.4.1 | 1.4.4 | OLD
nevada4 | pg_stat_statements | 1.2 | 1.4 | OLD
nevada4 | plpgsql | 1.0 | 1.0 | <no value>
nevada5 | pg_repack | 1.4.1 | 1.4.4 | OLD
nevada5 | pg_stat_statements | 1.2 | 1.4 | OLD
nevada5 | plpgsql | 1.0 | 1.0 | <no value>
nevada6 | pg_repack | 1.4.1 | 1.4.4 | OLD
nevada6 | pg_stat_statements | 1.2 | 1.4 | OLD
nevada6 | plpgsql | 1.0 | 1.0 | <no value>
nevada7 | pg_stat_statements | 1.2 | 1.4 | OLD
nevada7 | plpgsql | 1.0 | 1.0 | <no value>
nevada8 | pg_repack | 1.4.1 | 1.4.4 | OLD
nevada8 | pg_stat_statements | 1.2 | 1.4 | OLD
nevada8 | plpgsql | 1.0 | 1.0 | <no value>
nombox_co | pg_stat_statements | 1.2 | 1.4 | OLD
nombox_co | plpgsql | 1.0 | 1.0 | <no value>
postfix | plpgsql | 1.0 | 1.0 | <no value>
postgresmen_ru | plpgsql | 1.0 | 1.0 | <no value>
postila_ru | btree_gin | 1.0 | 1.0 | <no value>
postila_ru | dblink | 1.1 | 1.2 | OLD
postila_ru | file_fdw | 1.0 | 1.0 | <no value>
postila_ru | hstore | 1.4 | 1.4 | <no value>
postila_ru | pageinspect | 1.5 | 1.5 | <no value>
postila_ru | pg_buffercache | 1.0 | 1.2 | OLD
postila_ru | pg_prewarm | 1.1 | 1.1 | <no value>
postila_ru | pg_repack | 1.4.4 | 1.4.4 | <no value>
postila_ru | pg_stat_kcache | 2.1.1 | 2.1.1 | <no value>
postila_ru | pg_stat_statements | 1.4 | 1.4 | <no value>
postila_ru | pgcrypto | 1.1 | 1.3 | OLD
postila_ru | pgstattuple | 1.2 | 1.4 | OLD
postila_ru | plpgsql | 1.0 | 1.0 | <no value>
postila_ru | plpython2u | 1.0 | 1.0 | <no value>
postila_ru | plsh | 2 | 2 | <no value>
postila_ru | postgres_fdw | 1.0 | 1.0 | <no value>
postila_ru | rum | 1.2 | 1.3 | OLD
postila_ru | tablefunc | 1.0 | 1.0 | <no value>
postila_ru | uuid-ossp | 1.0 | 1.1 | OLD
prikolisti_com | pg_stat_statements | 1.2 | 1.4 | OLD
prikolisti_com | plpgsql | 1.0 | 1.0 | <no value>
redash | plpgsql | 1.0 | 1.0 | <no value>
startupturbo_com | pg_stat_statements | 1.2 | 1.4 | OLD
startupturbo_com | plpgsql | 1.0 | 1.0 | <no value>
startupturbo_com_old | plpgsql | 1.0 | 1.0 | <no value>
strakhovm_ru | plpgsql | 1.0 | 1.0 | <no value>
svoi_simplead_ru | plpgsql | 1.0 | 1.0 | <no value>
test | pg_repack | 1.4.4 | 1.4.4 | <no value>
test | pg_stat_statements | 1.4 | 1.4 | <no value>
test | pgstattuple | 1.4 | 1.4 | <no value>
test | plpgsql | 1.0 | 1.0 | <no value>
test2 | plpgsql | 1.0 | 1.0 | <no value>
tks_f | plpgsql | 1.0 | 1.0 | <no value>
tpl_drupal | plpgsql | 1.0 | 1.0 | <no value>
vegas | pg_repack | 1.4.2 | 1.4.4 | OLD
vegas | pg_stat_statements | 1.2 | 1.4 | OLD
vegas | pgstattuple | 1.4 | 1.4 | <no value>
vegas | plpgsql | 1.0 | 1.0 | <no value>
vividcortex | plpgsql | 1.0 | 1.0 | <no value>



## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_A006"></a>
[Table of contents](#postgres-checkup_top)
# A006 Postgres setting deviations #

## Observations ##

### Settings (pg_settings) that differ ###

&#9660;&nbsp;Setting | db2 | db3 
--------|-------|-------- 
pg_stat_statements.track_utility  | on | off
transaction_read_only  | on | off
work_mem  | 128.00 MiB | 64.00 MiB



No differences in `pg_config` are found.



## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_A007"></a>
[Table of contents](#postgres-checkup_top)
# A007 Altered settings #

## Observations ##


### Master (`db2`) ###
Source | Settings count | Changed settings
-------|----------------|-----------------
/etc/postgresql/9.6/main/postgresql.conf | 74 |  archive_command archive_mode archive_timeout auto_explain.log_analyze auto_explain.log_buffers auto_explain.log_format auto_explain.log_min_duration auto_explain.log_nested_statements auto_explain.log_timing auto_explain.log_triggers auto_explain.log_verbose autovacuum_analyze_scale_factor autovacuum_max_workers autovacuum_vacuum_scale_factor checkpoint_completion_target cluster_name DateStyle default_statistics_target dynamic_shared_memory_type enable_seqscan escape_string_warning external_pid_file from_collapse_limit hot_standby hot_standby_feedback idle_in_transaction_session_timeout join_collapse_limit lc_messages lc_monetary lc_numeric lc_time listen_addresses log_autovacuum_min_duration log_checkpoints log_connections log_destination log_disconnections log_duration logging_collector log_line_prefix log_lock_waits log_min_duration_statement log_min_error_statement log_min_messages log_replication_commands log_rotation_size log_temp_files log_timezone maintenance_work_mem max_connections max_replication_slots max_wal_senders max_wal_size pg_stat_statements.max pg_stat_statements.save port random_page_cost shared_buffers shared_preload_libraries ssl ssl_cert_file ssl_key_file stats_temp_directory synchronous_commit temp_buffers TimeZone track_activities track_counts track_functions track_io_timing unix_socket_directories wal_keep_segments wal_level wal_writer_delay  
/var/lib/postgresql/9.6/main/postgresql.auto.conf | 3 |  effective_cache_size pg_stat_statements.track_utility work_mem  
default | 198 | 




### Replica servers: ###
  
#### Replica (`db3`) ####
    
Source | Settings count | Changed settings
-------|----------------|-----------------
/etc/postgresql/9.6/main/postgresql.conf | 76 |  archive_command archive_mode archive_timeout auto_explain.log_analyze auto_explain.log_buffers auto_explain.log_format auto_explain.log_min_duration auto_explain.log_nested_statements auto_explain.log_timing auto_explain.log_triggers auto_explain.log_verbose autovacuum_analyze_scale_factor autovacuum_max_workers autovacuum_vacuum_scale_factor checkpoint_completion_target cluster_name DateStyle default_statistics_target dynamic_shared_memory_type effective_cache_size enable_seqscan escape_string_warning external_pid_file from_collapse_limit hot_standby hot_standby_feedback idle_in_transaction_session_timeout join_collapse_limit lc_messages lc_monetary lc_numeric lc_time listen_addresses log_autovacuum_min_duration log_checkpoints log_connections log_disconnections log_duration logging_collector log_line_prefix log_lock_waits log_min_duration_statement log_min_error_statement log_min_messages log_replication_commands log_rotation_size log_temp_files log_timezone maintenance_work_mem max_connections max_replication_slots max_wal_senders max_wal_size pg_stat_statements.max pg_stat_statements.save pg_stat_statements.track_utility port random_page_cost shared_buffers shared_preload_libraries ssl ssl_cert_file ssl_key_file stats_temp_directory synchronous_commit temp_buffers TimeZone track_activities track_counts track_functions track_io_timing unix_socket_directories wal_keep_segments wal_level wal_writer_delay work_mem  
default | 199 | 
    

## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_A008"></a>
[Table of contents](#postgres-checkup_top)
# A008 Disk usage and file system type
Output of `df -TPh` (follows symlinks)

## Observations ##


### Master (`db2`) ###
Name | FS Type | Size | Available | Use | Used | Mount Point | Path | Device
-----|---------|------|-----------|-----|------|-------------|------|-------
PGDATA | ext4 | 5.4T | 4.0T | 1.1T | 22% | /var/lib/postgresql | /var/lib/postgresql/9.6/main | /dev/mapper/cachedev0 
WAL directory | ext4 | 5.4T | 4.0T | 1.1T | 22% | /var/lib/postgresql | /var/lib/postgresql/9.6/main/pg_xlog | /dev/mapper/cachedev0 
stats_temp_directory | tmpfs | 32G | 32G | 3.1M | 1% | /dev/shm | /dev/shm | tmpfs 
log_directory | ext4 | 5.4T | 4.0T | 1.1T | 22% | /var/lib/postgresql | /var/lib/postgresql/9.6/main/pg_log | /dev/mapper/cachedev0 





### Replica servers: ###
  
#### Replica (`db3`) ####
Name | FS Type | Size | Available | Use | Used | Mount Point | Path | Device
-----|---------|------|-----------|-----|------|-------------|------|-------
PGDATA | ext4 | 5.4T | 4.0T | 1.1T | 22% | /var/lib/postgresql | /var/lib/postgresql/9.6/main | /dev/mapper/cachedev0  
WAL directory | ext4 | 5.4T | 4.0T | 1.1T | 22% | /var/lib/postgresql | /var/lib/postgresql/9.6/main/pg_xlog | /dev/mapper/cachedev0  
stats_temp_directory | tmpfs | 32G | 32G | 420K | 1% | /dev/shm | /dev/shm | tmpfs  
log_directory | ext4 | 5.4T | 4.0T | 1.1T | 22% | /var/lib/postgresql | /var/lib/postgresql/9.6/main/pg_log | /dev/mapper/cachedev0  





## Conclusions ##

## Recommendations ##
---
<a name="postgres-checkup_D002"></a>
[Table of contents](#postgres-checkup_top)
# D002 Useful Linux tools
## Observations ##


### Master (`db2`) ###
### cpu
Utility | Availability
--------|--------------
ps | yes
htop | 
top | yes
mpstat | yes
lscpu | yes
### free_space
Utility | Availability
--------|--------------
df | yes
du | yes
### io
Utility | Availability
--------|--------------
pidstat | yes
iostat | yes
iotop | yes
ftrace | 
blktrace | 
### memory
Utility | Availability
--------|--------------
free | yes
vmstat | yes
### network
Utility | Availability
--------|--------------
tcpdump | yes
netstat | yes
ss | yes
iptraf | 
ethtool | yes
### misc
Utility | Availability
--------|--------------
dstat | yes
strace | yes
ltrace | 
perf | yes
numastat | yes




### Replica servers:  

#### Replica (`db3`)  

### cpu
Utility | Availability
--------|--------------
ps | yes
htop | 
top | yes
mpstat | yes
lscpu | yes
### free_space
Utility | Availability
--------|--------------
df | yes
du | yes
### io
Utility | Availability
--------|--------------
pidstat | yes
iostat | yes
iotop | yes
ftrace | 
blktrace | 
### memory
Utility | Availability
--------|--------------
free | yes
vmstat | yes
### network
Utility | Availability
--------|--------------
tcpdump | yes
netstat | yes
ss | yes
iptraf | 
ethtool | yes
### misc
Utility | Availability
--------|--------------
dstat | yes
strace | yes
ltrace | 
perf | 
numastat | 





## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_D004"></a>
[Table of contents](#postgres-checkup_top)
# D004 pg_stat_statements and kcache settings #

## Observations ##


### Master (`db2`) ###

#### `pg_stat_statements` extension settings ####
Setting | Value | Unit | Type | Min value | Max value
--------|-------|------|------|-----------|-----------
[pg_stat_statements.max](https://postgresqlco.nf/en/doc/param/pg_stat_statements.max)|500||integer|100 |2147483647 
[pg_stat_statements.save](https://postgresqlco.nf/en/doc/param/pg_stat_statements.save)|on||bool||
[pg_stat_statements.track](https://postgresqlco.nf/en/doc/param/pg_stat_statements.track)|all||enum||
[pg_stat_statements.track_utility](https://postgresqlco.nf/en/doc/param/pg_stat_statements.track_utility)|off||bool||

#### `kcache` extension settings ####
Setting | Value | Unit | Type | Min value | Max value
--------|-------|------|------|-----------|-----------
[pg_stat_kcache.linux_hz](https://postgresqlco.nf/en/doc/param/pg_stat_kcache.linux_hz)|1000000||integer|-1 |2147483647 


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_F001"></a>
[Table of contents](#postgres-checkup_top)
# F001 Autovacuum: Current settings #

## Observations ##


### Master (`db2`) ###
&#9660;&nbsp;Setting name | Value | Unit | Pretty value
-------------|-------|------|--------------
[autovacuum](https://postgresqlco.nf/en/doc/param/autovacuum)|on|<no value> | 
[autovacuum_analyze_scale_factor](https://postgresqlco.nf/en/doc/param/autovacuum_analyze_scale_factor)|0.05|<no value> | 
[autovacuum_analyze_threshold](https://postgresqlco.nf/en/doc/param/autovacuum_analyze_threshold)|50|<no value> | 
[autovacuum_freeze_max_age](https://postgresqlco.nf/en/doc/param/autovacuum_freeze_max_age)|200000000|<no value> | 
[autovacuum_max_workers](https://postgresqlco.nf/en/doc/param/autovacuum_max_workers)|10|<no value> | 
[autovacuum_multixact_freeze_max_age](https://postgresqlco.nf/en/doc/param/autovacuum_multixact_freeze_max_age)|400000000|<no value> | 
[autovacuum_naptime](https://postgresqlco.nf/en/doc/param/autovacuum_naptime)|60|s | 
[autovacuum_vacuum_cost_delay](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_cost_delay)|20|ms | 
[autovacuum_vacuum_cost_limit](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_cost_limit)|-1|<no value> | 
[autovacuum_vacuum_scale_factor](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_scale_factor)|0.01|<no value> | 
[autovacuum_vacuum_threshold](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_threshold)|50|<no value> | 
[autovacuum_work_mem](https://postgresqlco.nf/en/doc/param/autovacuum_work_mem)|-1|kB | 
[maintenance_work_mem](https://postgresqlco.nf/en/doc/param/maintenance_work_mem)|2097152|kB | 2.00 GiB
[vacuum_cost_delay](https://postgresqlco.nf/en/doc/param/vacuum_cost_delay)|0|ms | 
[vacuum_cost_limit](https://postgresqlco.nf/en/doc/param/vacuum_cost_limit)|200|<no value> | 
[vacuum_cost_page_dirty](https://postgresqlco.nf/en/doc/param/vacuum_cost_page_dirty)|20|<no value> | 
[vacuum_cost_page_hit](https://postgresqlco.nf/en/doc/param/vacuum_cost_page_hit)|1|<no value> | 
[vacuum_cost_page_miss](https://postgresqlco.nf/en/doc/param/vacuum_cost_page_miss)|10|<no value> | 
[vacuum_defer_cleanup_age](https://postgresqlco.nf/en/doc/param/vacuum_defer_cleanup_age)|0|<no value> | 
[vacuum_freeze_min_age](https://postgresqlco.nf/en/doc/param/vacuum_freeze_min_age)|50000000|<no value> | 
[vacuum_freeze_table_age](https://postgresqlco.nf/en/doc/param/vacuum_freeze_table_age)|150000000|<no value> | 
[vacuum_multixact_freeze_min_age](https://postgresqlco.nf/en/doc/param/vacuum_multixact_freeze_min_age)|5000000|<no value> | 
[vacuum_multixact_freeze_table_age](https://postgresqlco.nf/en/doc/param/vacuum_multixact_freeze_table_age)|150000000|<no value> | 






### Replicas settings ###
Setting | db3 
--------|-------- 
[hot_standby_feedback](https://postgresqlco.nf/en/doc/param/hot_standby_feedback)| on


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_F002"></a>
[Table of contents](#postgres-checkup_top)
# F002 Autovacuum: Transaction wraparound check #

## Observations ##


### Master (`db2`) ###

#### Databases ####
 Database | &#9660;&nbsp;Age | Capacity used, % | Warning | datfrozenxid
----------|-----|------------------|---------|--------------
test |198853684 |10.2 |  |2614674968
fffail_ru |191660905 |9.83 |  |2621867747
startupturbo_com_old |187925517 |9.64 |  |2625603135
test2 |187610748 |9.62 |  |2625917904
postila_ru |184037743 |9.44 |  |2629490909
_4vkusa_ru |183071865 |9.39 |  |2630456787
kotomail_ru |167906845 |8.61 |  |2645621807
foto_prikolisti_com |163888030 |8.4 |  |2649640622
vegas |160854201 |8.25 |  |2652674451
tks_f |160925787 |8.25 |  |2652602865
fotominuta_ru |160925787 |8.25 |  |2652602865
startupturbo_com |160778516 |8.25 |  |2652750136
svoi_simplead_ru |160862242 |8.25 |  |2652666410
nevada |160925787 |8.25 |  |2652602865
prikolisti_com |160854201 |8.25 |  |2652674451
nevada7 |160885135 |8.25 |  |2652643517
nevada8 |160885135 |8.25 |  |2652643517
postfix |160885135 |8.25 |  |2652643517
postgres |160885135 |8.25 |  |2652643517
postgresmen_ru |160885135 |8.25 |  |2652643517
nevada4 |160885135 |8.25 |  |2652643517
strakhovm_ru |160888838 |8.25 |  |2652639814
nevada5 |160775776 |8.24 |  |2652752876
nevada6 |160738516 |8.24 |  |2652790136
tpl_drupal |160775776 |8.24 |  |2652752876
redash |143901236 |7.38 |  |2669627416
dba |137651441 |7.06 |  |2675877211
demotivator_prikolisti_com |133863137 |6.86 |  |2679665515
nevada3 |115791568 |5.94 |  |2697737084
nevada1 |115587540 |5.93 |  |2697941112
nevada2 |115416516 |5.92 |  |2698112136
nombox_co |101372225 |5.2 |  |2712156427
vividcortex |96404115 |4.94 |  |2717124537
template1 |87651441 |4.49 |  |2725877211
template0 |60971398 |3.13 |  |2752557254



#### Tables in the observed database ####
 Relation | Age | &#9660;&nbsp;Capacity used, % | Warning |rel_relfrozenxid | toast_relfrozenxid 
----------|-----|------------------|---------|-----------------|--------------------
pnct_PicturePost |184037743 |9.44 |  |2629490909 |2634093293 |
pnct_Board |181137123 |9.29 |  |2734413145 |2632391529 |
tmp.erotic_posts_20170911 |179375158 |9.2 |  |2634153494 |2634153494 |
archive.issue_3141_posts_wo_domain |179375158 |9.2 |  |2634153494 |2634153494 |
archive.spam_20140725_posts_issue_937 |179375158 |9.2 |  |2634153494 |2634153494 |
archive.users_bounced_in_201605 |179375158 |9.2 |  |2634153494 |2634153494 |
antispam.followuser_20160312 |179375158 |9.2 |  |2634153494 |0 |
user_visits |179375158 |9.2 |  |2717312653 |2634153494 |
archive.banned_boards_20150105 |179375158 |9.2 |  |2634153494 |2634153494 |
archive.board_restore_20160504_antiporn_epicfail |179375158 |9.2 |  |2634153494 |2634153494 |
archive.spam_20140721_posts |179375158 |9.2 |  |2634153494 |2634153494 |
archive.spam_20140722_posts_2 |179375158 |9.2 |  |2634153494 |2634153494 |
archive.spam_20140728_posts_1 |179375158 |9.2 |  |2634153494 |2634153494 |
archive.users_banned_0diet_20150829 |179375158 |9.2 |  |2634153494 |2634153494 |
ban_0diet_20151002 |179375158 |9.2 |  |2634153494 |2634153494 |
anna_preds |179375158 |9.2 |  |2634153494 |0 |
antispam.followuser_20160305 |179375158 |9.2 |  |2634153494 |0 |
antispam.followuser_20160309 |179375158 |9.2 |  |2634153494 |0 |
archive.abuse_snapshot_20141001 |179375158 |9.2 |  |2634153494 |2634153494 |
archive.antispam_snapshot_board_20140723 |179375158 |9.2 |  |2634153494 |2634153494 |
pnct_unsubscribe_mail |179375158 |9.2 |  |2634153494 |2634153494 |
archive.banned_posts_20150105 |179375158 |9.2 |  |2634153494 |2634153494 |
archive.pnct_PictureComment |179375158 |9.2 |  |2634153494 |2634153494 |
archive.pnct_User |179375158 |9.2 |  |2634153494 |2634153494 |
pnct_StorageBin |179375158 |9.2 |  |2634153494 |2634153494 |
archive.spam_20140722_posts |179375158 |9.2 |  |2634153494 |2634153494 |
pnct_PictureReport |179375158 |9.2 |  |2634153494 |2634153494 |
archive.spam_20140727_posts_1 |179375158 |9.2 |  |2634153494 |2634153494 |
archive.spam_snapshot_20141025 |179375158 |9.2 |  |2634153494 |2634153494 |
pnct_NetworkRequest |179375158 |9.2 |  |2634153494 |2634153494 |
topic |179375158 |9.2 |  |2634153494 |2634153494 |
antispam.followuser_20160302 |179375158 |9.2 |  |2634153494 |0 |
ban_20151004 |179375158 |9.2 |  |2634153494 |2634153494 |
pnct_BoardUser |179375158 |9.2 |  |2634153494 |0 |
_popular_search_queries_current_week |179375158 |9.2 |  |2634153494 |2634153494 |
pnct_Ads |179375158 |9.2 |  |2634153494 |2634153494 |
antispam.followuser_20160312_2 |179375158 |9.2 |  |2634153494 |0 |
pnct_PictureCategory |179375158 |9.2 |  |2634153494 |2634153494 |
post_view |179375158 |9.2 |  |2634153494 |2634153494 |
archive.abuse_board_20150208 |179375158 |9.2 |  |2634153494 |2634153494 |
pnct_PictureCommentReport |179375158 |9.2 |  |2634153494 |2634153494 |
archive.abuse_snapshot_20150208 |179375158 |9.2 |  |2634153494 |2634153494 |
_tmp_facebook_contacts |179375158 |9.2 |  |2634153494 |0 |
pnct_AnyPost |179405334 |9.2 |  |2717312653 |2634123318 |
pnct_Video |179375158 |9.2 |  |2634153494 |2634153494 |
kcache_avail |179375158 |9.2 |  |2634153494 |0 |
_tmp_fill_b_selector |179375158 |9.2 |  |2634153494 |2634153494 |
archive.likes_userecho_273376 |179375158 |9.2 |  |2634153494 |2634153494 |
v1.search_popular_with_gender |179375158 |9.2 |  |2750466341 |2634153494 |
tmp.hosts_views |179375158 |9.2 |  |2634153494 |2634153494 |




## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_F003"></a>
[Table of contents](#postgres-checkup_top)
# F003 Autovacuum: Dead tuples #

## Observations ##


### Master (`db2`) ###
Stats reset: 2 mons 6 days 20:54:00 ago (2018-12-05 17:44:47 +0000 +0000)  
Report created: 2019-02-12 17:38:42 +0300 MSK  

 Relation | Type | Since last autovacuum | Since last vacuum | Autovacuum Count | Vacuum Count | n_tup_ins | n_tup_upd | n_tup_del | pg_class.reltuples | n_live_tup | n_dead_tup | &#9660;Dead Tuples Ratio, %
----------|------|-----------------------|-------------------|----------|---------|-----------|-----------|-----------|--------------------|------------|------------|-----------
pg_toast.pg_toast_2619 |t |02:42:04.60075 |17 days 15:20:09.764676 |697 |2 |160116 |0 |159455 |18554 |18556 |227 |1.21
translation_proxy.cache |r |17 days 19:02:03.877645 |17 days 13:28:28.286572 |3 |1 |661882 |0 |0 |19965288 |19965287 |181438 |0.9
pg_catalog.pg_attribute |r |02:38:04.43642 |17 days 15:19:15.094711 |1042 |2 |113526 |2 |112790 |12584 |12584 |100 |0.79
pnct_PicturePost |r |37 days 02:16:29.118721 |17 days 12:43:33.834491 |2 |3 |1013606 |1736007 |188410 |58061504 |58260199 |450883 |0.77
pnct_User |r |1 day 08:24:41.68896 |17 days 15:19:40.285942 |28 |4 |68983 |4015871 |0 |6756024 |6757763 |46104 |0.68
pnct_Board |r |03:07:14.850134 |17 days 14:39:17.37272 |116 |3 |11242 |1853271 |1388 |1250655 |1250564 |7386 |0.59
pnct_AnyBoard |r |03:29:03.626231 |17 days 13:21:57.227413 |189 |2 |1095 |60115 |80 |26476 |26481 |154 |0.58
pg_catalog.pg_depend |r |05:38:27.641211 |17 days 15:20:07.887788 |148 |2 |35122 |0 |34849 |18718 |18703 |90 |0.48
news_daily_edit |r |37 days 08:32:25.273473 |17 days 14:38:51.871904 |2 |1 |683 |691 |7 |15353 |15531 |43 |0.28
pg_toast.pg_toast_33859736 |t |37 days 06:48:59.680425 |17 days 12:43:33.795486 |2 |3 |18645 |0 |1765 |220923 |225196 |547 |0.24
pnct_network_follow |r |4 days 17:53:15.955547 |17 days 14:16:19.81101 |4 |1 |1827357 |7849333 |104901 |265945696 |266091060 |565283 |0.21
pnct_AnyPost |r |6 days 05:28:45.417335 |17 days 15:13:36.350728 |4 |3 |103058 |47704 |7047 |2277859 |2282912 |4743 |0.21
post_view |r |37 days 00:50:23.407301 |17 days 14:45:34.936285 |2 |1 |102277808 |73739255 |27951 |1022339520 |1049097065 |1873287 |0.18
conveyor_failed |r |37 days 08:25:07.741974 |17 days 12:33:17.279521 |2 |1 |727902 |0 |0 |2463870 |2546766 |3099 |0.12
token |r |37 days 08:30:28.389792 |17 days 14:38:55.216192 |2 |1 |33994 |40799 |0 |2420534 |2422482 |1234 |0.05
pnct_unsubscribe_mail |r |37 days 08:40:21.280892 |17 days 15:19:10.053229 |2 |2 |14662 |0 |413 |349493 |353154 |115 |0.03
pg_toast.pg_toast_58152185 |t |27 days 08:53:54.121977 |17 days 15:13:36.34624 |3 |3 |954 |0 |30 |15919 |16280 |4 |0.02
pnct_NetworkFollowUser |r |6 days 04:36:04.019547 |17 days 11:48:15.150769 |4 |1 |249787 |0 |89836 |25535906 |25549803 |5816 |0.02
pnct_UserNotify |r |37 days 08:37:41.391833 |17 days 13:36:36.578712 |2 |1 |10573 |8206 |768 |1544009 |1546457 |377 |0.02
pnct_NetworkFollowBoard |r |6 days 05:28:10.743852 |17 days 14:39:47.007901 |4 |1 |223146 |0 |16897 |16514268 |16533379 |1655 |0.01
pnct_Hash |r |37 days 06:44:08.523681 |17 days 11:49:50.026231 |2 |1 |843 |0 |304 |1434143 |1434295 |76 |0.01
proxy_content |r |37 days 08:32:26.247675 |17 days 14:38:50.042943 |2 |1 |945 |0 |0 |28372 |28598 |2 |0.01
pnct_PictureLike |r |37 days 08:23:15.421745 |17 days 13:02:11.274318 |2 |1 |125128 |0 |28934 |18588216 |18617267 |2177 |0.01
antispam.money_fraud201801_followuser |r |37 days 07:36:53.523825 |17 days 13:22:04.645633 |2 |1 |0 |0 |0 |135917 |135917 |0 |0
jobs_translate.mv_titles_new2lock |r |37 days 08:37:34.297332 |17 days 14:39:26.309952 |2 |1 |0 |0 |0 |18219 |18219 |0 |0
spammers20181125.pnct_network_follow |r |32 days 23:50:07.229568 |17 days 14:38:48.479196 |2 |1 |104129 |0 |0 |1535485 |1535485 |0 |0
ml.posts_fresh_predict_fplus |r |26 days 07:20:23.275186 |17 days 15:13:40.703609 |3 |2 |0 |0 |0 |11904 |11904 |0 |0
job_move_old_images |r |37 days 07:37:47.769471 |17 days 13:30:16.393226 |2 |1 |0 |0 |0 |8633436 |8633436 |0 |0
archive.pnct_PicturePost |r |37 days 08:28:56.075643 |17 days 13:22:46.233667 |2 |1 |188410 |0 |0 |5750815 |5822544 |0 |0
tmp_is_male_all |r |37 days 07:59:56.311451 |17 days 13:36:39.907822 |2 |1 |0 |0 |0 |189536 |189536 |0 |0
pnct_StorageFile |r |27 days 09:10:19.87861 |17 days 14:38:27.905584 |3 |1 |13032 |0 |0 |44280224 |44283563 |0 |0
postgresql_dba.normalyzed_query |r |37 days 08:40:56.542246 |17 days 15:19:08.821132 |2 |2 |0 |0 |0 |11986 |11986 |0 |0
job_move_old_binimages |r |37 days 07:56:49.47052 |17 days 13:29:36.637252 |2 |1 |0 |0 |0 |3129056 |3129056 |0 |0
tmp.male_boards |r |37 days 08:37:50.83338 |17 days 14:39:44.744856 |2 |1 |0 |0 |0 |125933 |125933 |0 |0
user_directory |r |37 days 08:36:52.027704 |17 days 14:39:07.583125 |2 |1 |0 |0 |0 |287682 |287682 |0 |0
archive.kino_i_tpm__pirate_video |r |37 days 08:37:50.884458 |17 days 14:39:44.772319 |2 |1 |0 |0 |0 |17566 |17566 |0 |0
jobs_translate.mv_messages_new2lock |r |37 days 08:37:32.079386 |17 days 14:39:27.988759 |2 |1 |0 |0 |0 |466980 |466980 |0 |0
pnct_NetworkInvite |r |37 days 08:37:16.542153 |17 days 14:39:30.996683 |2 |1 |0 |0 |0 |1396312 |1396312 |0 |0
tmp.male_boards_latest_post |r |37 days 08:36:47.147444 |17 days 14:39:05.060212 |2 |1 |0 |0 |0 |125930 |125930 |0 |0
_gt_calc_title |r |37 days 08:32:28.220497 |17 days 14:38:51.343021 |2 |1 |0 |0 |0 |332538 |332538 |0 |0
ml.posts_fresh |r |26 days 07:20:23.312189 |17 days 15:19:07.919312 |3 |2 |0 |0 |0 |11904 |11904 |0 |0
pnct_StorageBin |r |37 days 08:39:21.432029 |17 days 15:19:13.588113 |2 |2 |12975 |19 |0 |13299163 |13302487 |0 |0
archive.pnct_Board |r |37 days 07:59:54.980636 |17 days 13:36:38.30055 |2 |1 |1388 |0 |0 |85506 |85836 |0 |0
tmp.hosts_views_lastmonth |r |37 days 08:37:42.57246 |17 days 14:39:28.7194 |2 |1 |0 |0 |0 |235103 |235103 |0 |0
_popular_search_queries_current_week |r |37 days 08:40:59.500353 |17 days 15:20:09.687966 |2 |2 |0 |0 |0 |28020 |28020 |0 |0
jobs_translate.cache_20170522 |r |37 days 08:37:49.822005 |17 days 14:39:41.586269 |2 |1 |0 |0 |0 |17148 |17148 |0 |0
google_translate.cache_orig |r |37 days 08:29:59.494835 |17 days 14:12:10.401701 |2 |1 |0 |0 |0 |6017689 |6017689 |0 |0
action |r |7 days 04:48:07.809117 |17 days 13:34:24.989916 |3 |1 |4998212 |0 |0 |39283724 |39782888 |0 |0
tmp.hosts_views |r |37 days 08:40:55.729425 |17 days 15:13:38.625698 |2 |2 |0 |0 |0 |235096 |235096 |0 |0
pnct_Picture |r |37 days 08:34:18.574378 |17 days 14:39:39.454515 |2 |1 |19 |0 |0 |9483014 |9483019 |0 |0



## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_F004"></a>
[Table of contents](#postgres-checkup_top)
# F004 Autovacuum: Heap bloat #
:warning: This report is based on estimations. The errors in bloat estimates may be significant (in some cases, up to 15% and even more). Use it only as an indicator of potential issues.

## Observations ##


### Master (`db2`) ###

 Table | Size | Extra | &#9660;&nbsp;Estimated bloat | Est. bloat, bytes | Est. bloat ratio,% | Live | Last vacuum | Fillfactor
-------|------|-------|------------------------------|------------------|--------------------|------|-------------|------------
**Total** | **405.69 GiB** ||**16.47 GiB** |**17,679,253,504** ||||
bot_visit |57.40 GiB |~6.61 GiB (11.50%)|6.61 GiB |7,089,266,688 |11.50 |~50.80 GiB | 2019-02-06 11:39:00 (auto)  |100
post_subtitles |110.92 GiB |~3.17 GiB (2.85%)|3.17 GiB |3,398,672,384 |2.85 |~107.76 GiB | 2019-02-07 12:17:37 (auto)  |100
pnct_network_follow |38.83 GiB |~2.17 GiB (5.56%)|2.17 GiB |2,319,433,728 |5.56 |~36.67 GiB | 2019-02-07 20:45:27 (auto)  |100
pnct_PicturePost |32.02 GiB |~1.03 GiB (3.21%)|1.03 GiB |1,102,553,088 |3.21 |~31.00 GiB | 2019-01-26 01:55:09  |100
user_visits |14.30 GiB |~754.04 MiB (5.15%)|754.04 MiB |790,659,072 |5.15 |~13.56 GiB | 2019-02-06 10:01:26 (auto)  |100
pnct_NetworkEvent |15.51 GiB |~542.56 MiB (3.42%)|542.56 MiB |568,909,824 |3.42 |~14.98 GiB | 2019-01-26 02:53:56  |100
pnct_User |2.41 GiB |~473.28 MiB (19.19%)|473.28 MiB |496,263,168 |19.19 |~1.95 GiB | 2019-02-11 06:14:02 (auto)  |100
search_log |8.17 GiB |~392.11 MiB (4.69%)|392.11 MiB |411,156,480 |4.69 |~7.79 GiB | 2019-02-06 09:19:00 (auto)  |100
action |14.00 GiB |~385.93 MiB (2.69%)|385.93 MiB |404,668,416 |2.69 |~13.62 GiB | 2019-02-05 09:50:35 (auto)  |100
translation_proxy.cache |6.12 GiB |~248.75 MiB (3.97%)|248.75 MiB |260,825,088 |3.97 |~5.88 GiB | 2019-01-26 01:10:15  |100
pnct_StorageFile |5.60 GiB |~163.04 MiB (2.85%)|163.04 MiB |170,958,848 |2.85 |~5.44 GiB | 2019-01-26 00:00:15  |100
archive.pnct_PicturePost |3.40 GiB |~113.97 MiB (3.28%)|113.97 MiB |119,496,704 |3.28 |~3.29 GiB | 2019-01-26 01:15:57  |100
pnct_Board |263.86 MiB |~84.35 MiB (31.97%)|84.35 MiB |88,440,832 |31.97 |~179.51 MiB | 2019-02-12 11:31:28 (auto)  |100
conveyor_failed |1.55 GiB |~82.55 MiB (5.22%)|82.55 MiB |86,556,672 |5.22 |~1.47 GiB | 2019-01-26 02:05:26  |100
archive.log |538.95 MiB |~71.14 MiB (13.20%)|71.14 MiB |74,588,160 |13.20 |~467.82 MiB | 2019-02-06 09:03:35 (auto)  |100
pnct_AnyPost |1.38 GiB |~62.94 MiB (4.47%)|62.94 MiB |65,994,752 |4.47 |~1.32 GiB | 2019-02-06 09:09:58 (auto)  |100
token |370.66 MiB |~30.30 MiB (8.17%)|30.30 MiB |31,768,576 |8.17 |~340.36 MiB | 2019-01-25 23:59:48  |100
pnct_NetworkFollowUser |1.46 GiB |~26.91 MiB (1.80%)|26.91 MiB |28,213,248 |1.80 |~1.44 GiB | 2019-02-06 10:02:39 (auto)  |100
archive.pnct_AnyPost |565.84 MiB |~20.81 MiB (3.68%)|20.81 MiB |21,815,296 |3.68 |~545.04 MiB | 2019-02-06 08:58:53 (auto)  |100
pnct_StorageBin |783.30 MiB |~20.08 MiB (2.56%)|20.08 MiB |21,045,248 |2.56 |~763.23 MiB | 2019-01-25 23:19:30  |100
ml.posts_fresh |31.14 MiB |~17.95 MiB (57.64%)|17.95 MiB |18,817,024 |57.64 |~13.19 MiB | 2019-01-25 23:19:35  |100
job_move_old_images |638.64 MiB |~17.21 MiB (2.69%)|17.21 MiB |18,038,784 |2.69 |~621.43 MiB | 2019-01-26 01:08:27  |100
google_translate.cache_orig |1.61 GiB |~13.31 MiB (0.81%)|13.31 MiB |13,950,976 |0.81 |~1.60 GiB | 2019-01-26 00:26:33  |100
postfix.list_4_mar_2018 |128.25 MiB |~11.52 MiB (8.98%)|11.52 MiB |12,075,008 |8.98 |~116.74 MiB | 2019-01-26 01:08:52  |100
pnct_NetworkFollowBoard |0.94 GiB |~9.43 MiB (0.99%)|9.43 MiB |9,887,744 |0.99 |~947.74 MiB | 2019-02-06 09:10:33 (auto)  |100
job_move_old_binimages |329.29 MiB |~7.41 MiB (2.25%)|7.41 MiB |7,766,016 |2.25 |~321.88 MiB | 2019-01-26 01:09:07  |100
tmp.male_boards_latest_post |60.41 MiB |~5.99 MiB (9.91%)|5.99 MiB |6,275,072 |9.91 |~54.43 MiB | 2019-01-25 23:59:38  |100
archive.antispam_snapshot_board_20140723 |70.25 MiB |~4.85 MiB (6.90%)|4.85 MiB |5,079,040 |6.90 |~65.41 MiB | 2019-01-25 23:18:18  |100
pnct_PictureFragment |589.54 MiB |~4.33 MiB (0.73%)|4.33 MiB |4,538,368 |0.73 |~585.21 MiB | 2019-01-26 01:02:39  |100
jobs_translate.cache_20170526_1 |80.90 MiB |~3.66 MiB (4.52%)|3.66 MiB |3,833,856 |4.52 |~77.25 MiB | 2019-01-25 23:59:31  |100
board_directory |31.53 MiB |~3.62 MiB (11.47%)|3.62 MiB |3,792,896 |11.47 |~27.91 MiB | 2019-01-25 23:59:34  |100
pnct_AnyBoard |6.68 MiB |~2.72 MiB (40.63%)|2.72 MiB |2,842,624 |40.63 |~3.97 MiB | 2019-02-12 11:09:40 (auto)  |100
pnct_PictureComment |44.61 MiB |~1.88 MiB (4.20%)|1.88 MiB |1,966,080 |4.20 |~42.74 MiB | 2019-02-06 08:58:11 (auto)  |100
postgresql_dba.normalyzed_query |21.24 MiB |~1.74 MiB (8.17%)|1.74 MiB |1,818,624 |8.17 |~19.50 MiB | 2019-01-25 23:19:34  |100
user_directory |35.71 MiB |~1.68 MiB (4.70%)|1.68 MiB |1,761,280 |4.70 |~34.03 MiB | 2019-01-25 23:59:36  |100
tmp.checktranslate |14.61 MiB |~1.43 MiB (9.73%)|1.43 MiB |1,490,944 |9.73 |~13.19 MiB | 2019-01-26 01:16:43  |100
pnct_NetworkInvite |145.46 MiB |~1.22 MiB (0.83%)|1.22 MiB |1,269,760 |0.83 |~144.25 MiB | 2019-01-25 23:59:12  |100
archive.pnct_Board |14.77 MiB |~1.13 MiB (7.62%)|1.13 MiB |1,179,648 |7.62 |~13.65 MiB | 2019-01-26 01:02:05  |100
archive.banned_posts_20150105 |15.34 MiB |~0.98 MiB (6.37%)|0.98 MiB |1,024,000 |6.37 |~14.36 MiB | 2019-01-25 23:18:24  |100
archive.pnct_User |16.43 MiB |~0.97 MiB (5.85%)|0.97 MiB |1,007,616 |5.85 |~15.47 MiB | 2019-01-25 23:18:23  |100
tmp.msai_posts3 |6.55 MiB |~976.00 KiB (14.56%)|976.00 KiB |999,424 |14.56 |~5.60 MiB | 2019-01-26 01:16:52  |100
tmp.hosts_views |15.95 MiB |~896.00 KiB (5.49%)|896.00 KiB |917,504 |5.49 |~15.08 MiB | 2019-01-25 23:25:05  |100
jobs_translate.mv_messages_new2lock |77.47 MiB |~824.00 KiB (1.04%)|824.00 KiB |843,776 |1.04 |~76.66 MiB | 2019-01-25 23:59:15  |100
ml.posts_fresh_predict_fplus |14.33 MiB |~768.00 KiB (5.24%)|768.00 KiB |786,432 |5.24 |~13.58 MiB | 2019-01-25 23:25:03  |100
ml.posts_fresh_predict_m |14.33 MiB |~760.00 KiB (5.18%)|760.00 KiB |778,240 |5.18 |~13.58 MiB | 2019-01-25 23:59:09  |100
pnct_Picture |617.40 MiB |~624.00 KiB (0.10%)|624.00 KiB |638,976 |0.10 |~616.79 MiB | 2019-01-25 23:59:04  |100
conveyor |1.10 MiB |~424.00 KiB (37.86%)|424.00 KiB |434,176 |37.86 |~696.00 KiB | 2019-02-12 14:32:53 (auto)  |100
ml.kusochek_learndata |6.49 MiB |~352.00 KiB (5.30%)|352.00 KiB |360,448 |5.30 |~6.15 MiB | 2019-01-25 23:59:54  |100
pnct_Hash |115.52 MiB |~296.00 KiB (0.25%)|296.00 KiB |303,104 |0.25 |~115.23 MiB | 2019-01-26 02:48:53  |100
pnct_Autosourcer |440.00 KiB |~272.00 KiB (61.82%)|272.00 KiB |278,528 |61.82 |~168.00 KiB | 2019-02-12 14:30:52 (auto)  |100
pnct_UserNotify |77.07 MiB |~272.00 KiB (0.34%)|272.00 KiB |278,528 |0.34 |~76.80 MiB | 2019-01-26 01:02:07  |100
news_daily_edit |5.72 MiB |~264.00 KiB (4.51%)|264.00 KiB |270,336 |4.51 |~5.46 MiB | 2019-01-25 23:59:51  |100
host |16.25 MiB |~232.00 KiB (1.39%)|232.00 KiB |237,568 |1.39 |~16.02 MiB | 2019-01-26 02:04:31  |100
tmp.img_cannot_save |472.00 KiB |~232.00 KiB (49.15%)|232.00 KiB |237,568 |49.15 |~240.00 KiB | 2019-01-26 02:04:33  |100
jobs_translate.cache_20170522 |4.60 MiB |~200.00 KiB (4.25%)|200.00 KiB |204,800 |4.25 |~4.40 MiB | 2019-01-25 23:59:02  |100
tmp_small_images |3.57 MiB |~184.00 KiB (5.04%)|184.00 KiB |188,416 |5.04 |~3.39 MiB | 2019-01-25 23:59:35  |100
_tmp_fill_b_selector |1.81 MiB |~144.00 KiB (7.79%)|144.00 KiB |147,456 |7.79 |~1.67 MiB | 2019-01-25 23:18:34  |100
tmp.male_boards |36.25 MiB |~144.00 KiB (0.39%)|144.00 KiB |147,456 |0.39 |~36.11 MiB | 2019-01-25 23:58:59  |100
jobs_translate.cache_backup |52.36 MiB |~144.00 KiB (0.27%)|144.00 KiB |147,456 |0.27 |~52.22 MiB | 2019-01-26 01:02:07  |100
tmp.erotic_posts_20170907 |2.34 MiB |~120.00 KiB (5.02%)|120.00 KiB |122,880 |5.02 |~2.22 MiB | 2019-01-26 01:02:02  |100
tmp.erotic_posts_20170910 |1.68 MiB |~112.00 KiB (6.51%)|112.00 KiB |114,688 |6.51 |~1.58 MiB | 2019-01-25 23:59:57  |100
archive.spam_20140721_posts |2.24 MiB |~104.00 KiB (4.55%)|104.00 KiB |106,496 |4.55 |~2.14 MiB | 2019-01-25 23:18:34  |100
tmp.erotic_posts_20170905 |1.63 MiB |~104.00 KiB (6.25%)|104.00 KiB |106,496 |6.25 |~1.53 MiB | 2019-01-26 01:03:07  |100
archive.issue_3141_posts_wo_domain |1.79 MiB |~88.00 KiB (4.80%)|88.00 KiB |90,112 |4.80 |~1.71 MiB | 2019-01-25 23:30:58  |100
jobs_translate.mv_titles_new2lock |1.13 MiB |~80.00 KiB (6.94%)|80.00 KiB |81,920 |6.94 |~1.05 MiB | 2019-01-25 23:59:17  |100
delme2 |1.09 MiB |~64.00 KiB (5.76%)|64.00 KiB |65,536 |5.76 |~1.03 MiB | 2019-01-25 23:59:14  |100
_gt_calc_description |12.64 MiB |~64.00 KiB (0.49%)|64.00 KiB |65,536 |0.49 |~12.58 MiB | 2019-01-25 23:59:52  |100
archive.pnct_PictureComment |4.72 MiB |~48.00 KiB (1.00%)|48.00 KiB |49,152 |1.00 |~4.67 MiB | 2019-01-25 23:18:24  |100
tmp.hosts |104.00 KiB |~48.00 KiB (46.15%)|48.00 KiB |49,152 |46.15 |~56.00 KiB | 2019-01-26 01:15:04  |100
pnct_CmsArticle |72.00 KiB |~40.00 KiB (55.56%)|40.00 KiB |40,960 |55.56 |~32.00 KiB | 2019-01-26 02:04:32  |100
pnct_CmsPage |456.00 KiB |~40.00 KiB (8.77%)|40.00 KiB |40,960 |8.77 |~416.00 KiB | 2019-01-26 02:04:32  |100
support539899.post |968.00 KiB |~40.00 KiB (4.13%)|40.00 KiB |40,960 |4.13 |~928.00 KiB | 2019-01-25 23:59:57  |100
antispam.money_fraud20180202_followuser |496.00 KiB |~40.00 KiB (8.06%)|40.00 KiB |40,960 |8.06 |~456.00 KiB | 2019-01-26 01:02:11  |100
_save_google_translate |4.83 MiB |~32.00 KiB (0.65%)|32.00 KiB |32,768 |0.65 |~4.80 MiB | 2019-01-25 23:59:53  |100
pnct_PictureReport |104.00 KiB |~32.00 KiB (30.77%)|32.00 KiB |32,768 |30.77 |~72.00 KiB | 2019-01-25 23:18:34  |100
archive.pnct_AnyBoard |416.00 KiB |~32.00 KiB (7.69%)|32.00 KiB |32,768 |7.69 |~384.00 KiB | 2019-01-25 23:59:12  |100
tmp.host_blacklisted_20170920 |200.00 KiB |~24.00 KiB (12.00%)|24.00 KiB |24,576 |12.00 |~176.00 KiB | 2019-01-25 23:59:54  |100
jobs_translate.last_id_board |32.00 KiB |~24.00 KiB (75.00%)|24.00 KiB |24,576 |75.00 |~8.00 KiB | 2019-01-26 01:02:03  |100
tmp.host_blacklisted_snapshot_20170827 |176.00 KiB |~24.00 KiB (13.64%)|24.00 KiB |24,576 |13.64 |~152.00 KiB | 2019-01-25 23:59:17  |100
editors_audit |1.27 MiB |~16.00 KiB (1.23%)|16.00 KiB |16,384 |1.23 |~1.25 MiB | 2019-01-26 00:26:46  |100
pnct_Video |19.99 MiB |~16.00 KiB (0.08%)|16.00 KiB |16,384 |0.08 |~19.97 MiB | 2019-01-25 23:25:02  |100
archive.spam_20140722_posts |240.00 KiB |~16.00 KiB (6.67%)|16.00 KiB |16,384 |6.67 |~224.00 KiB | 2019-01-25 23:18:34  |100
jobs_translate.current_last_id |24.00 KiB |~16.00 KiB (66.67%)|16.00 KiB |16,384 |66.67 |~8.00 KiB | 2019-01-25 23:18:34  |100
_pg_stat_statements_20171206_012800 |256.00 KiB |~16.00 KiB (6.25%)|16.00 KiB |16,384 |6.25 |~240.00 KiB | 2019-01-26 01:15:04  |100
news_all |152.00 KiB |~8.00 KiB (5.26%)|8.00 KiB |8,192 |5.26 |~144.00 KiB | 2019-01-26 00:26:30  |100
archive.spam_20140727_posts_1 |64.00 KiB |~8.00 KiB (12.50%)|8.00 KiB |8,192 |12.50 |~56.00 KiB | 2019-01-25 23:18:34  |100
antispam.money_fraud201801_followuser |7.82 MiB |~8.00 KiB (0.10%)|8.00 KiB |8,192 |0.10 |~7.81 MiB | 2019-01-26 01:16:39  |100
ml.result_full |80.00 KiB |~8.00 KiB (10.00%)|8.00 KiB |8,192 |10.00 |~72.00 KiB | 2019-01-25 23:25:05  |100
archive.spam_20140722_posts_2 |104.00 KiB |~8.00 KiB (7.69%)|8.00 KiB |8,192 |7.69 |~96.00 KiB | 2019-01-25 23:18:34  |100
sqitch.events |128.00 KiB |~8.00 KiB (6.25%)|8.00 KiB |8,192 |6.25 |~120.00 KiB | 2019-01-26 02:04:32  |100
pnct_NetworkRequest |472.00 KiB |~8.00 KiB (1.69%)|8.00 KiB |8,192 |1.69 |~464.00 KiB | 2019-01-25 23:18:35  |100
archive.spam_snapshot_20141025 |200.00 KiB |~8.00 KiB (4.00%)|8.00 KiB |8,192 |4.00 |~192.00 KiB | 2019-01-25 23:18:34  |100
ml.result_fplus_glm |56.00 KiB |~8.00 KiB (14.29%)|8.00 KiB |8,192 |14.29 |~48.00 KiB | 2019-01-25 23:59:15  |100
tmp.erotic_posts_20170911 |248.00 KiB |~8.00 KiB (3.23%)|8.00 KiB |8,192 |3.23 |~240.00 KiB | 2019-01-25 23:25:03  |100
ml.kusochek_tsvector_top_words |8.00 KiB |~0.00 bytes (0.00%)|0.00 bytes |0 |0.00 |~8.00 KiB | 2019-01-25 23:59:57  |100
ml.kusochek_learndata_fplus_linregr2_summary |8.00 KiB |~0.00 bytes (0.00%)|0.00 bytes |0 |0.00 |~8.00 KiB | 2019-01-25 23:18:35  |100
ml.kusochek_learndata_all_linregr_summary |8.00 KiB |~0.00 bytes (0.00%)|0.00 bytes |0 |0.00 |~8.00 KiB | 2019-01-25 23:18:34  |100
hm_promo |2.03 MiB |~0.00 bytes (0.00%)|0.00 bytes |0 |0.00 |~2.03 MiB | 2019-01-25 23:59:55  |100
ml.kusochek_learndata_all_linregr |32.00 KiB |~0.00 bytes (0.00%)|0.00 bytes |0 |0.00 |~32.00 KiB | 2019-01-25 23:18:34  |100
antispam.money_fraud201801 |16.00 KiB |~0.00 bytes (0.00%)|0.00 bytes |0 |0.00 |~16.00 KiB | 2019-01-26 01:16:39  |100
 

## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_F005"></a>
[Table of contents](#postgres-checkup_top)
# F005 Autovacuum: Index bloat #
:warning: This report is based on estimations. The errors in bloat estimates may be significant (in some cases, up to 15% and even more). Use it only as an indicator of potential issues.

## Observations ##


### Master (`db2`) ###
 Index (Table) | &#9660;&nbsp;Size | Extra | Estimated bloat | Est. bloat, bytes | Est. bloat ratio,% | Live | Fill factor
---------------|-------------------|-------|-------|-------------|-------------|------|-------------
**Total** |**197.13 GiB** ||**35.25 GiB** |**37,839,020,032**|||
pk_post_view (post_view) |42.88 GiB |~16.08 GiB (0.00%) |13.11 GiB |14,071,242,752 |30.57 |~29.78 GiB |90
u_bot_view_row_id (bot_visit) |19.85 GiB |~6.44 GiB (0.00%) |4.94 GiB |5,297,381,376 |24.85 |~14.92 GiB |90
u_network_follow_comby (pnct_network_follow) |15.55 GiB |~4.51 GiB (0.00%) |3.25 GiB |3,486,138,368 |20.89 |~12.30 GiB |90
bot_visit_pkey (bot_visit) |14.97 GiB |~1.56 GiB (0.00%) |54.41 MiB |57,049,088 |0.35 |~14.92 GiB |90
i_network_follow_provider_id (pnct_network_follow) |12.00 GiB |~2.97 GiB (0.00%) |1.94 GiB |2,082,398,208 |16.17 |~10.06 GiB |90
i_pnct_network_follow_lower_email (pnct_network_follow) |8.99 GiB |~1.93 GiB (0.00%) |1.14 GiB |1,216,176,128 |12.61 |~7.85 GiB |90
u_post_subtitles_uniq (post_subtitles) |8.47 GiB |~1.12 GiB (0.00%) |308.40 MiB |323,371,008 |3.56 |~8.17 GiB |90
pnct_network_follow_pkey (pnct_network_follow) |6.23 GiB |~1.22 GiB (0.00%) |667.11 MiB |699,514,880 |10.46 |~5.58 GiB |90
post_subtitles_pkey (post_subtitles) |5.87 GiB |~623.88 MiB (0.00%) |21.16 MiB |22,183,936 |0.35 |~5.85 GiB |90
i_click_post_id (click) |5.75 GiB |~878.87 MiB (0.00%) |318.39 MiB |333,848,576 |5.41 |~5.44 GiB |90
click_pkey (click) |5.46 GiB |~580.15 MiB (0.00%) |19.67 MiB |20,619,264 |0.35 |~5.44 GiB |90
i_post_moderation (pnct_PicturePost) |4.77 GiB |~1.04 GiB (0.00%) |637.37 MiB |668,327,936 |13.07 |~4.14 GiB |90
i_networkevent_userid_created (pnct_NetworkEvent) |4.41 GiB |~1.62 GiB (0.00%) |1.31 GiB |1,404,690,432 |29.71 |~3.10 GiB |90
i_network_follow_follow_used (pnct_network_follow) |3.71 GiB |~1.76 GiB (0.00%) |1.54 GiB |1,644,748,800 |41.37 |~2.18 GiB |90
pnct_NetworkEvent_pkey (pnct_NetworkEvent) |3.11 GiB |~330.43 MiB (0.00%) |11.21 MiB |11,747,328 |0.35 |~3.10 GiB |90
i_user_visits_postgrest_auth (user_visits) |2.40 GiB |~76.72 MiB (0.00%) | | | |~2.59 GiB |90
user_visits_pkey (user_visits) |2.36 GiB |~467.83 MiB (0.00%) |247.91 MiB |259,948,544 |10.29 |~2.12 GiB |90
i_picture_post_md5__notnull (pnct_PicturePost) |2.11 GiB |~1.10 GiB (0.00%) |0.99 GiB |1,055,604,736 |46.72 |~1.13 GiB |90
search_log_pkey (search_log) |2.09 GiB |~222.36 MiB (0.00%) |7.54 MiB |7,905,280 |0.35 |~2.09 GiB |90
i_picturepost_parentid_id2 (pnct_PicturePost) |1.86 GiB |~786.82 MiB (0.00%) |662.01 MiB |694,165,504 |34.83 |~1.21 GiB |90
pnct_picturepost_sourcedomain (pnct_PicturePost) |1.85 GiB |~308.49 MiB (0.00%) |130.90 MiB |137,256,960 |6.93 |~1.72 GiB |90
i_picture_post_by_board_id_id_2 (pnct_PicturePost) |1.82 GiB |~747.07 MiB (0.00%) |622.26 MiB |652,484,608 |33.43 |~1.21 GiB |90
i_picturepost_created_likes (pnct_PicturePost) |1.82 GiB |~297.29 MiB (0.00%) |124.82 MiB |130,875,392 |6.73 |~1.70 GiB |90
i_picture_post_by_user_id_id_2 (pnct_PicturePost) |1.81 GiB |~730.71 MiB (0.00%) |605.90 MiB |635,330,560 |32.84 |~1.21 GiB |90
i_post_subtitles_postid_created_ru_index (post_subtitles) |1.68 GiB |~254.05 MiB (0.00%) |92.04 MiB |96,501,760 |5.36 |~1.59 GiB |90
i_tmp_53266_ssd (post_subtitles) |1.68 GiB |~253.91 MiB (0.00%) |91.89 MiB |96,346,112 |5.35 |~1.59 GiB |90
u_cache_q_source_target2 (translation_proxy.cache) |1.45 GiB |~327.04 MiB (0.00%) |203.88 MiB |213,778,432 |13.75 |~1.25 GiB |90
pnct_PicturePost_pkey (pnct_PicturePost) |1.25 GiB |~157.54 MiB (0.00%) |32.73 MiB |34,316,288 |2.57 |~1.21 GiB |90
pnct_StorageFile_pkey (pnct_StorageFile) |948.65 MiB |~98.58 MiB (0.00%) |3.36 MiB |3,514,368 |0.35 |~945.30 MiB |90
action_pkey (action) |845.95 MiB |~87.91 MiB (0.00%) |3.00 MiB |3,137,536 |0.35 |~842.96 MiB |90
pnct_NetworkFollowUser_pkey2 (pnct_NetworkFollowUser) |555.62 MiB |~62.50 MiB (0.00%) |7.25 MiB |7,602,176 |1.30 |~548.37 MiB |90
cache_pkey2 (translation_proxy.cache) |461.67 MiB |~78.29 MiB (0.00%) |35.34 MiB |37,052,416 |7.65 |~426.33 MiB |90
pnct_PictureLike_pkey (pnct_PictureLike) |399.58 MiB |~42.75 MiB (0.00%) |2.79 MiB |2,916,352 |0.70 |~396.79 MiB |90
pnct_NetworkFollowBoard_pkey2 (pnct_NetworkFollowBoard) |356.35 MiB |~38.20 MiB (0.00%) |2.56 MiB |2,678,784 |0.72 |~353.79 MiB |90
pnct_PictureFragment_pkey (pnct_PictureFragment) |353.97 MiB |~38.08 MiB (0.00%) |3.11 MiB |3,260,416 |0.88 |~350.86 MiB |90
u_cache_q_source_target (google_translate.cache_orig) |338.92 MiB | | | | |~385.37 MiB |90
i_log_row_id_table_name (archive.log) |329.86 MiB |~106.00 MiB (0.00%) |80.65 MiB |84,557,824 |24.45 |~249.22 MiB |90
i_pnct_user_lower_firstname_is_male (pnct_User) |312.28 MiB |~79.75 MiB (0.00%) |53.54 MiB |56,131,584 |17.14 |~258.75 MiB |90
pnct_user_email (pnct_User) |299.24 MiB |~62.54 MiB (0.00%) |36.63 MiB |38,404,096 |12.24 |~262.61 MiB |90
pnct_StorageBin_pkey (pnct_StorageBin) |284.90 MiB |~29.61 MiB (0.00%) |1.01 MiB |1,056,768 |0.35 |~283.90 MiB |90
i_network_follow_provider_id_by_provider (pnct_network_follow) |272.01 MiB |~148.96 MiB (0.00%) |135.01 MiB |141,565,952 |49.63 |~137.00 MiB |90
u_user_mailru_id (pnct_User) |263.22 MiB |~80.57 MiB (0.00%) |60.20 MiB |63,119,360 |22.87 |~203.02 MiB |90
u_user_username_lower (pnct_User) |261.22 MiB |~79.83 MiB (0.00%) |59.75 MiB |62,644,224 |22.87 |~201.47 MiB |90
u_user_facebook_id (pnct_User) |250.17 MiB |~120.79 MiB (0.00%) |106.34 MiB |111,501,312 |42.51 |~143.83 MiB |90
pnct_user_role (pnct_User) |248.83 MiB |~119.13 MiB (0.00%) |104.60 MiB |109,674,496 |42.04 |~144.23 MiB |90
u_user_odnoklassniki_id (pnct_User) |248.11 MiB |~169.32 MiB (0.00%) |160.56 MiB |168,353,792 |64.71 |~87.55 MiB |90
i_user_google_id (pnct_User) |247.81 MiB |~168.90 MiB (0.00%) |160.12 MiB |167,895,040 |64.61 |~87.69 MiB |90
u_user_vk_id (pnct_User) |246.71 MiB |~166.24 MiB (0.00%) |157.39 MiB |165,027,840 |63.79 |~89.33 MiB |90
i_board_target_age_from_to_gender_recommended (pnct_Board) |207.65 MiB |~184.55 MiB (0.00%) |181.93 MiB |190,758,912 |87.61 |~25.73 MiB |90
pnct_Picture_pkey (pnct_Picture) |203.16 MiB |~21.12 MiB (0.00%) |744.00 KiB |761,856 |0.36 |~202.43 MiB |90
u_job_move_old_binimages_id (job_move_old_binimages) |202.44 MiB |~142.36 MiB (0.00%) |135.64 MiB |142,221,312 |67.00 |~66.81 MiB |90
job_move_old_binimages_status (job_move_old_binimages) |201.33 MiB |~171.39 MiB (0.00%) |168.04 MiB |176,193,536 |83.46 |~33.30 MiB |90
u_job_move_old_images_id (job_move_old_images) |184.97 MiB |~19.23 MiB (0.00%) |680.00 KiB |696,320 |0.36 |~184.30 MiB |90
i_archived_post_id (archive.pnct_PicturePost) |176.43 MiB |~66.02 MiB (0.00%) |53.65 MiB |56,254,464 |30.41 |~122.78 MiB |90
conveyor_failed_pkey (conveyor_failed) |166.58 MiB |~62.51 MiB (0.00%) |50.61 MiB |53,059,584 |30.38 |~115.97 MiB |90
pnct_User_pkey (pnct_User) |162.25 MiB |~32.56 MiB (0.00%) |18.03 MiB |18,898,944 |11.11 |~144.23 MiB |90
i_board_categoryid_editors_choice (pnct_Board) |152.99 MiB |~138.90 MiB (0.00%) |137.36 MiB |144,023,552 |89.78 |~15.64 MiB |90
log_pkey (archive.log) |138.79 MiB |~14.48 MiB (0.00%) |568.00 KiB |581,632 |0.40 |~138.23 MiB |90
cache_pkey (google_translate.cache_orig) |128.93 MiB |~13.40 MiB (0.00%) |472.00 KiB |483,328 |0.36 |~128.47 MiB |90
pnct_user_id_role__partial (pnct_User) |128.13 MiB |~40.32 MiB (0.00%) |30.48 MiB |31,956,992 |23.79 |~97.65 MiB |90
job_move_old_images_status (job_move_old_images) |124.79 MiB |~12.98 MiB (0.00%) |464.00 KiB |475,136 |0.36 |~124.34 MiB |90
i_board_lower_title (pnct_Board) |124.15 MiB |~72.72 MiB (0.00%) |66.83 MiB |70,074,368 |53.83 |~57.33 MiB |90
pnct_AnyPost_image_original_md5_idx (pnct_AnyPost) |115.97 MiB |~42.75 MiB (0.00%) |34.39 MiB |36,052,992 |29.65 |~81.59 MiB |90
u_board_unique_title_per_user (pnct_Board) |115.84 MiB |~64.40 MiB (0.00%) |58.52 MiB |61,358,080 |50.52 |~57.33 MiB |90
u_board_unique_url_per_user (pnct_Board) |99.73 MiB |~57.62 MiB (0.00%) |52.86 MiB |55,418,880 |53.00 |~46.88 MiB |90
pnct_AnyPost_lower_idx (pnct_AnyPost) |84.76 MiB |~24.00 MiB (0.00%) |17.11 MiB |17,940,480 |20.19 |~67.65 MiB |90
pnct_AnyPost_userId_id_idx (pnct_AnyPost) |83.75 MiB |~22.15 MiB (0.00%) |15.33 MiB |16,064,512 |18.29 |~68.43 MiB |90
pnct_AnyPost_boardId_id_idx (pnct_AnyPost) |83.30 MiB |~21.70 MiB (0.00%) |14.88 MiB |15,597,568 |17.86 |~68.43 MiB |90
pnct_AnyPost_parentId_id_idx (pnct_AnyPost) |82.58 MiB |~20.98 MiB (0.00%) |14.16 MiB |14,843,904 |17.14 |~68.43 MiB |90
pnct_AnyPost_created_likes_idx (pnct_AnyPost) |81.96 MiB |~2.64 MiB (0.00%) | | | |~88.31 MiB |90
pnct_Hash_pkey (pnct_Hash) |80.65 MiB |~8.81 MiB (0.00%) |1.17 MiB |1,220,608 |1.44 |~79.48 MiB |90
u_token_user_provider (token) |76.49 MiB |~11.50 MiB (0.00%) |4.30 MiB |4,505,600 |5.62 |~72.19 MiB |90
i_postfix_list4mar2018 (postfix.list_4_mar_2018) |60.65 MiB |~5.12 MiB (0.00%) | | | |~61.82 MiB |90
i_board_by_posts_and_last_post_added3 (pnct_Board) |60.39 MiB |~37.34 MiB (0.00%) |34.80 MiB |36,487,168 |57.63 |~25.59 MiB |90
pnct_AnyPost_pkey (pnct_AnyPost) |54.43 MiB | | | | |~68.43 MiB |90
i_user_avatar__notnull (pnct_User) |54.05 MiB |~12.59 MiB (0.00%) |7.97 MiB |8,347,648 |14.73 |~46.09 MiB |90
token_pkey (token) |51.88 MiB |~5.40 MiB (0.00%) |200.00 KiB |204,800 |0.38 |~51.68 MiB |90
pnct_Board_pkey (pnct_Board) |47.92 MiB |~24.54 MiB (0.00%) |21.91 MiB |22,970,368 |45.72 |~26.01 MiB |90
pnct_UserNotify_pkey (pnct_UserNotify) |33.17 MiB |~3.52 MiB (0.00%) |200.00 KiB |204,800 |0.59 |~32.97 MiB |90
pnct_NetworkInvite_pkey (pnct_NetworkInvite) |29.94 MiB |~3.13 MiB (0.00%) |120.00 KiB |122,880 |0.39 |~29.83 MiB |90
u_picturepost_boardid_misc_autosourcer (pnct_PicturePost) |28.33 MiB |~12.11 MiB (0.00%) |10.40 MiB |10,895,360 |36.68 |~17.94 MiB |90
i_archived_anypost_id (archive.pnct_AnyPost) |23.86 MiB |~6.24 MiB (0.00%) |4.26 MiB |4,464,640 |17.85 |~19.60 MiB |90
um_email (pnct_unsubscribe_mail) |22.00 MiB |~9.90 MiB (0.00%) |8.54 MiB |8,945,664 |38.79 |~13.47 MiB |90
u_host_name (host) |10.47 MiB |~2.79 MiB (0.00%) |1.91 MiB |1,998,848 |18.21 |~8.57 MiB |90
u_mv_messages_new2lock_id (jobs_translate.mv_messages_new2lock) |10.03 MiB |~1.05 MiB (0.00%) |48.00 KiB |49,152 |0.47 |~9.98 MiB |90
i_tmp_13 (translation_proxy.cache) |9.80 MiB |~9.79 MiB (0.00%) |9.79 MiB |10,264,576 |99.92 |~8.00 KiB |90
i_board_directory_steps (board_directory) |9.55 MiB |~1.04 MiB (0.00%) |88.00 KiB |90,112 |0.90 |~9.47 MiB |90
i_user_directory_steps (user_directory) |8.68 MiB |~968.00 KiB (0.00%) |88.00 KiB |90,112 |0.99 |~8.60 MiB |90
pnct_unsubscribe_mail_pkey (pnct_unsubscribe_mail) |7.54 MiB |~840.00 KiB (0.00%) |72.00 KiB |73,728 |0.93 |~7.47 MiB |90
pnct_Video_pkey (pnct_Video) |7.48 MiB |~808.00 KiB (0.00%) |40.00 KiB |40,960 |0.52 |~7.44 MiB |90
pnct_PictureComment_pkey (pnct_PictureComment) |6.29 MiB |~712.00 KiB (0.00%) |72.00 KiB |73,728 |1.12 |~6.22 MiB |90
i_tmp_killme3 (pnct_PicturePost) |6.02 MiB |~4.46 MiB (0.00%) |4.29 MiB |4,489,216 |71.17 |~1.74 MiB |90
i_host_category (host) |4.98 MiB |~2.40 MiB (0.00%) |2.11 MiB |2,211,840 |42.39 |~2.87 MiB |90
host_pkey (host) |4.78 MiB |~512.00 KiB (0.00%) |24.00 KiB |24,576 |0.49 |~4.75 MiB |90
i_board_cover__notnull (pnct_Board) |4.00 MiB |~2.57 MiB (0.00%) |2.41 MiB |2,523,136 |60.27 |~1.59 MiB |90
conveyor_pkey (conveyor) |3.81 MiB |~3.55 MiB (0.00%) |3.52 MiB |3,686,400 |92.40 |~296.00 KiB |90
pnct_AnyBoard_target_age_from_target_age_to_target… (pnct_AnyBoard) |3.69 MiB |~2.98 MiB (0.00%) |2.90 MiB |3,039,232 |78.60 |~808.00 KiB |90
i_male_boards_latest_post_post_id (tmp.male_boards_latest_post) |3.36 MiB |~944.00 KiB (0.00%) |664.00 KiB |679,936 |19.35 |~2.71 MiB |90
pnct_AnyBoard_userId_lower_idx1 (pnct_AnyBoard) |3.08 MiB |~1.94 MiB (0.00%) |1.82 MiB |1,900,544 |59.03 |~1.26 MiB |90
pnct_AnyBoard_userId_lower_idx (pnct_AnyBoard) |2.98 MiB |~2.05 MiB (0.00%) |1.95 MiB |2,039,808 |65.35 |~1.04 MiB |90


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_F008"></a>
[Table of contents](#postgres-checkup_top)
# F008 Autovacuum: Resource usage #

## Observations ##

### Settings ###



Setting name | Value | Unit | Pretty value
-------------|-------|------|--------------
[autovacuum_max_workers](https://postgresqlco.nf/en/doc/param/autovacuum_max_workers)|10|<no value> | 
[autovacuum_work_mem](https://postgresqlco.nf/en/doc/param/autovacuum_work_mem)|-1|kB | 
[log_autovacuum_min_duration](https://postgresqlco.nf/en/doc/param/log_autovacuum_min_duration)|0|ms | 
[maintenance_work_mem](https://postgresqlco.nf/en/doc/param/maintenance_work_mem)|2097152|kB | 2.00 GiB
[max_connections](https://postgresqlco.nf/en/doc/param/max_connections)|1000|<no value> | 
[shared_buffers](https://postgresqlco.nf/en/doc/param/shared_buffers)|2097152|8kB | 16.00 GiB
[work_mem](https://postgresqlco.nf/en/doc/param/work_mem)|65536|kB | 64.00 MiB


### CPU ###

Cpu count you can see in report A001  

### RAM ###

Ram amount you can see in report A001

Max workers memory: 20 GiB


### DISK ###

:warning: Warning: collection of current impact on disks is not yet implemented. Please refer to Postgres logs and see current read and write IO bandwidth caused by autovacuum.

## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_G001"></a>
[Table of contents](#postgres-checkup_top)
# G001 Memory-related settings #

## Observations ##


### Master (`db2`) ###

Setting name | Value | Unit | Pretty value
-------------|-------|------|--------------
[autovacuum_work_mem](https://postgresqlco.nf/en/doc/param/autovacuum_work_mem) | -1| kB | 
[effective_cache_size](https://postgresqlco.nf/en/doc/param/effective_cache_size) | 6291456| 8kB | 48.00 GiB
[maintenance_work_mem](https://postgresqlco.nf/en/doc/param/maintenance_work_mem) | 2097152| kB | 2.00 GiB
[max_connections](https://postgresqlco.nf/en/doc/param/max_connections) | 1000| <no value> | 
[shared_buffers](https://postgresqlco.nf/en/doc/param/shared_buffers) | 2097152| 8kB | 16.00 GiB
[temp_buffers](https://postgresqlco.nf/en/doc/param/temp_buffers) | 8192| 8kB | 64.00 MiB
[work_mem](https://postgresqlco.nf/en/doc/param/work_mem) | 65536| kB | 64.00 MiB




### Replica servers: ###
    
#### Replica (`db3`) ####
        
Setting name | Value
-------------|-------
[autovacuum_work_mem](https://postgresqlco.nf/en/doc/param/autovacuum_work_mem)|
[effective_cache_size](https://postgresqlco.nf/en/doc/param/effective_cache_size)|48.00 GiB
[maintenance_work_mem](https://postgresqlco.nf/en/doc/param/maintenance_work_mem)|2.00 GiB
[max_connections](https://postgresqlco.nf/en/doc/param/max_connections)|
[shared_buffers](https://postgresqlco.nf/en/doc/param/shared_buffers)|16.00 GiB
[temp_buffers](https://postgresqlco.nf/en/doc/param/temp_buffers)|64.00 MiB
[work_mem](https://postgresqlco.nf/en/doc/param/work_mem)|128.00 MiB


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_G002"></a>
[Table of contents](#postgres-checkup_top)
# G002 Connections and current activity #

## Observations ##


### Master (`db2`) ###
\# | User | DB | Current state | Count | State changed >1m ago | State changed >1h ago
----|------|----|---------------|-------|-----------------------|-----------------------
1 | ALL users | ALL databases | ALL states | 89 | 12 | 5
2 | postila_web | ALL databases | active | 2 | 0 | 0
3 | vyagofarov | ALL databases | active | 1 | 0 | 0
4 | nevada | ALL databases | idle | 40 | 2 | 0
5 | drupal | ALL databases | idle | 12 | 2 | 0
6 | postila_web | ALL databases | idle | 10 | 2 | 0
7 | drupal_cron | ALL databases | idle | 7 | 0 | 0
8 | kid | ALL databases | idle | 5 | 0 | 0
9 | nevada_adm | ALL databases | idle | 4 | 4 | 4
10 | pcons_user | ALL databases | idle | 3 | 1 | 1
11 | rest_api_user | ALL databases | idle | 3 | 0 | 0
12 | kid2 | ALL databases | idle | 1 | 1 | 0
13 | poauth_user | ALL databases | idle | 1 | 0 | 0
14 | drupal | _4vkusa_ru | idle | 3 | 0 | 0
15 | drupal_cron | _4vkusa_ru | idle | 1 | 0 | 0
16 | nevada | _4vkusa_ru | idle | 1 | 0 | 0
17 | drupal_cron | demotivator_prikolisti_com | idle | 1 | 0 | 0
18 | drupal | demotivator_prikolisti_com | idle | 1 | 0 | 0
19 | nevada | demotivator_prikolisti_com | idle | 1 | 0 | 0
20 | drupal | fffail_ru | idle | 3 | 0 | 0
21 | drupal_cron | fffail_ru | idle | 1 | 0 | 0
22 | nevada | fffail_ru | idle | 1 | 0 | 0
23 | drupal_cron | foto_prikolisti_com | idle | 1 | 0 | 0
24 | nevada | foto_prikolisti_com | idle | 1 | 0 | 0
25 | drupal | foto_prikolisti_com | idle | 1 | 1 | 0
26 | drupal | kotomail_ru | idle | 2 | 0 | 0
27 | nevada | kotomail_ru | idle | 1 | 0 | 0
28 | drupal_cron | kotomail_ru | idle | 1 | 0 | 0
29 | kid | nevada | idle | 1 | 0 | 0
30 | nevada | nevada | idle | 1 | 0 | 0
31 | postila_web | postfix | idle | 3 | 2 | 0
32 | nevada | postfix | idle | 2 | 2 | 0
33 | postila_web | postila_ru | active | 2 | 0 | 0
34 | vyagofarov | postila_ru | active | 1 | 0 | 0
35 | nevada | postila_ru | idle | 28 | 0 | 0
36 | postila_web | postila_ru | idle | 7 | 0 | 0
37 | pcons_user | postila_ru | idle | 3 | 1 | 1
38 | rest_api_user | postila_ru | idle | 3 | 0 | 0
39 | nevada_adm | postila_ru | idle | 2 | 2 | 2
40 | poauth_user | postila_ru | idle | 1 | 0 | 0
41 | kid2 | postila_ru | idle | 1 | 1 | 0
42 | drupal_cron | prikolisti_com | idle | 2 | 0 | 0
43 | drupal | prikolisti_com | idle | 2 | 1 | 0
44 | nevada_adm | test | idle | 2 | 2 | 2
45 | nevada | vegas | idle | 4 | 0 | 0
46 | kid | vegas | idle | 4 | 0 | 0


### Replica servers: ###

#### Replica (`db3`) ####

\# | User | DB | Current state | Count | State changed >1m ago | State changed >1h ago
----|------|----|---------------|-------|-----------------------|-----------------------
1 | ALL users | ALL databases | ALL states | 19 | 3 | 3
2 | rouser | ALL databases | active | 4 | 0 | 0
3 | vyagofarov | ALL databases | active | 1 | 0 | 0
4 | rouser | ALL databases | idle | 11 | 0 | 0
5 | nevada_adm | ALL databases | idle | 2 | 2 | 2
6 | nevada | ALL databases | idle | 1 | 1 | 1
7 | rouser | postila_ru | active | 4 | 0 | 0
8 | vyagofarov | postila_ru | active | 1 | 0 | 0
9 | rouser | postila_ru | idle | 11 | 0 | 0
10 | nevada_adm | postila_ru | idle | 2 | 2 | 2
11 | nevada | postila_ru | idle | 1 | 1 | 1


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_G003"></a>
[Table of contents](#postgres-checkup_top)
# G003 Timeouts, locks, deadlocks #

## Observations ##


### Master (`db2`) ###
#### Timeouts ####
Setting name | Value | Unit | Pretty value
-------------|-------|------|--------------
[authentication_timeout](https://postgresqlco.nf/en/doc/param/authentication_timeout)|60|s|
[idle_in_transaction_session_timeout](https://postgresqlco.nf/en/doc/param/idle_in_transaction_session_timeout)|600000|ms|
[statement_timeout](https://postgresqlco.nf/en/doc/param/statement_timeout)|120000|ms|

#### Locks ####
Setting name | Value | Unit | Pretty value
-------------|-------|------|--------------
[deadlock_timeout](https://postgresqlco.nf/en/doc/param/deadlock_timeout)|1000|ms|
[lock_timeout](https://postgresqlco.nf/en/doc/param/lock_timeout)|0|ms|
[max_locks_per_transaction](https://postgresqlco.nf/en/doc/param/max_locks_per_transaction)|64|<no value>|
[max_pred_locks_per_transaction](https://postgresqlco.nf/en/doc/param/max_pred_locks_per_transaction)|64|<no value>|


#### Database specified settings ####
Database | Setting
---------|---------
test2 | [lock_timeout=2s]

#### User specified settings ####
User | Setting
---------|---------
tester2 | [lock_timeout=3s]

#### Databases data ####
Database | Conflicts | &#9660;&nbsp;Deadlocks | Stats reset at | Stat reset
-------------|-------|-----------|----------------|------------
_4vkusa_ru|0|0|2018-12-05T17:44:47.630562+00:00|68 days 20:54:01
dba|0|0|2018-12-05T17:45:43.693411+00:00|68 days 20:53:05
demotivator_prikolisti_com|0|0|2018-12-05T17:44:46.515991+00:00|68 days 20:54:02
fffail_ru|0|0|2018-12-05T17:44:49.350127+00:00|68 days 20:53:59
foto_prikolisti_com|0|0|2018-12-05T17:44:47.216088+00:00|68 days 20:54:01
fotominuta_ru|0|0|2018-12-05T17:45:44.055744+00:00|68 days 20:53:05
kotomail_ru|0|0|2018-12-05T17:44:58.873581+00:00|68 days 20:53:50
nevada|0|0|2018-12-05T17:44:47.458967+00:00|68 days 20:54:01
nevada1|0|0|2018-12-05T17:45:45.174594+00:00|68 days 20:53:03
nevada2|0|0|2018-12-05T17:45:43.666073+00:00|68 days 20:53:05
nevada3|0|0|2018-12-05T17:45:45.12644+00:00|68 days 20:53:03
nevada4|0|0|2018-12-05T17:45:45.443986+00:00|68 days 20:53:03
nevada5|0|0|2018-12-05T17:45:45.854795+00:00|68 days 20:53:03
nevada6|0|0|2018-12-05T17:45:45.86433+00:00|68 days 20:53:03
nevada7|0|0|2018-12-05T17:45:45.183802+00:00|68 days 20:53:03
nevada8|0|0|2018-12-05T17:45:45.193902+00:00|68 days 20:53:03
nombox_co|0|0|2018-12-05T17:45:44.640312+00:00|68 days 20:53:04
postfix|0|0|2018-12-05T17:44:47.590755+00:00|68 days 20:54:01
postgres|0|0|2018-12-05T17:44:46.895218+00:00|68 days 20:54:02
postgresmen_ru|0|0|2018-12-05T17:45:45.256167+00:00|68 days 20:53:03
postila_ru|0|0|2018-12-05T17:44:46.538221+00:00|68 days 20:54:02
prikolisti_com|0|0|2018-12-05T17:44:55.21009+00:00|68 days 20:53:53
redash|0|0|2018-12-05T17:45:45.498811+00:00|68 days 20:53:03
startupturbo_com|0|0|2018-12-05T17:45:45.822767+00:00|68 days 20:53:03
startupturbo_com_old|0|0|2018-12-05T17:45:43.787586+00:00|68 days 20:53:05
strakhovm_ru|0|0|2018-12-05T17:45:44.943143+00:00|68 days 20:53:04
svoi_simplead_ru|0|0|2018-12-05T17:45:44.759181+00:00|68 days 20:53:04
test|0|0|2018-12-05T17:45:43.684085+00:00|68 days 20:53:05
test2|0|0|2018-12-19T20:33:31.466471+00:00|54 days 18:05:17
tks_f|0|0|2018-12-05T17:45:43.858903+00:00|68 days 20:53:05
tpl_drupal|0|0|2018-12-05T17:45:45.734894+00:00|68 days 20:53:03
vegas|0|0|2018-12-05T17:44:58.82623+00:00|68 days 20:53:50
vividcortex|0|0|2018-12-05T17:45:43.675804+00:00|68 days 20:53:05


### Replica servers: ###

#### Replica (`db3`) ####

#### Timeouts ####
Setting name | Value | Unit | Pretty value
-------------|-------|------|--------------
[authentication_timeout](https://postgresqlco.nf/en/doc/param/authentication_timeout)|60|s|
[idle_in_transaction_session_timeout](https://postgresqlco.nf/en/doc/param/idle_in_transaction_session_timeout)|600000|ms|
[statement_timeout](https://postgresqlco.nf/en/doc/param/statement_timeout)|120000|ms|

#### Locks ####
Setting name | Value | Unit | Pretty value
-------------|-------|------|--------------
[deadlock_timeout](https://postgresqlco.nf/en/doc/param/deadlock_timeout)|1000|ms|
[lock_timeout](https://postgresqlco.nf/en/doc/param/lock_timeout)|0|ms|
[max_locks_per_transaction](https://postgresqlco.nf/en/doc/param/max_locks_per_transaction)|64|<no value>|
[max_pred_locks_per_transaction](https://postgresqlco.nf/en/doc/param/max_pred_locks_per_transaction)|64|<no value>|


#### Database specified settings ####
Database | Setting
---------|---------
test2 | [lock_timeout=2s]

#### User specified settings ####
User | Setting
---------|---------
tester2 | [lock_timeout=3s]

#### Databases data ####
Database | Conflicts | &#9660;&nbsp;Deadlocks | Stats reset at | Stat reset
-------------|-------|-----------|----------------|------------
_4vkusa_ru|0|0|2019-01-25T21:36:38.456004+00:00|17 days 17:01:19
dba|0|0|2019-01-25T21:36:35.966287+00:00|17 days 17:01:21
demotivator_prikolisti_com|0|0|2019-01-25T21:36:56.948846+00:00|17 days 17:01:00
fffail_ru|0|0|2019-01-25T21:37:03.660711+00:00|17 days 17:00:53
foto_prikolisti_com|0|0|2019-01-25T21:36:44.85309+00:00|17 days 17:01:12
fotominuta_ru|0|0|2019-01-25T21:36:40.133172+00:00|17 days 17:01:17
kotomail_ru|0|0|2019-01-25T21:36:43.680659+00:00|17 days 17:01:13
nevada|0|0|2019-01-25T21:36:47.685161+00:00|17 days 17:01:09
nevada1|0|0|2019-01-25T21:36:54.529187+00:00|17 days 17:01:02
nevada2|0|0|2019-01-25T21:36:34.840094+00:00|17 days 17:01:22
nevada3|0|0|2019-01-25T21:36:53.682205+00:00|17 days 17:01:03
nevada4|0|0|2019-01-25T21:37:06.187272+00:00|17 days 17:00:51
nevada5|0|0|2019-01-25T21:37:17.688604+00:00|17 days 17:00:39
nevada6|0|0|2019-01-25T21:37:19.081371+00:00|17 days 17:00:38
nevada7|0|0|2019-01-25T21:36:55.154385+00:00|17 days 17:01:02
nevada8|0|0|2019-01-25T21:36:56.018447+00:00|17 days 17:01:01
nombox_co|0|0|2019-01-25T21:36:42.041178+00:00|17 days 17:01:15
postfix|0|0|2019-01-25T21:36:59.376154+00:00|17 days 17:00:58
postgres|0|0|2019-01-25T21:36:09.489564+00:00|17 days 17:01:47
postgresmen_ru|0|0|2019-01-25T21:37:00.583841+00:00|17 days 17:00:56
postila_ru|251|0|2019-01-25T21:36:13.769138+00:00|17 days 17:01:43
prikolisti_com|0|0|2019-01-25T21:37:12.601014+00:00|17 days 17:00:44
redash|0|0|2019-01-25T21:37:09.144727+00:00|17 days 17:00:48
startupturbo_com|0|0|2019-01-25T21:37:15.204257+00:00|17 days 17:00:42
startupturbo_com_old|0|0|2019-01-25T21:36:36.362741+00:00|17 days 17:01:21
strakhovm_ru|0|0|2019-01-25T21:36:51.480363+00:00|17 days 17:01:05
svoi_simplead_ru|0|0|2019-01-25T21:36:46.353925+00:00|17 days 17:01:11
test|0|0|2019-01-25T21:36:35.541779+00:00|17 days 17:01:21
test2|0|0|2019-01-25T21:37:02.710689+00:00|17 days 17:00:54
tks_f|0|0|2019-01-25T21:36:37.526769+00:00|17 days 17:01:19
tpl_drupal|0|0|2019-01-25T21:37:10.063064+00:00|17 days 17:00:47
vegas|0|0|2019-01-25T21:37:07.38903+00:00|17 days 17:00:50
vividcortex|0|0|2019-01-25T21:36:35.243867+00:00|17 days 17:01:22



## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_H001"></a>
[Table of contents](#postgres-checkup_top)
# H001 Invalid indexes #

## Observations ##



### Master (`db2`) ###

Invalid indexes not found

## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_H002"></a>
[Table of contents](#postgres-checkup_top)
# H002 Unused/Rarely Used Indexes #

## Observations ##





Stats reset: 2 mons 6 days 20:54:00 ago (2018-12-05 17:44:47 +0000 +0000)  
Report created: 2019-02-12 17:38:49 +0300 MSK  


### Never Used Indexes ###
Index | db2 usage | db3 usage | &#9660;&nbsp;Index size | Usage
--------|-------|--------|-----|-----
u_bot_view_row_id |0|0|Index&nbsp;size:&nbsp;20&nbsp;GB<br/>Table&nbsp;size:&nbsp;58&nbsp;GB |Not used 
job_move_old_binimages_status |0|0|Index&nbsp;size:&nbsp;201&nbsp;MB<br/>Table&nbsp;size:&nbsp;329&nbsp;MB |Not used 
pnct_user_id_role__partial |0|0|Index&nbsp;size:&nbsp;128&nbsp;MB<br/>Table&nbsp;size:&nbsp;2461&nbsp;MB |Not used 
job_move_old_images_status |0|0|Index&nbsp;size:&nbsp;125&nbsp;MB<br/>Table&nbsp;size:&nbsp;639&nbsp;MB |Not used 
pnct_AnyPost_created_likes_idx |0|0|Index&nbsp;size:&nbsp;82&nbsp;MB<br/>Table&nbsp;size:&nbsp;1388&nbsp;MB |Not used 
i_postfix_list4mar2018 |0|0|Index&nbsp;size:&nbsp;61&nbsp;MB<br/>Table&nbsp;size:&nbsp;128&nbsp;MB |Not used 
i_user_avatar__notnull |0|0|Index&nbsp;size:&nbsp;54&nbsp;MB<br/>Table&nbsp;size:&nbsp;2461&nbsp;MB |Not used 
i_any_post_picture__notnull |0|0|Index&nbsp;size:&nbsp;18&nbsp;MB<br/>Table&nbsp;size:&nbsp;1388&nbsp;MB |Not used 
i_tmp_13 |0|0|Index&nbsp;size:&nbsp;10032&nbsp;kB<br/>Table&nbsp;size:&nbsp;6319&nbsp;MB |Not used 
i_tmp_killme3 |0|0|Index&nbsp;size:&nbsp;6160&nbsp;kB<br/>Table&nbsp;size:&nbsp;32&nbsp;GB |Not used 
i_board_cover__notnull |0|0|Index&nbsp;size:&nbsp;4088&nbsp;kB<br/>Table&nbsp;size:&nbsp;261&nbsp;MB |Not used 
pnct_AnyBoard_lower_idx |0|0|Index&nbsp;size:&nbsp;2720&nbsp;kB<br/>Table&nbsp;size:&nbsp;6808&nbsp;kB |Not used 
i_archive_log_id_ring__boards |0|<no value>|Index&nbsp;size:&nbsp;1992&nbsp;kB<br/>Table&nbsp;size:&nbsp;539&nbsp;MB |Not used 
pnct_AnyBoard_recommended_int4_idx |0|0|Index&nbsp;size:&nbsp;1896&nbsp;kB<br/>Table&nbsp;size:&nbsp;6808&nbsp;kB |Not used 
i_tmp_killme4 |0|0|Index&nbsp;size:&nbsp;600&nbsp;kB<br/>Table&nbsp;size:&nbsp;1388&nbsp;MB |Not used 
i_any_board_cover__notnull |0|0|Index&nbsp;size:&nbsp;312&nbsp;kB<br/>Table&nbsp;size:&nbsp;6808&nbsp;kB |Not used 
pghero_space_stats_database_captured_at_idx |0|0|Index&nbsp;size:&nbsp;32&nbsp;kB<br/>Table&nbsp;size:&nbsp;40&nbsp;kB |Not used 
i_ml_kusochek_tsvector_top_words_word |0|0|Index&nbsp;size:&nbsp;16&nbsp;kB<br/>Table&nbsp;size:&nbsp;8192&nbsp;bytes |Not used 
pghero_query_stats_database_captured_at_idx |0|0|Index&nbsp;size:&nbsp;16&nbsp;kB<br/>Table&nbsp;size:&nbsp;32&nbsp;kB |Not used 
i_ml_kusochek_top_words_word |0|0|Index&nbsp;size:&nbsp;16&nbsp;kB<br/>Table&nbsp;size:&nbsp;8192&nbsp;bytes |Not used 


### Other unused indexes ###
Index | Reason |db2 | db3 | Usage
------|--------|-------|--------|-----
i_network_follow_provider_id | Low Scans, High Writes | Usage:&nbsp;1.190311e+06<br/>Index&nbsp;size:12&nbsp;GB<br/>Table&nbsp;size:39&nbsp;GB  | Usage:&nbsp;119025<br/>Index&nbsp;size:12&nbsp;GB<br/>Table&nbsp;size:39&nbsp;GB|  Used
i_pnct_network_follow_lower_email | Low Scans, High Writes | Usage:&nbsp;482163<br/>Index&nbsp;size:9206&nbsp;MB<br/>Table&nbsp;size:39&nbsp;GB  | Usage:&nbsp;71406<br/>Index&nbsp;size:9206&nbsp;MB<br/>Table&nbsp;size:39&nbsp;GB|  Used
u_post_subtitles_uniq | Low Scans, High Writes | Usage:&nbsp;1.839543e+06<br/>Index&nbsp;size:8699&nbsp;MB<br/>Table&nbsp;size:111&nbsp;GB  | Usage:&nbsp;<no value><br/>Index&nbsp;size:<br/>Table&nbsp;size:|  Used
i_post_moderation | Low Scans, High Writes | Usage:&nbsp;51<br/>Index&nbsp;size:5012&nbsp;MB<br/>Table&nbsp;size:32&nbsp;GB  | Usage:&nbsp;136<br/>Index&nbsp;size:5012&nbsp;MB<br/>Table&nbsp;size:32&nbsp;GB|  Used
i_network_follow_follow_used | Low Scans, High Writes | Usage:&nbsp;228229<br/>Index&nbsp;size:3820&nbsp;MB<br/>Table&nbsp;size:39&nbsp;GB  | Usage:&nbsp;0<br/>Index&nbsp;size:3820&nbsp;MB<br/>Table&nbsp;size:39&nbsp;GB|  Used
i_user_visits_postgrest_auth | Low Scans, High Writes | Usage:&nbsp;146<br/>Index&nbsp;size:2455&nbsp;MB<br/>Table&nbsp;size:14&nbsp;GB  | Usage:&nbsp;0<br/>Index&nbsp;size:2455&nbsp;MB<br/>Table&nbsp;size:14&nbsp;GB|  Used
i_picture_post_md5__notnull | Low Scans, High Writes | Usage:&nbsp;464882<br/>Index&nbsp;size:2253&nbsp;MB<br/>Table&nbsp;size:32&nbsp;GB  | Usage:&nbsp;0<br/>Index&nbsp;size:2253&nbsp;MB<br/>Table&nbsp;size:32&nbsp;GB|  Used
pnct_picturepost_sourcedomain | Low Scans, High Writes | Usage:&nbsp;2.244002e+06<br/>Index&nbsp;size:1908&nbsp;MB<br/>Table&nbsp;size:32&nbsp;GB  | Usage:&nbsp;3.239759e+06<br/>Index&nbsp;size:1908&nbsp;MB<br/>Table&nbsp;size:32&nbsp;GB|  Used
i_post_subtitles_postid_created_ru_index | Low Scans, High Writes | Usage:&nbsp;50000<br/>Index&nbsp;size:1728&nbsp;MB<br/>Table&nbsp;size:111&nbsp;GB  | Usage:&nbsp;0<br/>Index&nbsp;size:1728&nbsp;MB<br/>Table&nbsp;size:111&nbsp;GB|  Used
i_pnct_user_lower_firstname_is_male | Low Scans, High Writes | Usage:&nbsp;15069<br/>Index&nbsp;size:312&nbsp;MB<br/>Table&nbsp;size:2461&nbsp;MB  | Usage:&nbsp;0<br/>Index&nbsp;size:312&nbsp;MB<br/>Table&nbsp;size:2461&nbsp;MB|  Used
i_network_follow_provider_id_by_provider | Low Scans, High Writes | Usage:&nbsp;139314<br/>Index&nbsp;size:272&nbsp;MB<br/>Table&nbsp;size:39&nbsp;GB  | Usage:&nbsp;23802<br/>Index&nbsp;size:272&nbsp;MB<br/>Table&nbsp;size:39&nbsp;GB|  Used
pnct_user_role | Low Scans, High Writes | Usage:&nbsp;13012<br/>Index&nbsp;size:249&nbsp;MB<br/>Table&nbsp;size:2461&nbsp;MB  | Usage:&nbsp;0<br/>Index&nbsp;size:249&nbsp;MB<br/>Table&nbsp;size:2461&nbsp;MB|  Used
i_board_target_age_from_to_gender_recommended | Low Scans, High Writes | Usage:&nbsp;43477<br/>Index&nbsp;size:208&nbsp;MB<br/>Table&nbsp;size:261&nbsp;MB  | Usage:&nbsp;10467<br/>Index&nbsp;size:208&nbsp;MB<br/>Table&nbsp;size:261&nbsp;MB|  Used
i_post_user_id | Low Scans, High Writes | Usage:&nbsp;13242<br/>Index&nbsp;size:163&nbsp;MB<br/>Table&nbsp;size:3452&nbsp;MB  | Usage:&nbsp;0<br/>Index&nbsp;size:163&nbsp;MB<br/>Table&nbsp;size:3452&nbsp;MB|  Used
i_board_categoryid_editors_choice | Low Scans, High Writes | Usage:&nbsp;97473<br/>Index&nbsp;size:153&nbsp;MB<br/>Table&nbsp;size:261&nbsp;MB  | Usage:&nbsp;216263<br/>Index&nbsp;size:153&nbsp;MB<br/>Table&nbsp;size:261&nbsp;MB|  Used
i_board_lower_title | Low Scans, High Writes | Usage:&nbsp;1567<br/>Index&nbsp;size:124&nbsp;MB<br/>Table&nbsp;size:261&nbsp;MB  | Usage:&nbsp;23329<br/>Index&nbsp;size:124&nbsp;MB<br/>Table&nbsp;size:261&nbsp;MB|  Used
pnct_AnyPost_lower_idx | Low Scans, High Writes | Usage:&nbsp;20675<br/>Index&nbsp;size:85&nbsp;MB<br/>Table&nbsp;size:1388&nbsp;MB  | Usage:&nbsp;<no value><br/>Index&nbsp;size:<br/>Table&nbsp;size:|  Used
i_board_by_posts_and_last_post_added3 | Low Scans, High Writes | Usage:&nbsp;42710<br/>Index&nbsp;size:60&nbsp;MB<br/>Table&nbsp;size:261&nbsp;MB  | Usage:&nbsp;<no value><br/>Index&nbsp;size:<br/>Table&nbsp;size:|  Used
pnct_AnyBoard_target_age_from_target_age_to_target_gender_r_idx | Low Scans, High Writes | Usage:&nbsp;11665<br/>Index&nbsp;size:3776&nbsp;kB<br/>Table&nbsp;size:6808&nbsp;kB  | Usage:&nbsp;<no value><br/>Index&nbsp;size:<br/>Table&nbsp;size:|  Used
i_picturepost_parentid_id2 | Seldom Used Large Indexes | Usage:&nbsp;1.4822764e+07<br/>Index&nbsp;size:1925&nbsp;MB<br/>Table&nbsp;size:32&nbsp;GB  | Usage:&nbsp;4.322447e+07<br/>Index&nbsp;size:1925&nbsp;MB<br/>Table&nbsp;size:32&nbsp;GB|  Used
pnct_AnyPost_image_original_md5_idx | Seldom Used Large Indexes | Usage:&nbsp;454579<br/>Index&nbsp;size:116&nbsp;MB<br/>Table&nbsp;size:1388&nbsp;MB  | Usage:&nbsp;0<br/>Index&nbsp;size:116&nbsp;MB<br/>Table&nbsp;size:1388&nbsp;MB|  Used

### Redundant indexes ###

Index | db2 usage | db3 usage | Usage | Index size
--------|-------|--------|-----|-----
i_ml_kusochek_top_words_word | <no value>|0| Not used  | 16 kB
i_ml_kusochek_tsvector_top_words_word | <no value>|0| Not used  | 16 kB
i_post_subtitles_postid_created_ru_index | <no value>|0| Not used  | 1728 MB






## Conclusions ##


## Recommendations ##

#### "DO" database migration code ####
```
DROP INDEX CONCURRENTLY public.i_any_board_cover__notnull; -- 312 kB, unused (idx_scan: 0), table pnct_AnyBoard
DROP INDEX CONCURRENTLY public.i_any_post_picture__notnull; -- 18 MB, unused (idx_scan: 0), table pnct_AnyPost
DROP INDEX CONCURRENTLY archive.i_archive_log_id_ring__boards; -- 1992 kB, unused (idx_scan: 0), table log
DROP INDEX CONCURRENTLY public.i_board_cover__notnull; -- 4088 kB, unused (idx_scan: 0), table pnct_Board
DROP INDEX CONCURRENTLY ml.i_ml_kusochek_top_words_word; -- 16 kB, unused (idx_scan: 0), table kusochek_top_words
DROP INDEX CONCURRENTLY ml.i_ml_kusochek_tsvector_top_words_word; -- 16 kB, unused (idx_scan: 0), table kusochek_top_words
DROP INDEX CONCURRENTLY postfix.i_postfix_list4mar2018; -- 61 MB, unused (idx_scan: 0), table list_4_mar_2018
DROP INDEX CONCURRENTLY translation_proxy.i_tmp_13; -- 10032 kB, unused (idx_scan: 0), table cache
DROP INDEX CONCURRENTLY public.i_tmp_killme3; -- 6160 kB, unused (idx_scan: 0), table pnct_PicturePost
DROP INDEX CONCURRENTLY public.i_tmp_killme4; -- 600 kB, unused (idx_scan: 0), table pnct_AnyPost
DROP INDEX CONCURRENTLY public.i_user_avatar__notnull; -- 54 MB, unused (idx_scan: 0), table pnct_User
DROP INDEX CONCURRENTLY public.job_move_old_binimages_status; -- 201 MB, unused (idx_scan: 0), table job_move_old_binimages
DROP INDEX CONCURRENTLY public.job_move_old_images_status; -- 125 MB, unused (idx_scan: 0), table job_move_old_images
DROP INDEX CONCURRENTLY public.pghero_query_stats_database_captured_at_idx; -- 16 kB, unused (idx_scan: 0), table pghero_query_stats
DROP INDEX CONCURRENTLY public.pghero_space_stats_database_captured_at_idx; -- 32 kB, unused (idx_scan: 0), table pghero_space_stats
DROP INDEX CONCURRENTLY public.pnct_AnyBoard_lower_idx; -- 2720 kB, unused (idx_scan: 0), table pnct_AnyBoard
DROP INDEX CONCURRENTLY public.pnct_AnyBoard_recommended_int4_idx; -- 1896 kB, unused (idx_scan: 0), table pnct_AnyBoard
DROP INDEX CONCURRENTLY public.pnct_AnyPost_created_likes_idx; -- 82 MB, unused (idx_scan: 0), table pnct_AnyPost
DROP INDEX CONCURRENTLY public.pnct_user_id_role__partial; -- 128 MB, unused (idx_scan: 0), table pnct_User
DROP INDEX CONCURRENTLY public.u_bot_view_row_id; -- 20 GB, unused (idx_scan: 0), table bot_visit

```



#### "UNDO" database migration code ####
```
CREATE INDEX CONCURRENTLY i_any_board_cover__notnull ON public."pnct_AnyBoard" USING btree (cover) WHERE (cover > 0); -- table pnct_AnyBoard
CREATE INDEX CONCURRENTLY i_any_post_picture__notnull ON public."pnct_AnyPost" USING btree ("pictureId") WHERE ("pictureId" > 0); -- table pnct_AnyPost
CREATE INDEX CONCURRENTLY i_archive_log_id_ring__boards ON archive.log USING btree (((row_id % (1000)::bigint))) WHERE (table_name = ANY (ARRAY['pnct_Board'::text, 'pnct_AnyBoard'::text])); -- table log
CREATE INDEX CONCURRENTLY i_board_cover__notnull ON public."pnct_Board" USING btree (cover) WHERE (cover > 0); -- table pnct_Board
CREATE INDEX CONCURRENTLY i_ml_kusochek_top_words_word ON ml.kusochek_top_words USING btree (word); -- table kusochek_top_words
CREATE INDEX CONCURRENTLY i_ml_kusochek_tsvector_top_words_word ON ml.kusochek_top_words USING btree (word); -- table kusochek_top_words
CREATE INDEX CONCURRENTLY i_postfix_list4mar2018 ON postfix.list_4_mar_2018 USING btree (lower(esl_address)); -- table list_4_mar_2018
CREATE INDEX CONCURRENTLY i_tmp_13 ON translation_proxy.cache USING btree (api_engine) WHERE (result IS NULL); -- table cache
CREATE INDEX CONCURRENTLY i_tmp_killme3 ON public."pnct_PicturePost" USING btree (id) WHERE ((image_original IS NOT NULL) AND (image_original_md5 IS NULL)); -- table pnct_PicturePost
CREATE INDEX CONCURRENTLY i_tmp_killme4 ON public."pnct_AnyPost" USING btree (id) WHERE ((image_original IS NOT NULL) AND (image_original_md5 IS NULL)); -- table pnct_AnyPost
CREATE INDEX CONCURRENTLY i_user_avatar__notnull ON public."pnct_User" USING btree (avatar) WHERE (avatar > 0); -- table pnct_User
CREATE INDEX CONCURRENTLY job_move_old_binimages_status ON public.job_move_old_binimages USING btree (bin_id) WHERE (status IS NULL); -- table job_move_old_binimages
CREATE INDEX CONCURRENTLY job_move_old_images_status ON public.job_move_old_images USING btree (picture_id) WHERE (status IS NULL); -- table job_move_old_images
CREATE INDEX CONCURRENTLY pghero_query_stats_database_captured_at_idx ON public.pghero_query_stats USING btree (database, captured_at); -- table pghero_query_stats
CREATE INDEX CONCURRENTLY pghero_space_stats_database_captured_at_idx ON public.pghero_space_stats USING btree (database, captured_at); -- table pghero_space_stats
CREATE INDEX CONCURRENTLY "pnct_AnyBoard_lower_idx" ON public."pnct_AnyBoard" USING btree (lower(title)); -- table pnct_AnyBoard
CREATE INDEX CONCURRENTLY "pnct_AnyBoard_recommended_int4_idx" ON public."pnct_AnyBoard" USING btree (recommended DESC NULLS LAST, ((((posts)::double precision + round((((1)::double precision * ((20)::real / (86400)::double precision)) * date_part('epoch'::text, last_post_added)))))::integer) DESC NULLS LAST); -- table pnct_AnyBoard
CREATE INDEX CONCURRENTLY "pnct_AnyPost_created_likes_idx" ON public."pnct_AnyPost" USING btree (created, likes); -- table pnct_AnyPost
CREATE INDEX CONCURRENTLY pnct_user_id_role__partial ON public."pnct_User" USING btree (id) WHERE ((role = ANY (ARRAY['user'::text, 'administrator'::text])) AND (allow_u2b = 1)); -- table pnct_User
CREATE INDEX CONCURRENTLY u_bot_view_row_id ON public.bot_visit USING btree (row_id); -- table bot_visit

```
---
<a name="postgres-checkup_H003"></a>
[Table of contents](#postgres-checkup_top)
# H003 Non indexed foreign keys (or with bad indexes) #

## Observations ##

### Master (`db2`) ###

No data
### Replica servers: ###

#### Replica (`db3`) ####

No data


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_K001"></a>
[Table of contents](#postgres-checkup_top)
# K001 Globally aggregated query metrics

## Observations ##


### Master (`db2`) ###
Start: 2019-02-12T13:27:50.528347+00:00  
End: 2019-02-12T14:38:52.83089+00:00  
Period seconds: 4262.30254  
Period age: 01:11:02.302543  

Error (calls): 0.00 (0.00%)  
Error (total time): 0.00 (0.00%)

Calls | Total&nbsp;time | Rows | shared_blks_hit | shared_blks_read | shared_blks_dirtied | shared_blks_written | blk_read_time | blk_write_time | kcache_reads | kcache_writes | kcache_user_time_ms | kcache_system_time 
-------|------------|------|-----------------|------------------|---------------------|---------------------|---------------|----------------|--------------|---------------|---------------------|--------------------
1,989,112<br/>466.68/sec<br/>1.00/call<br/>100.00% |8,249,358.36&nbsp;ms<br/>1.935s/sec<br/>4ms/call<br/>100.00% |4,634,248<br/>1.09K/sec<br/>2.33/call<br/>100.00% |2,005,517,094&nbsp;blks<br/>470.53K&nbsp;blks/sec<br/>1.01K&nbsp;blks/call<br/>100.00% |19,001,420&nbsp;blks<br/>4.46K&nbsp;blks/sec<br/>9.55&nbsp;blks/call<br/>100.00% |296,613&nbsp;blks<br/>69.59&nbsp;blks/sec<br/>0.15&nbsp;blks/call<br/>100.00% |2,591&nbsp;blks<br/>0.61&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>100.00% |4,956,220.07&nbsp;ms<br/>1.162s/sec<br/>2ms/call<br/>100.00% |24.65&nbsp;ms<br/>0s/sec<br/>0s/call<br/>100.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00%




### Replica servers: ###

#### Replica (`db3`) ####

Start: 2019-02-12T13:26:52.771225+00:00  
End: 2019-02-12T14:38:02.052634+00:00  
Period seconds: 4269.28141  
Period age: 01:11:09.281409  

Calls | Total&nbsp;time | Rows | shared_blks_hit | shared_blks_read | shared_blks_dirtied | shared_blks_written | blk_read_time | blk_write_time | kcache_reads | kcache_writes | kcache_user_time_ms | kcache_system_time 
-------|------------|------|-----------------|------------------|---------------------|---------------------|---------------|----------------|--------------|---------------|---------------------|--------------------
4,520,601<br/>1.06K/sec<br/>1.00/call<br/>100.00% |15,016,169.84&nbsp;ms<br/>3.517s/sec<br/>3ms/call<br/>100.00% |13,784,246<br/>3.23K/sec<br/>3.05/call<br/>100.00% |8,368,715,165&nbsp;blks<br/>1.97M&nbsp;blks/sec<br/>1.86K&nbsp;blks/call<br/>100.00% |356,261,593&nbsp;blks<br/>83.45K&nbsp;blks/sec<br/>78.81&nbsp;blks/call<br/>100.00% |2,670&nbsp;blks<br/>0.63&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>100.00% |12,777&nbsp;blks<br/>2.99&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>100.00% |5,410,412.82&nbsp;ms<br/>1.267s/sec<br/>1ms/call<br/>100.00% |118.39&nbsp;ms<br/>0s/sec<br/>0s/call<br/>100.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00%


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_K002"></a>
[Table of contents](#postgres-checkup_top)
# K002 Workload type ("first word" analysis)

## Observations ##


### Master (`db2`) ###
Start: 2019-02-12T13:27:50.528347+00:00  
End: 2019-02-12T14:38:52.83089+00:00  
Period seconds: 4262.30254  
Period age: 01:11:02.302543  

Error (calls): 0.00 (0.00%)  
Error (total time): 0.00 (0.00%)

\# | Workload type | Calls | &#9660;&nbsp;Total&nbsp;time | Rows | shared_blks_hit | shared_blks_read | shared_blks_dirtied | shared_blks_written | blk_read_time | blk_write_time | kcache_reads | kcache_writes | kcache_user_time_ms | kcache_system_time 
----|-------|------------|------|-----------------|------------------|---------------------|---------------------|---------------|----------------|--------------|---------------|---------------------|--------------------|------- 
1 |select |1,656,675<br/>388.68/sec<br/>1.00/call<br/>83.29% |6,955,079.43&nbsp;ms<br/>1.631s/sec<br/>4ms/call<br/>84.31% |4,318,877<br/>1.02K/sec<br/>2.61/call<br/>93.19% |1,960,914,016&nbsp;blks<br/>460.06K&nbsp;blks/sec<br/>1.19K&nbsp;blks/call<br/>97.78% |18,440,137&nbsp;blks<br/>4.33K&nbsp;blks/sec<br/>11.13&nbsp;blks/call<br/>97.05% |14,540&nbsp;blks<br/>3.41&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>4.90% |2,512&nbsp;blks<br/>0.59&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>96.95% |3,789,545.52&nbsp;ms<br/>889ms/sec<br/>2ms/call<br/>76.46% |23.75&nbsp;ms<br/>0s/sec<br/>0s/call<br/>96.33% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00%
2 |insert |314,687<br/>73.83/sec<br/>1.00/call<br/>15.82% |1,186,227.05&nbsp;ms<br/>278ms/sec<br/>3ms/call<br/>14.38% |314,687<br/>73.83/sec<br/>1.00/call<br/>6.79% |2,533,964&nbsp;blks<br/>594.51&nbsp;blks/sec<br/>8.05&nbsp;blks/call<br/>0.13% |560,417&nbsp;blks<br/>131.48&nbsp;blks/sec<br/>1.78&nbsp;blks/call<br/>2.95% |281,803&nbsp;blks<br/>66.12&nbsp;blks/sec<br/>0.90&nbsp;blks/call<br/>95.01% |79&nbsp;blks<br/>0.02&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>3.05% |1,166,665.35&nbsp;ms<br/>273ms/sec<br/>3ms/call<br/>23.54% |0.91&nbsp;ms<br/>0s/sec<br/>0s/call<br/>3.67% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00%
3 |select ... for [no key] update |17,750<br/>4.16/sec<br/>1.00/call<br/>0.89% |108,051.88&nbsp;ms<br/>25ms/sec<br/>6ms/call<br/>1.31% |684<br/>0.16/sec<br/>0.04/call<br/>0.01% |42,069,114&nbsp;blks<br/>9.88K&nbsp;blks/sec<br/>2.38K&nbsp;blks/call<br/>2.10% |866&nbsp;blks<br/>0.20&nbsp;blks/sec<br/>0.05&nbsp;blks/call<br/>0.00% |270&nbsp;blks<br/>0.06&nbsp;blks/sec<br/>0.02&nbsp;blks/call<br/>0.09% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |9.20&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00%




### Replica servers: ###

#### Replica (`db3`) ####

Start: 2019-02-12T13:26:52.771225+00:00  
End: 2019-02-12T14:38:02.052634+00:00  
Period seconds: 4269.28141  
Period age: 01:11:09.281409  

\# | Workload type | Calls | &#9660;&nbsp;Total&nbsp;time | Rows | shared_blks_hit | shared_blks_read | shared_blks_dirtied | shared_blks_written | blk_read_time | blk_write_time | kcache_reads | kcache_writes | kcache_user_time_ms | kcache_system_time 
----|-------|------------|------|-----------------|------------------|---------------------|---------------------|---------------|----------------|--------------|---------------|---------------------|--------------------|------- 
1 |select |4,520,601<br/>1.06K/sec<br/>1.00/call<br/>100.00% |15,016,169.84&nbsp;ms<br/>3.517s/sec<br/>3ms/call<br/>100.00% |13,784,246<br/>3.23K/sec<br/>3.05/call<br/>100.00% |8,368,715,165&nbsp;blks<br/>1.97M&nbsp;blks/sec<br/>1.86K&nbsp;blks/call<br/>100.00% |356,261,593&nbsp;blks<br/>83.45K&nbsp;blks/sec<br/>78.81&nbsp;blks/call<br/>100.00% |2,670&nbsp;blks<br/>0.63&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>100.00% |12,777&nbsp;blks<br/>2.99&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>100.00% |5,410,412.82&nbsp;ms<br/>1.267s/sec<br/>1ms/call<br/>100.00% |118.39&nbsp;ms<br/>0s/sec<br/>0s/call<br/>100.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00%


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_K003"></a>
[Table of contents](#postgres-checkup_top)
# K003 Top-50 queries by total_time

## Observations ##


### Master (`db2`) ###
Start: 2019-02-12T13:27:50.528347+00:00  
End: 2019-02-12T14:38:52.83089+00:00  
Period seconds: 4262.30254  
Period age: 01:11:02.302543  

Error (calls): 0.00 (0.00%)  
Error (total time): 0.00 (0.00%)

\# | Calls | &#9660;&nbsp;Total&nbsp;time | Rows | shared_blks_hit | shared_blks_read | shared_blks_dirtied | shared_blks_written | blk_read_time | blk_write_time | kcache_reads | kcache_writes | kcache_user_time_ms | kcache_system_time | Query
----|-------|------------|------|-----------------|------------------|---------------------|---------------------|---------------|----------------|--------------|---------------|---------------------|--------------------|------- 
1 |24,575<br/>5.77/sec<br/>1.00/call<br/>1.24% |4,225,817.82&nbsp;ms<br/>991ms/sec<br/>171ms/call<br/>51.23% |479,388<br/>112.47/sec<br/>19.51/call<br/>10.34% |1,177,036,042&nbsp;blks<br/>276.16K&nbsp;blks/sec<br/>47.90K&nbsp;blks/call<br/>58.69% |12,428,957&nbsp;blks<br/>2.92K&nbsp;blks/sec<br/>505.76&nbsp;blks/call<br/>65.41% |273&nbsp;blks<br/>0.06&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>0.09% |2,086&nbsp;blks<br/>0.49&nbsp;blks/sec<br/>0.08&nbsp;blks/call<br/>80.51% |2,316,379.20&nbsp;ms<br/>543ms/sec<br/>94ms/call<br/>46.74% |19.22&nbsp;ms<br/>0s/sec<br/>0s/call<br/>77.98% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
2 |4,750<br/>1.11/sec<br/>1.00/call<br/>0.24% |869,014.61&nbsp;ms<br/>203ms/sec<br/>182ms/call<br/>10.53% |28,089<br/>6.59/sec<br/>5.91/call<br/>0.61% |132,236,464&nbsp;blks<br/>31.03K&nbsp;blks/sec<br/>27.84K&nbsp;blks/call<br/>6.59% |2,177,609&nbsp;blks<br/>510.90&nbsp;blks/sec<br/>458.44&nbsp;blks/call<br/>11.46% |56&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>0.02% |72&nbsp;blks<br/>0.02&nbsp;blks/sec<br/>0.02&nbsp;blks/call<br/>2.78% |397,057.22&nbsp;ms<br/>93ms/sec<br/>83ms/call<br/>8.01% |0.73&nbsp;ms<br/>0s/sec<br/>0s/call<br/>2.97% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
3 |6,266<br/>1.47/sec<br/>1.00/call<br/>0.32% |775,647.38&nbsp;ms<br/>181ms/sec<br/>123ms/call<br/>9.40% |49,249<br/>11.55/sec<br/>7.86/call<br/>1.06% |172,807,997&nbsp;blks<br/>40.55K&nbsp;blks/sec<br/>27.58K&nbsp;blks/call<br/>8.62% |2,326,293&nbsp;blks<br/>545.78&nbsp;blks/sec<br/>371.26&nbsp;blks/call<br/>12.24% |58&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>0.02% |116&nbsp;blks<br/>0.03&nbsp;blks/sec<br/>0.02&nbsp;blks/call<br/>4.48% |382,677.07&nbsp;ms<br/>89ms/sec<br/>61ms/call<br/>7.72% |1.12&nbsp;ms<br/>0s/sec<br/>0s/call<br/>4.53% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
4 |167,575<br/>39.32/sec<br/>1.00/call<br/>8.42% |756,148.63&nbsp;ms<br/>177ms/sec<br/>4ms/call<br/>9.17% |167,575<br/>39.32/sec<br/>1.00/call<br/>3.62% |1,165,839&nbsp;blks<br/>273.52&nbsp;blks/sec<br/>6.96&nbsp;blks/call<br/>0.06% |305,509&nbsp;blks<br/>71.68&nbsp;blks/sec<br/>1.82&nbsp;blks/call<br/>1.61% |137,201&nbsp;blks<br/>32.19&nbsp;blks/sec<br/>0.82&nbsp;blks/call<br/>46.26% |38&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>1.47% |745,759.00&nbsp;ms<br/>174ms/sec<br/>4ms/call<br/>15.05% |0.43&nbsp;ms<br/>0s/sec<br/>0s/call<br/>1.75% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |&nbsp;INSERT&nbsp;INTO&nbsp;post_view(post_id,&nbsp;cnt)&nbsp;VALUES&nbsp;(1776802,&nbsp;1)&nbsp;ON&nbsp;CONFLICT&nbsp;(post_id,&nbsp;view_date)&nbsp;DO&nbsp;UPDATE&nbsp;SET&nbsp;cnt&nbsp;=&nbsp;post_view.cnt&nbsp;+&nbsp;1&nbsp;WHERE&nbsp;post_view.post_id&nbsp;=&nbsp;EXCLUDED.post_id&nbsp;AND&nbsp;post_view.view_date&nbsp;=&nbsp;EXCLUDED.view_date&nbsp;;
5 |133,690<br/>31.37/sec<br/>1.00/call<br/>6.72% |389,969.61&nbsp;ms<br/>91ms/sec<br/>2ms/call<br/>4.73% |133,690<br/>31.37/sec<br/>1.00/call<br/>2.88% |1,251,138&nbsp;blks<br/>293.54&nbsp;blks/sec<br/>9.36&nbsp;blks/call<br/>0.06% |229,483&nbsp;blks<br/>53.84&nbsp;blks/sec<br/>1.72&nbsp;blks/call<br/>1.21% |132,458&nbsp;blks<br/>31.08&nbsp;blks/sec<br/>0.99&nbsp;blks/call<br/>44.66% |34&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>1.31% |382,065.59&nbsp;ms<br/>89ms/sec<br/>2ms/call<br/>7.71% |0.40&nbsp;ms<br/>0s/sec<br/>0s/call<br/>1.62% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |INSERT&nbsp;INTO&nbsp;bot_visit&nbsp;(table_name,&nbsp;row_id,&nbsp;bot)&nbsp;VALUES&nbsp;(?,&nbsp;?,&nbsp;?);
6 |18,527<br/>4.35/sec<br/>1.00/call<br/>0.93% |111,972.28&nbsp;ms<br/>26ms/sec<br/>6ms/call<br/>1.36% |18,527<br/>4.35/sec<br/>1.00/call<br/>0.40% |168,814,070&nbsp;blks<br/>39.61K&nbsp;blks/sec<br/>9.12K&nbsp;blks/call<br/>8.42% |268&nbsp;blks<br/>0.06&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |11.39&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;s.lid,&nbsp;t.translation,&nbsp;s.version&nbsp;FROM&nbsp;locales_source&nbsp;s&nbsp;LEFT&nbsp;JOIN&nbsp;locales_target&nbsp;t&nbsp;ON&nbsp;s.lid&nbsp;=&nbsp;t.lid&nbsp;AND&nbsp;t.language&nbsp;=&nbsp;?&nbsp;WHERE&nbsp;s.source&nbsp;=&nbsp;?&nbsp;AND&nbsp;s.context&nbsp;=&nbsp;?&nbsp;AND&nbsp;s.textgroup&nbsp;=&nbsp;?
7 |20,539<br/>4.82/sec<br/>1.00/call<br/>1.03% |111,038.85&nbsp;ms<br/>26ms/sec<br/>5ms/call<br/>1.35% |20,539<br/>4.82/sec<br/>1.00/call<br/>0.44% |176,300,014&nbsp;blks<br/>41.37K&nbsp;blks/sec<br/>8.59K&nbsp;blks/call<br/>8.79% |100&nbsp;blks<br/>0.02&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |212.48&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;s.lid,&nbsp;t.translation,&nbsp;s.version&nbsp;FROM&nbsp;locales_source&nbsp;s&nbsp;LEFT&nbsp;JOIN&nbsp;locales_target&nbsp;t&nbsp;ON&nbsp;s.lid&nbsp;=&nbsp;t.lid&nbsp;AND&nbsp;t.language&nbsp;=&nbsp;?&nbsp;WHERE&nbsp;s.source&nbsp;=&nbsp;?&nbsp;AND&nbsp;s.context&nbsp;=&nbsp;?&nbsp;AND&nbsp;s.textgroup&nbsp;=&nbsp;?
8 |6,286<br/>1.47/sec<br/>1.00/call<br/>0.32% |109,906.06&nbsp;ms<br/>25ms/sec<br/>17ms/call<br/>1.33% |50,288<br/>11.80/sec<br/>8.00/call<br/>1.09% |58,858,679&nbsp;blks<br/>13.81K&nbsp;blks/sec<br/>9.37K&nbsp;blks/call<br/>2.93% |476,992&nbsp;blks<br/>111.91&nbsp;blks/sec<br/>75.88&nbsp;blks/call<br/>2.51% |44&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>0.01% |53&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>2.05% |62,813.41&nbsp;ms<br/>14ms/sec<br/>9ms/call<br/>1.27% |0.49&nbsp;ms<br/>0s/sec<br/>0s/call<br/>1.99% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
9 |17,750<br/>4.16/sec<br/>1.00/call<br/>0.89% |108,051.88&nbsp;ms<br/>25ms/sec<br/>6ms/call<br/>1.31% |684<br/>0.16/sec<br/>0.04/call<br/>0.01% |42,069,114&nbsp;blks<br/>9.88K&nbsp;blks/sec<br/>2.38K&nbsp;blks/call<br/>2.10% |866&nbsp;blks<br/>0.20&nbsp;blks/sec<br/>0.05&nbsp;blks/call<br/>0.00% |270&nbsp;blks<br/>0.06&nbsp;blks/sec<br/>0.02&nbsp;blks/call<br/>0.09% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |9.20&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;row_id,&nbsp;try_countFROM&nbsp;public.conveyorWHERE&nbsp;table_name&nbsp;=&nbsp;?&nbsp;and&nbsp;operation&nbsp;=&nbsp;?&nbsp;and&nbsp;status&nbsp;=&nbsp;?&nbsp;AND&nbsp;changed&nbsp;<&nbsp;now()&nbsp;-&nbsp;((?&nbsp;^&nbsp;try_count)::text&nbsp;||&nbsp;?)::intervalORDER&nbsp;BY&nbsp;changed&nbsp;ascLIMIT&nbsp;?FOR&nbsp;UPDATE&nbsp;SKIP&nbsp;LOCKED;
10 |25,515<br/>5.99/sec<br/>1.00/call<br/>1.28% |69,634.67&nbsp;ms<br/>16ms/sec<br/>2ms/call<br/>0.84% |428,723<br/>100.58/sec<br/>16.80/call<br/>9.25% |315,987&nbsp;blks<br/>74.14&nbsp;blks/sec<br/>12.38&nbsp;blks/call<br/>0.02% |217,734&nbsp;blks<br/>51.08&nbsp;blks/sec<br/>8.53&nbsp;blks/call<br/>1.15% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |28&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>1.08% |64,220.18&nbsp;ms<br/>15ms/sec<br/>2ms/call<br/>1.30% |0.27&nbsp;ms<br/>0s/sec<br/>0s/call<br/>1.09% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_PicturePost"&nbsp;"t"&nbsp;WHERE&nbsp;(("boardId"=?)&nbsp;AND&nbsp;(t.id&nbsp;<&nbsp;?))&nbsp;AND&nbsp;(t.image_original&nbsp;is&nbsp;not&nbsp;null&nbsp;OR&nbsp;t."pictureId"&nbsp;is&nbsp;not&nbsp;null)&nbsp;ORDER&nbsp;BY&nbsp;t.id&nbsp;DESC&nbsp;LIMIT&nbsp;?
11 |124,989<br/>29.32/sec<br/>1.00/call<br/>6.28% |63,725.32&nbsp;ms<br/>14ms/sec<br/>0s/call<br/>0.77% |241,428<br/>56.64/sec<br/>1.93/call<br/>5.21% |1,057,688&nbsp;blks<br/>248.15&nbsp;blks/sec<br/>8.46&nbsp;blks/call<br/>0.05% |121,808&nbsp;blks<br/>28.58&nbsp;blks/sec<br/>0.97&nbsp;blks/call<br/>0.64% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |14&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.54% |55,405.51&nbsp;ms<br/>12ms/sec<br/>0s/call<br/>1.12% |0.14&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.58% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"avatarBin"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"avatarBin"."owner"&nbsp;AS&nbsp;"t1_c1",&nbsp;"avatarBin"."status"&nbsp;AS&nbsp;"t1_c2",&nbsp;"avatarBin"."created"&nbsp;AS&nbsp;"t1_c3",&nbsp;"files"."id"&nbsp;AS&nbsp;"t2_c0",&nbsp;"files"."binId"&nbsp;AS&nbsp;"t2_c1",&nbsp;"files"."name"&nbsp;AS&nbsp;"t2_c2",&nbsp;"files"."hash"&nbsp;AS&nbsp;"t2_c3",&nbsp;"files"."extension"&nbsp;AS&nbsp;"t2_c4",&nbsp;"files"."created"&nbsp;AS&nbsp;"t2_c5",&nbsp;"files"."size"&nbsp;AS&nbsp;"t2_c6",&nbsp;"files"."width"&nbsp;AS&nbsp;"t2_c7",&nbsp;"files"."height"&nbsp;AS&nbsp;"t2_c8",&nbsp;"files"."md5"&nbsp;AS&nbsp;"t2_c9"&nbsp;FROM&nbsp;"pnct_StorageBin"&nbsp;"avatarBin"&nbsp;LEFT&nbsp;OUTER&nbsp;JOIN&nbsp;"pnct_StorageFile"&nbsp;"files"&nbsp;ON&nbsp;("files"."binId"="avatarBin"."id")&nbsp;WHERE&nbsp;("avatarBin"."id"=?)
12 |14,404<br/>3.38/sec<br/>1.00/call<br/>0.72% |62,235.18&nbsp;ms<br/>14ms/sec<br/>4ms/call<br/>0.75% |39,529<br/>9.27/sec<br/>2.74/call<br/>0.85% |37,881&nbsp;blks<br/>8.89&nbsp;blks/sec<br/>2.63&nbsp;blks/call<br/>0.00% |24,460&nbsp;blks<br/>5.74&nbsp;blks/sec<br/>1.70&nbsp;blks/call<br/>0.13% |37&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.01% |6&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.23% |61,797.11&nbsp;ms<br/>14ms/sec<br/>4ms/call<br/>1.25% |0.09&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.36% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;begin_time,&nbsp;value&nbsp;from&nbsp;post_subtitles&nbsp;where&nbsp;post_id&nbsp;=&nbsp;?&nbsp;and&nbsp;language&nbsp;=&nbsp;?&nbsp;order&nbsp;by&nbsp;begin_time,&nbsp;id;
13 |4,745<br/>1.11/sec<br/>1.00/call<br/>0.24% |50,779.93&nbsp;ms<br/>11ms/sec<br/>10ms/call<br/>0.62% |28,482<br/>6.68/sec<br/>6.00/call<br/>0.61% |45,685,836&nbsp;blks<br/>10.72K&nbsp;blks/sec<br/>9.63K&nbsp;blks/call<br/>2.28% |116,092&nbsp;blks<br/>27.24&nbsp;blks/sec<br/>24.47&nbsp;blks/call<br/>0.61% |39&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>0.01% |69&nbsp;blks<br/>0.02&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>2.66% |16,434.85&nbsp;ms<br/>3ms/sec<br/>3ms/call<br/>0.33% |0.55&nbsp;ms<br/>0s/sec<br/>0s/call<br/>2.23% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
14 |2,792<br/>0.66/sec<br/>1.00/call<br/>0.14% |42,874.09&nbsp;ms<br/>10ms/sec<br/>15ms/call<br/>0.52% |6,484<br/>1.52/sec<br/>2.32/call<br/>0.14% |7,662&nbsp;blks<br/>1.80&nbsp;blks/sec<br/>2.74&nbsp;blks/call<br/>0.00% |6,781&nbsp;blks<br/>1.59&nbsp;blks/sec<br/>2.43&nbsp;blks/call<br/>0.04% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |42,776.45&nbsp;ms<br/>10ms/sec<br/>15ms/call<br/>0.86% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"fragments"."picture_id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"fragments"."index"&nbsp;AS&nbsp;"t1_c1",&nbsp;"fragments"."top"&nbsp;AS&nbsp;"t1_c2",&nbsp;"fragments"."bottom"&nbsp;AS&nbsp;"t1_c3",&nbsp;"fragments"."caption"&nbsp;AS&nbsp;"t1_c4"&nbsp;FROM&nbsp;"pnct_PictureFragment"&nbsp;"fragments"&nbsp;WHERE&nbsp;("fragments"."picture_id"=?)
15 |31,858<br/>7.47/sec<br/>1.00/call<br/>1.60% |38,585.56&nbsp;ms<br/>9ms/sec<br/>1ms/call<br/>0.47% |31,699<br/>7.44/sec<br/>1.00/call<br/>0.68% |102,589&nbsp;blks<br/>24.07&nbsp;blks/sec<br/>3.22&nbsp;blks/call<br/>0.01% |24,813&nbsp;blks<br/>5.82&nbsp;blks/sec<br/>0.78&nbsp;blks/call<br/>0.13% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |3&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.12% |37,784.97&nbsp;ms<br/>8ms/sec<br/>1ms/call<br/>0.76% |0.03&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.13% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_StorageBin"&nbsp;"t"&nbsp;WHERE&nbsp;"id"&nbsp;in&nbsp;(?)
16 |37,768<br/>8.86/sec<br/>1.00/call<br/>1.90% |31,683.55&nbsp;ms<br/>7ms/sec<br/>0s/call<br/>0.38% |143,551<br/>33.68/sec<br/>3.80/call<br/>3.10% |179,939&nbsp;blks<br/>42.22&nbsp;blks/sec<br/>4.76&nbsp;blks/call<br/>0.01% |32,274&nbsp;blks<br/>7.57&nbsp;blks/sec<br/>0.85&nbsp;blks/call<br/>0.17% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |3&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.12% |30,390.73&nbsp;ms<br/>7ms/sec<br/>0s/call<br/>0.61% |0.03&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.11% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_StorageFile"&nbsp;"t"&nbsp;WHERE&nbsp;"binId"&nbsp;in&nbsp;(?)
17 |668,862<br/>156.93/sec<br/>1.00/call<br/>33.63% |31,639.51&nbsp;ms<br/>7ms/sec<br/>0s/call<br/>0.38% |668,862<br/>156.93/sec<br/>1.00/call<br/>14.43% |2,734,149&nbsp;blks<br/>641.47&nbsp;blks/sec<br/>4.09&nbsp;blks/call<br/>0.14% |3,172&nbsp;blks<br/>0.74&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.02% |33&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.01% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |2,513.36&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.05% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"user"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"user"."email"&nbsp;AS&nbsp;"t1_c1",&nbsp;"user"."password"&nbsp;AS&nbsp;"t1_c2",&nbsp;"user"."salt"&nbsp;AS&nbsp;"t1_c3",&nbsp;"user"."changePassword"&nbsp;AS&nbsp;"t1_c4",&nbsp;"user"."role"&nbsp;AS&nbsp;"t1_c5",&nbsp;"user"."created"&nbsp;AS&nbsp;"t1_c6",&nbsp;"user"."firstName"&nbsp;AS&nbsp;"t1_c7",&nbsp;"user"."lastName"&nbsp;AS&nbsp;"t1_c8",&nbsp;"user"."username"&nbsp;AS&nbsp;"t1_c9",&nbsp;"user"."avatar"&nbsp;AS&nbsp;"t1_c10",&nbsp;"user"."timeZone"&nbsp;AS&nbsp;"t1_c11",&nbsp;"user"."ip"&nbsp;AS&nbsp;"t1_c12",&nbsp;"user"."language"&nbsp;AS&nbsp;"t1_c13",&nbsp;"user"."about"&nbsp;AS&nbsp;"t1_c14",&nbsp;"user"."birthday"&nbsp;AS&nbsp;"t1_c15",&nbsp;"user"."source"&nbsp;AS&nbsp;"t1_c16",&nbsp;"user"."newsletter"&nbsp;AS&nbsp;"t1_c17",&nbsp;"user"."is_male"&nbsp;AS&nbsp;"t1_c18",&nbsp;"user"."access"&nbsp;AS&nbsp;"t1_c19",&nbsp;"user"."inviter_email"&nbsp;AS&nbsp;"t1_c20",&nbsp;"user"."allow_u2b"&nbsp;AS&nbsp;"t1_c21",&nbsp;"user"."country"&nbsp;AS&nbsp;"t1_c22",&nbsp;"user"."city"&nbsp;AS&nbsp;"t1_c23",&nbsp;"user"."mailru_id"&nbsp;AS&nbsp;"t1_c24",&nbsp;"user"."facebook_id"&nbsp;AS&nbsp;"t1_c25",&nbsp;"user"."post_notify_mode"&nbsp;AS&nbsp;"t1_c26",&nbsp;"user"."friends_check_date"&nbsp;AS&nbsp;"t1_c27",&nbsp;"user"."is_editor"&nbsp;AS&nbsp;"t1_c28",&nbsp;"user"."main_page"&nbsp;AS&nbsp;"t1_c29",&nbsp;"user"."notified_about_search"&nbsp;AS&nbsp;"t1_c30",&nbsp;"user"."biz"&nbsp;AS&nbsp;"t1_c31",&nbsp;"user"."biz_about"&nbsp;AS&nbsp;"t1_c32",&nbsp;"user"."followers"&nbsp;AS&nbsp;"t1_c33",&nbsp;"user"."censor_trust_level"&nbsp;AS&nbsp;"t1_c34",&nbsp;"user"."odnoklassniki_id"&nbsp;AS&nbsp;"t1_c35",&nbsp;"user"."vk_id"&nbsp;AS&nbsp;"t1_c36",&nbsp;"user"."confirmed_18plus_policy"&nbsp;AS&nbsp;"t1_c37",&nbsp;"user"."misc"&nbsp;AS&nbsp;"t1_c38",&nbsp;"user"."img_src"&nbsp;AS&nbsp;"t1_c39",&nbsp;"user"."img_w"&nbsp;AS&nbsp;"t1_c40",&nbsp;"user"."img_h"&nbsp;AS&nbsp;"t1_c41",&nbsp;"user"."google_id"&nbsp;AS&nbsp;"t1_c42",&nbsp;"user"."invite_all_requested"&nbsp;AS&nbsp;"t1_c43",&nbsp;"user"."new_email"&nbsp;AS&nbsp;"t1_c44",&nbsp;"user"."forbid_invitations"&nbsp;AS&nbsp;"t1_c45"&nbsp;FROM&nbsp;"pnct_User"&nbsp;"user"&nbsp;WHERE&nbsp;("user"."id"=?)
18 |5,542<br/>1.30/sec<br/>1.00/call<br/>0.28% |25,547.35&nbsp;ms<br/>5ms/sec<br/>4ms/call<br/>0.31% |5,542<br/>1.30/sec<br/>1.00/call<br/>0.12% |33,630&nbsp;blks<br/>7.89&nbsp;blks/sec<br/>6.07&nbsp;blks/call<br/>0.00% |12,115&nbsp;blks<br/>2.84&nbsp;blks/sec<br/>2.19&nbsp;blks/call<br/>0.06% |4,897&nbsp;blks<br/>1.15&nbsp;blks/sec<br/>0.88&nbsp;blks/call<br/>1.65% |1&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.04% |24,696.29&nbsp;ms<br/>5ms/sec<br/>4ms/call<br/>0.50% |0.01&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.05% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |INSERT&nbsp;INTO&nbsp;post_view&nbsp;as&nbsp;pv&nbsp;(post_id,&nbsp;cnt,&nbsp;source_cnt)&nbsp;VALUES&nbsp;(?,&nbsp;?,&nbsp;?&nbsp;)ON&nbsp;CONFLICT&nbsp;(post_id,&nbsp;view_date)DO&nbsp;UPDATE&nbsp;SET&nbsp;cnt&nbsp;=&nbsp;pv.cnt&nbsp;+&nbsp;?,&nbsp;source_cnt&nbsp;=&nbsp;coalesce(pv.source_cnt,&nbsp;?::jsonb)&nbsp;||&nbsp;jsonb_build_object(?,&nbsp;(coalesce(pv.source_cnt->>?,&nbsp;?))::int&nbsp;+&nbsp;?)WHERE&nbsp;pv.post_id&nbsp;=&nbsp;?&nbsp;and&nbsp;pv.view_date&nbsp;=&nbsp;date&nbsp;?;
19 |6,288<br/>1.48/sec<br/>1.00/call<br/>0.32% |20,189.29&nbsp;ms<br/>4ms/sec<br/>3ms/call<br/>0.24% |12,576<br/>2.95/sec<br/>2.00/call<br/>0.27% |36,982&nbsp;blks<br/>8.68&nbsp;blks/sec<br/>5.88&nbsp;blks/call<br/>0.00% |13,330&nbsp;blks<br/>3.13&nbsp;blks/sec<br/>2.12&nbsp;blks/call<br/>0.07% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |1&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.04% |19,968.55&nbsp;ms<br/>4ms/sec<br/>3ms/call<br/>0.40% |0.01&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.04% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_StorageBin"&nbsp;"t"&nbsp;WHERE&nbsp;"id"&nbsp;in&nbsp;(?,?)
20 |6,996<br/>1.64/sec<br/>1.00/call<br/>0.35% |18,748.96&nbsp;ms<br/>4ms/sec<br/>2ms/call<br/>0.23% |34,980<br/>8.21/sec<br/>5.00/call<br/>0.75% |128,384&nbsp;blks<br/>30.12&nbsp;blks/sec<br/>18.35&nbsp;blks/call<br/>0.01% |11,495&nbsp;blks<br/>2.70&nbsp;blks/sec<br/>1.64&nbsp;blks/call<br/>0.06% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |3&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.12% |18,374.14&nbsp;ms<br/>4ms/sec<br/>2ms/call<br/>0.37% |0.03&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.11% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_StorageBin"&nbsp;"t"&nbsp;WHERE&nbsp;"id"&nbsp;in&nbsp;(?,?,?,?,?)
21 |16,088<br/>3.77/sec<br/>1.00/call<br/>0.81% |18,273.82&nbsp;ms<br/>4ms/sec<br/>1ms/call<br/>0.22% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |57,114&nbsp;blks<br/>13.40&nbsp;blks/sec<br/>3.55&nbsp;blks/call<br/>0.00% |7,304&nbsp;blks<br/>1.71&nbsp;blks/sec<br/>0.45&nbsp;blks/call<br/>0.04% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.08% |17,883.18&nbsp;ms<br/>4ms/sec<br/>1ms/call<br/>0.36% |0.13&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.53% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;votingapi_vote.*FROM&nbsp;votingapi_vote&nbsp;votingapi_voteWHERE&nbsp;(entity_type&nbsp;=&nbsp;?)&nbsp;AND&nbsp;(entity_id&nbsp;=&nbsp;?)&nbsp;AND&nbsp;(tag&nbsp;=&nbsp;?)&nbsp;AND&nbsp;(value_type&nbsp;=&nbsp;?)&nbsp;AND&nbsp;(uid&nbsp;=&nbsp;?)&nbsp;AND&nbsp;(vote_source&nbsp;=&nbsp;?)&nbsp;AND&nbsp;(timestamp&nbsp;=&nbsp;?)&nbsp;
22 |4,432<br/>1.04/sec<br/>1.00/call<br/>0.22% |17,474.58&nbsp;ms<br/>4ms/sec<br/>3ms/call<br/>0.21% |69,722<br/>16.36/sec<br/>15.73/call<br/>1.50% |87,999&nbsp;blks<br/>20.65&nbsp;blks/sec<br/>19.86&nbsp;blks/call<br/>0.00% |14,397&nbsp;blks<br/>3.38&nbsp;blks/sec<br/>3.25&nbsp;blks/call<br/>0.08% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.08% |17,041.04&nbsp;ms<br/>3ms/sec<br/>3ms/call<br/>0.34% |0.02&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.08% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_StorageFile"&nbsp;"t"&nbsp;WHERE&nbsp;"binId"&nbsp;in&nbsp;(?,?,?,?)
23 |4,432<br/>1.04/sec<br/>1.00/call<br/>0.22% |17,456.59&nbsp;ms<br/>4ms/sec<br/>3ms/call<br/>0.21% |17,728<br/>4.16/sec<br/>4.00/call<br/>0.38% |59,750&nbsp;blks<br/>14.02&nbsp;blks/sec<br/>13.48&nbsp;blks/call<br/>0.00% |11,149&nbsp;blks<br/>2.62&nbsp;blks/sec<br/>2.52&nbsp;blks/call<br/>0.06% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.08% |17,254.70&nbsp;ms<br/>4ms/sec<br/>3ms/call<br/>0.35% |0.03&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.10% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_StorageBin"&nbsp;"t"&nbsp;WHERE&nbsp;"id"&nbsp;in&nbsp;(?,?,?,?)
24 |9,541<br/>2.24/sec<br/>1.00/call<br/>0.48% |17,336.96&nbsp;ms<br/>4ms/sec<br/>1ms/call<br/>0.21% |57,246<br/>13.43/sec<br/>6.00/call<br/>1.24% |217,423&nbsp;blks<br/>51.01&nbsp;blks/sec<br/>22.79&nbsp;blks/call<br/>0.01% |11,404&nbsp;blks<br/>2.68&nbsp;blks/sec<br/>1.20&nbsp;blks/call<br/>0.06% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |1&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.04% |16,837.47&nbsp;ms<br/>3ms/sec<br/>1ms/call<br/>0.34% |0.01&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.05% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_StorageBin"&nbsp;"t"&nbsp;WHERE&nbsp;"id"&nbsp;in&nbsp;(?,?,?,?,?,?)
25 |9,541<br/>2.24/sec<br/>1.00/call<br/>0.48% |17,119.30&nbsp;ms<br/>4ms/sec<br/>1ms/call<br/>0.21% |228,408<br/>53.59/sec<br/>23.94/call<br/>4.93% |327,489&nbsp;blks<br/>76.83&nbsp;blks/sec<br/>34.32&nbsp;blks/call<br/>0.02% |15,070&nbsp;blks<br/>3.54&nbsp;blks/sec<br/>1.58&nbsp;blks/call<br/>0.08% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |16,191.45&nbsp;ms<br/>3ms/sec<br/>1ms/call<br/>0.33% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_StorageFile"&nbsp;"t"&nbsp;WHERE&nbsp;"binId"&nbsp;in&nbsp;(?,?,?,?,?,?)
26 |11,561<br/>2.71/sec<br/>1.00/call<br/>0.58% |16,635.34&nbsp;ms<br/>3ms/sec<br/>1ms/call<br/>0.20% |481,240<br/>112.91/sec<br/>41.63/call<br/>10.38% |4,502,027&nbsp;blks<br/>1.06K&nbsp;blks/sec<br/>389.42&nbsp;blks/call<br/>0.22% |107,661&nbsp;blks<br/>25.26&nbsp;blks/sec<br/>9.31&nbsp;blks/call<br/>0.57% |6&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |16&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.62% |11,712.02&nbsp;ms<br/>2ms/sec<br/>1ms/call<br/>0.24% |0.20&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.83% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"pictureId"FROM&nbsp;"pnct_AnyPost"WHERE&nbsp;("id"&nbsp;IN&nbsp;(?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?))&nbsp;AND&nbsp;("pictureId"&nbsp;IS&nbsp;NOT&nbsp;NULL)
27 |4,130<br/>0.97/sec<br/>1.00/call<br/>0.21% |16,366.07&nbsp;ms<br/>3ms/sec<br/>3ms/call<br/>0.20% |12,390<br/>2.91/sec<br/>3.00/call<br/>0.27% |37,991&nbsp;blks<br/>8.91&nbsp;blks/sec<br/>9.20&nbsp;blks/call<br/>0.00% |11,572&nbsp;blks<br/>2.71&nbsp;blks/sec<br/>2.80&nbsp;blks/call<br/>0.06% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |16,208.32&nbsp;ms<br/>3ms/sec<br/>3ms/call<br/>0.33% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_StorageBin"&nbsp;"t"&nbsp;WHERE&nbsp;"id"&nbsp;in&nbsp;(?,?,?)
28 |4,758<br/>1.12/sec<br/>1.00/call<br/>0.24% |16,053.50&nbsp;ms<br/>3ms/sec<br/>3ms/call<br/>0.19% |151,948<br/>35.65/sec<br/>31.94/call<br/>3.28% |211,278&nbsp;blks<br/>49.57&nbsp;blks/sec<br/>44.40&nbsp;blks/call<br/>0.01% |15,927&nbsp;blks<br/>3.74&nbsp;blks/sec<br/>3.35&nbsp;blks/call<br/>0.08% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |4&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.15% |15,465.53&nbsp;ms<br/>3ms/sec<br/>3ms/call<br/>0.31% |0.04&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.16% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_StorageFile"&nbsp;"t"&nbsp;WHERE&nbsp;"binId"&nbsp;in&nbsp;(?,?,?,?,?,?,?,?)
29 |4,130<br/>0.97/sec<br/>1.00/call<br/>0.21% |15,952.99&nbsp;ms<br/>3ms/sec<br/>3ms/call<br/>0.19% |47,869<br/>11.23/sec<br/>11.59/call<br/>1.03% |55,265&nbsp;blks<br/>12.97&nbsp;blks/sec<br/>13.38&nbsp;blks/call<br/>0.00% |14,738&nbsp;blks<br/>3.46&nbsp;blks/sec<br/>3.57&nbsp;blks/call<br/>0.08% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |4&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.15% |15,698.85&nbsp;ms<br/>3ms/sec<br/>3ms/call<br/>0.32% |0.04&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.15% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_StorageFile"&nbsp;"t"&nbsp;WHERE&nbsp;"binId"&nbsp;in&nbsp;(?,?,?)
30 |6,996<br/>1.64/sec<br/>1.00/call<br/>0.35% |15,750.45&nbsp;ms<br/>3ms/sec<br/>2ms/call<br/>0.19% |139,309<br/>32.68/sec<br/>19.91/call<br/>3.01% |192,093&nbsp;blks<br/>45.07&nbsp;blks/sec<br/>27.46&nbsp;blks/call<br/>0.01% |14,944&nbsp;blks<br/>3.51&nbsp;blks/sec<br/>2.14&nbsp;blks/call<br/>0.08% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.08% |15,109.45&nbsp;ms<br/>3ms/sec<br/>2ms/call<br/>0.30% |0.02&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.07% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_StorageFile"&nbsp;"t"&nbsp;WHERE&nbsp;"binId"&nbsp;in&nbsp;(?,?,?,?,?)
31 |7,880<br/>1.85/sec<br/>1.00/call<br/>0.40% |14,561.46&nbsp;ms<br/>3ms/sec<br/>1ms/call<br/>0.18% |7,880<br/>1.85/sec<br/>1.00/call<br/>0.17% |83,357&nbsp;blks<br/>19.56&nbsp;blks/sec<br/>10.58&nbsp;blks/call<br/>0.00% |13,310&nbsp;blks<br/>3.12&nbsp;blks/sec<br/>1.69&nbsp;blks/call<br/>0.07% |7,247&nbsp;blks<br/>1.70&nbsp;blks/sec<br/>0.92&nbsp;blks/call<br/>2.44% |6&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.23% |14,144.46&nbsp;ms<br/>3ms/sec<br/>1ms/call<br/>0.29% |0.06&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.25% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |INSERT&nbsp;INTO&nbsp;click(user_id,&nbsp;post_id,&nbsp;url,&nbsp;domain)&nbsp;VALUES(?,&nbsp;?,&nbsp;?,&nbsp;?);
32 |8,904<br/>2.09/sec<br/>1.00/call<br/>0.45% |14,062.02&nbsp;ms<br/>3ms/sec<br/>1ms/call<br/>0.17% |248,679<br/>58.34/sec<br/>27.93/call<br/>5.37% |361,156&nbsp;blks<br/>84.73&nbsp;blks/sec<br/>40.56&nbsp;blks/call<br/>0.02% |13,414&nbsp;blks<br/>3.15&nbsp;blks/sec<br/>1.51&nbsp;blks/call<br/>0.07% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.08% |13,102.27&nbsp;ms<br/>3ms/sec<br/>1ms/call<br/>0.26% |0.02&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.09% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_StorageFile"&nbsp;"t"&nbsp;WHERE&nbsp;"binId"&nbsp;in&nbsp;(?,?,?,?,?,?,?)
33 |8,904<br/>2.09/sec<br/>1.00/call<br/>0.45% |14,047.50&nbsp;ms<br/>3ms/sec<br/>1ms/call<br/>0.17% |62,328<br/>14.62/sec<br/>7.00/call<br/>1.34% |239,098&nbsp;blks<br/>56.10&nbsp;blks/sec<br/>26.85&nbsp;blks/call<br/>0.01% |10,094&nbsp;blks<br/>2.37&nbsp;blks/sec<br/>1.13&nbsp;blks/call<br/>0.05% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |13,538.88&nbsp;ms<br/>3ms/sec<br/>1ms/call<br/>0.27% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_StorageBin"&nbsp;"t"&nbsp;WHERE&nbsp;"id"&nbsp;in&nbsp;(?,?,?,?,?,?,?)
34 |6,288<br/>1.48/sec<br/>1.00/call<br/>0.32% |13,980.36&nbsp;ms<br/>3ms/sec<br/>2ms/call<br/>0.17% |46,226<br/>10.85/sec<br/>7.35/call<br/>1.00% |53,715&nbsp;blks<br/>12.60&nbsp;blks/sec<br/>8.54&nbsp;blks/call<br/>0.00% |16,960&nbsp;blks<br/>3.98&nbsp;blks/sec<br/>2.70&nbsp;blks/call<br/>0.09% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |3&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.12% |13,700.72&nbsp;ms<br/>3ms/sec<br/>2ms/call<br/>0.28% |0.04&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.16% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_StorageFile"&nbsp;"t"&nbsp;WHERE&nbsp;"binId"&nbsp;in&nbsp;(?,?)
35 |377,403<br/>88.54/sec<br/>1.00/call<br/>18.97% |11,660.20&nbsp;ms<br/>2ms/sec<br/>0s/call<br/>0.14% |16,089<br/>3.77/sec<br/>0.04/call<br/>0.35% |1,135,809&nbsp;blks<br/>266.48&nbsp;blks/sec<br/>3.01&nbsp;blks/call<br/>0.06% |12,629&nbsp;blks<br/>2.96&nbsp;blks/sec<br/>0.03&nbsp;blks/call<br/>0.07% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.08% |7,963.82&nbsp;ms<br/>1ms/sec<br/>0s/call<br/>0.16% |0.02&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.09% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_Picture"&nbsp;"t"&nbsp;WHERE&nbsp;"id"&nbsp;in&nbsp;(?)
36 |14,404<br/>3.38/sec<br/>1.00/call<br/>0.72% |11,250.95&nbsp;ms<br/>2ms/sec<br/>0s/call<br/>0.14% |10,826<br/>2.54/sec<br/>0.75/call<br/>0.23% |52,125&nbsp;blks<br/>12.23&nbsp;blks/sec<br/>3.62&nbsp;blks/call<br/>0.00% |19,852&nbsp;blks<br/>4.66&nbsp;blks/sec<br/>1.38&nbsp;blks/call<br/>0.10% |6&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |5&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.19% |10,702.03&nbsp;ms<br/>2ms/sec<br/>0s/call<br/>0.22% |0.07&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.27% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_PicturePost"&nbsp;"t"&nbsp;WHERE&nbsp;t."parentId"&nbsp;=&nbsp;?&nbsp;ORDER&nbsp;BY&nbsp;t.id&nbsp;DESC&nbsp;LIMIT&nbsp;?
37 |2,767<br/>0.65/sec<br/>1.00/call<br/>0.14% |10,094.29&nbsp;ms<br/>2ms/sec<br/>3ms/call<br/>0.12% |13,835<br/>3.25/sec<br/>5.00/call<br/>0.30% |12,423,830&nbsp;blks<br/>2.92K&nbsp;blks/sec<br/>4.49K&nbsp;blks/call<br/>0.62% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;n.nid&nbsp;AS&nbsp;nid,&nbsp;n.title&nbsp;AS&nbsp;title,&nbsp;n.created&nbsp;AS&nbsp;ncreatedFROM&nbsp;node&nbsp;nINNER&nbsp;JOIN&nbsp;simplenews_newsletter&nbsp;sn&nbsp;ON&nbsp;n.nid&nbsp;=&nbsp;sn.nidWHERE&nbsp;(sn.tid&nbsp;=&nbsp;?)&nbsp;AND&nbsp;(n.status&nbsp;=&nbsp;?)&nbsp;AND&nbsp;(sn.status&nbsp;<>&nbsp;?)&nbsp;ORDER&nbsp;BY&nbsp;n.created&nbsp;DESCLIMIT&nbsp;?&nbsp;OFFSET&nbsp;?
38 |11,488<br/>2.70/sec<br/>1.00/call<br/>0.58% |9,133.86&nbsp;ms<br/>2ms/sec<br/>0s/call<br/>0.11% |81,479<br/>19.12/sec<br/>7.09/call<br/>1.76% |114,456&nbsp;blks<br/>26.85&nbsp;blks/sec<br/>9.96&nbsp;blks/call<br/>0.01% |19,614&nbsp;blks<br/>4.60&nbsp;blks/sec<br/>1.71&nbsp;blks/call<br/>0.10% |7&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |7,874.91&nbsp;ms<br/>1ms/sec<br/>0s/call<br/>0.16% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_PicturePost"&nbsp;"t"&nbsp;WHERE&nbsp;(("boardId"=?)&nbsp;AND&nbsp;(t.image_original&nbsp;is&nbsp;not&nbsp;null&nbsp;OR&nbsp;t."pictureId"&nbsp;is&nbsp;not&nbsp;null))&nbsp;AND&nbsp;(t.id&nbsp;not&nbsp;in&nbsp;(?))&nbsp;ORDER&nbsp;BY&nbsp;t.id&nbsp;DESC&nbsp;LIMIT&nbsp;?
39 |3,847<br/>0.90/sec<br/>1.00/call<br/>0.19% |6,851.67&nbsp;ms<br/>1ms/sec<br/>1ms/call<br/>0.08% |11,541<br/>2.71/sec<br/>3.00/call<br/>0.25% |36,954&nbsp;blks<br/>8.67&nbsp;blks/sec<br/>9.61&nbsp;blks/call<br/>0.00% |9,225&nbsp;blks<br/>2.16&nbsp;blks/sec<br/>2.40&nbsp;blks/call<br/>0.05% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.08% |6,705.71&nbsp;ms<br/>1ms/sec<br/>1ms/call<br/>0.14% |0.02&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.08% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_Picture"&nbsp;"t"&nbsp;WHERE&nbsp;"id"&nbsp;in&nbsp;(?,?,?)
40 |4,739<br/>1.11/sec<br/>1.00/call<br/>0.24% |6,486.82&nbsp;ms<br/>1ms/sec<br/>1ms/call<br/>0.08% |37,912<br/>8.89/sec<br/>8.00/call<br/>0.82% |142,832&nbsp;blks<br/>33.51&nbsp;blks/sec<br/>30.14&nbsp;blks/call<br/>0.01% |10,326&nbsp;blks<br/>2.42&nbsp;blks/sec<br/>2.18&nbsp;blks/call<br/>0.05% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |6,176.07&nbsp;ms<br/>1ms/sec<br/>1ms/call<br/>0.12% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_Picture"&nbsp;"t"&nbsp;WHERE&nbsp;"id"&nbsp;in&nbsp;(?,?,?,?,?,?,?,?)
41 |9,493<br/>2.23/sec<br/>1.00/call<br/>0.48% |6,453.91&nbsp;ms<br/>1ms/sec<br/>0s/call<br/>0.08% |56,958<br/>13.36/sec<br/>6.00/call<br/>1.23% |221,186&nbsp;blks<br/>51.89&nbsp;blks/sec<br/>23.30&nbsp;blks/call<br/>0.01% |9,415&nbsp;blks<br/>2.21&nbsp;blks/sec<br/>0.99&nbsp;blks/call<br/>0.05% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.08% |5,959.70&nbsp;ms<br/>1ms/sec<br/>0s/call<br/>0.12% |0.02&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.10% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_Picture"&nbsp;"t"&nbsp;WHERE&nbsp;"id"&nbsp;in&nbsp;(?,?,?,?,?,?)
42 |6,935<br/>1.63/sec<br/>1.00/call<br/>0.35% |6,357.73&nbsp;ms<br/>1ms/sec<br/>0s/call<br/>0.08% |34,675<br/>8.14/sec<br/>5.00/call<br/>0.75% |130,380&nbsp;blks<br/>30.59&nbsp;blks/sec<br/>18.80&nbsp;blks/call<br/>0.01% |9,626&nbsp;blks<br/>2.26&nbsp;blks/sec<br/>1.39&nbsp;blks/call<br/>0.05% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |6,021.49&nbsp;ms<br/>1ms/sec<br/>0s/call<br/>0.12% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_Picture"&nbsp;"t"&nbsp;WHERE&nbsp;"id"&nbsp;in&nbsp;(?,?,?,?,?)
43 |17,078<br/>4.01/sec<br/>1.00/call<br/>0.86% |6,283.54&nbsp;ms<br/>1ms/sec<br/>0s/call<br/>0.08% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |47,871&nbsp;blks<br/>11.23&nbsp;blks/sec<br/>2.80&nbsp;blks/call<br/>0.00% |3,411&nbsp;blks<br/>0.80&nbsp;blks/sec<br/>0.20&nbsp;blks/call<br/>0.02% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |5,971.43&nbsp;ms<br/>1ms/sec<br/>0s/call<br/>0.12% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;votingapi_vote.*FROM&nbsp;votingapi_vote&nbsp;votingapi_voteWHERE&nbsp;(entity_type&nbsp;=&nbsp;?)&nbsp;AND&nbsp;(entity_id&nbsp;=&nbsp;?)&nbsp;AND&nbsp;(tag&nbsp;=&nbsp;?)&nbsp;AND&nbsp;(value_type&nbsp;=&nbsp;?)&nbsp;AND&nbsp;(uid&nbsp;=&nbsp;?)&nbsp;AND&nbsp;(vote_source&nbsp;=&nbsp;?)&nbsp;AND&nbsp;(timestamp&nbsp;=&nbsp;?)&nbsp;
44 |4,283<br/>1.00/sec<br/>1.00/call<br/>0.22% |5,972.76&nbsp;ms<br/>1ms/sec<br/>1ms/call<br/>0.07% |17,132<br/>4.02/sec<br/>4.00/call<br/>0.37% |59,801&nbsp;blks<br/>14.03&nbsp;blks/sec<br/>13.96&nbsp;blks/call<br/>0.00% |8,980&nbsp;blks<br/>2.11&nbsp;blks/sec<br/>2.10&nbsp;blks/call<br/>0.05% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |1&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.04% |5,784.43&nbsp;ms<br/>1ms/sec<br/>1ms/call<br/>0.12% |0.01&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.04% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_Picture"&nbsp;"t"&nbsp;WHERE&nbsp;"id"&nbsp;in&nbsp;(?,?,?,?)
45 |18,069<br/>4.24/sec<br/>1.00/call<br/>0.91% |5,867.07&nbsp;ms<br/>1ms/sec<br/>0s/call<br/>0.07% |18,069<br/>4.24/sec<br/>1.00/call<br/>0.39% |1,419,222&nbsp;blks<br/>332.97&nbsp;blks/sec<br/>78.54&nbsp;blks/call<br/>0.07% |8,105&nbsp;blks<br/>1.90&nbsp;blks/sec<br/>0.45&nbsp;blks/call<br/>0.04% |6,990&nbsp;blks<br/>1.64&nbsp;blks/sec<br/>0.39&nbsp;blks/call<br/>2.36% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.08% |918.28&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.02% |0.15&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.60% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;incstat&nbsp;FROM&nbsp;public.incstat(?,&nbsp;?,&nbsp;?,&nbsp;?);
46 |18,069<br/>4.24/sec<br/>1.00/call<br/>0.91% |5,347.42&nbsp;ms<br/>1ms/sec<br/>0s/call<br/>0.06% |18,069<br/>4.24/sec<br/>1.00/call<br/>0.39% |1,418,858&nbsp;blks<br/>332.89&nbsp;blks/sec<br/>78.52&nbsp;blks/call<br/>0.07% |8,073&nbsp;blks<br/>1.89&nbsp;blks/sec<br/>0.45&nbsp;blks/call<br/>0.04% |6,990&nbsp;blks<br/>1.64&nbsp;blks/sec<br/>0.39&nbsp;blks/call<br/>2.36% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.08% |918.03&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.02% |0.15&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.60% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;public.incstat(i_site_id::int,&nbsp;i_key::text,&nbsp;i_segment::text,&nbsp;i_inc::bigint,&nbsp;NOW()::date)
47 |5,490<br/>1.29/sec<br/>1.00/call<br/>0.28% |5,252.08&nbsp;ms<br/>1ms/sec<br/>0s/call<br/>0.06% |45,424<br/>10.66/sec<br/>8.27/call<br/>0.98% |409,741&nbsp;blks<br/>96.13&nbsp;blks/sec<br/>74.63&nbsp;blks/call<br/>0.02% |29,151&nbsp;blks<br/>6.84&nbsp;blks/sec<br/>5.31&nbsp;blks/call<br/>0.15% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.08% |4,607.72&nbsp;ms<br/>1ms/sec<br/>0s/call<br/>0.09% |0.03&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.12% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"pictureId"FROM&nbsp;"pnct_AnyPost"WHERE&nbsp;("id"&nbsp;IN&nbsp;(?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?,&nbsp;?))&nbsp;AND&nbsp;("pictureId"&nbsp;IS&nbsp;NOT&nbsp;NULL)
48 |8,859<br/>2.08/sec<br/>1.00/call<br/>0.45% |4,973.74&nbsp;ms<br/>1ms/sec<br/>0s/call<br/>0.06% |62,013<br/>14.55/sec<br/>7.00/call<br/>1.34% |242,952&nbsp;blks<br/>57.00&nbsp;blks/sec<br/>27.42&nbsp;blks/call<br/>0.01% |8,328&nbsp;blks<br/>1.95&nbsp;blks/sec<br/>0.94&nbsp;blks/call<br/>0.04% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |4,468.06&nbsp;ms<br/>1ms/sec<br/>0s/call<br/>0.09% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_Picture"&nbsp;"t"&nbsp;WHERE&nbsp;"id"&nbsp;in&nbsp;(?,?,?,?,?,?,?)
49 |14,404<br/>3.38/sec<br/>1.00/call<br/>0.72% |4,911.71&nbsp;ms<br/>1ms/sec<br/>0s/call<br/>0.06% |15,189<br/>3.56/sec<br/>1.05/call<br/>0.33% |51,713&nbsp;blks<br/>12.13&nbsp;blks/sec<br/>3.59&nbsp;blks/call<br/>0.00% |24,025&nbsp;blks<br/>5.64&nbsp;blks/sec<br/>1.67&nbsp;blks/call<br/>0.13% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |4,533.87&nbsp;ms<br/>1ms/sec<br/>0s/call<br/>0.09% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_PictureLike"&nbsp;"t"&nbsp;WHERE&nbsp;t."postId"&nbsp;=&nbsp;?&nbsp;ORDER&nbsp;BY&nbsp;t.id&nbsp;DESC&nbsp;LIMIT&nbsp;?
50 |34,482<br/>8.09/sec<br/>1.00/call<br/>1.73% |4,179.13&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.05% |34,179<br/>8.02/sec<br/>0.99/call<br/>0.74% |263,525&nbsp;blks<br/>61.83&nbsp;blks/sec<br/>7.64&nbsp;blks/call<br/>0.01% |12,565&nbsp;blks<br/>2.95&nbsp;blks/sec<br/>0.36&nbsp;blks/call<br/>0.07% |1&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.08% |2,379.47&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.05% |0.03&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.11% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_AnyPost"&nbsp;"t"&nbsp;WHERE&nbsp;"t"."id"=?&nbsp;LIMIT&nbsp;?




### Replica servers: ###

#### Replica (`db3`) ####

Start: 2019-02-12T13:26:52.771225+00:00  
End: 2019-02-12T14:38:02.052634+00:00  
Period seconds: 4269.28141  
Period age: 01:11:09.281409  

\# | Calls | &#9660;&nbsp;Total&nbsp;time | Rows | shared_blks_hit | shared_blks_read | shared_blks_dirtied | shared_blks_written | blk_read_time | blk_write_time | kcache_reads | kcache_writes | kcache_user_time_ms | kcache_system_time | Query
----|-------|------------|------|-----------------|------------------|---------------------|---------------------|---------------|----------------|--------------|---------------|---------------------|--------------------|------- 
1 |31,260<br/>7.32/sec<br/>1.00/call<br/>0.69% |7,195,173.85&nbsp;ms<br/>1.685s/sec<br/>230ms/call<br/>47.92% |746,884<br/>174.94/sec<br/>23.89/call<br/>5.42% |4,288,425,244&nbsp;blks<br/>1.01M&nbsp;blks/sec<br/>137.19K&nbsp;blks/call<br/>51.24% |218,972,431&nbsp;blks<br/>51.30K&nbsp;blks/sec<br/>7.01K&nbsp;blks/call<br/>61.46% |51&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>1.91% |9,075&nbsp;blks<br/>2.13&nbsp;blks/sec<br/>0.29&nbsp;blks/call<br/>71.03% |2,176,260.72&nbsp;ms<br/>509ms/sec<br/>69ms/call<br/>40.22% |82.21&nbsp;ms<br/>0s/sec<br/>0s/call<br/>69.44% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
2 |26,300<br/>6.16/sec<br/>1.00/call<br/>0.58% |1,658,360.59&nbsp;ms<br/>388ms/sec<br/>63ms/call<br/>11.04% |203,892<br/>47.76/sec<br/>7.75/call<br/>1.48% |1,025,929,460&nbsp;blks<br/>240.31K&nbsp;blks/sec<br/>39.01K&nbsp;blks/call<br/>12.26% |39,505,665&nbsp;blks<br/>9.26K&nbsp;blks/sec<br/>1.51K&nbsp;blks/call<br/>11.09% |30&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>1.12% |1,564&nbsp;blks<br/>0.37&nbsp;blks/sec<br/>0.06&nbsp;blks/call<br/>12.24% |420,777.76&nbsp;ms<br/>98ms/sec<br/>15ms/call<br/>7.78% |13.71&nbsp;ms<br/>0s/sec<br/>0s/call<br/>11.58% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
3 |30,617<br/>7.17/sec<br/>1.00/call<br/>0.68% |1,336,761.32&nbsp;ms<br/>313ms/sec<br/>43ms/call<br/>8.90% |178,656<br/>41.85/sec<br/>5.84/call<br/>1.30% |837,343,901&nbsp;blks<br/>196.14K&nbsp;blks/sec<br/>27.35K&nbsp;blks/call<br/>10.01% |22,911,590&nbsp;blks<br/>5.37K&nbsp;blks/sec<br/>748.33&nbsp;blks/call<br/>6.43% |3&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.11% |395&nbsp;blks<br/>0.09&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>3.09% |321,290.39&nbsp;ms<br/>75ms/sec<br/>10ms/call<br/>5.94% |4.35&nbsp;ms<br/>0s/sec<br/>0s/call<br/>3.68% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
4 |618<br/>0.14/sec<br/>1.00/call<br/>0.01% |488,906.88&nbsp;ms<br/>114ms/sec<br/>791ms/call<br/>3.26% |12,813<br/>3.00/sec<br/>20.73/call<br/>0.09% |196,192,128&nbsp;blks<br/>45.96K&nbsp;blks/sec<br/>317.47K&nbsp;blks/call<br/>2.34% |19,860,664&nbsp;blks<br/>4.66K&nbsp;blks/sec<br/>32.14K&nbsp;blks/call<br/>5.57% |4&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>0.15% |313&nbsp;blks<br/>0.07&nbsp;blks/sec<br/>0.51&nbsp;blks/call<br/>2.45% |153,131.04&nbsp;ms<br/>35ms/sec<br/>247ms/call<br/>2.83% |2.57&nbsp;ms<br/>0s/sec<br/>0s/call<br/>2.17% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
5 |4,912<br/>1.15/sec<br/>1.00/call<br/>0.11% |463,338.86&nbsp;ms<br/>108ms/sec<br/>94ms/call<br/>3.09% |114,390<br/>26.79/sec<br/>23.29/call<br/>0.83% |220,651,572&nbsp;blks<br/>51.69K&nbsp;blks/sec<br/>44.93K&nbsp;blks/call<br/>2.64% |7,454,518&nbsp;blks<br/>1.75K&nbsp;blks/sec<br/>1.52K&nbsp;blks/call<br/>2.09% |7&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.26% |270&nbsp;blks<br/>0.06&nbsp;blks/sec<br/>0.05&nbsp;blks/call<br/>2.11% |107,608.99&nbsp;ms<br/>25ms/sec<br/>21ms/call<br/>1.99% |2.94&nbsp;ms<br/>0s/sec<br/>0s/call<br/>2.48% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
6 |26,705<br/>6.26/sec<br/>1.00/call<br/>0.59% |419,238.43&nbsp;ms<br/>98ms/sec<br/>15ms/call<br/>2.79% |213,632<br/>50.04/sec<br/>8.00/call<br/>1.55% |554,785,368&nbsp;blks<br/>129.95K&nbsp;blks/sec<br/>20.78K&nbsp;blks/call<br/>6.63% |4,829,944&nbsp;blks<br/>1.14K&nbsp;blks/sec<br/>180.86&nbsp;blks/call<br/>1.36% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.07% |188&nbsp;blks<br/>0.04&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>1.47% |35,550.42&nbsp;ms<br/>8ms/sec<br/>1ms/call<br/>0.66% |2.38&nbsp;ms<br/>0s/sec<br/>0s/call<br/>2.01% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
7 |439<br/>0.10/sec<br/>1.00/call<br/>0.01% |397,153.72&nbsp;ms<br/>93ms/sec<br/>904ms/call<br/>2.64% |11,123<br/>2.61/sec<br/>25.34/call<br/>0.08% |86,859,499&nbsp;blks<br/>20.35K&nbsp;blks/sec<br/>197.86K&nbsp;blks/call<br/>1.04% |13,462,486&nbsp;blks<br/>3.16K&nbsp;blks/sec<br/>30.67K&nbsp;blks/call<br/>3.78% |3&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>0.11% |225&nbsp;blks<br/>0.05&nbsp;blks/sec<br/>0.51&nbsp;blks/call<br/>1.76% |286,258.51&nbsp;ms<br/>67ms/sec<br/>652ms/call<br/>5.29% |1.96&nbsp;ms<br/>0s/sec<br/>0s/call<br/>1.65% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_PicturePost"&nbsp;"t"&nbsp;WHERE&nbsp;((id&nbsp;in&nbsp;(select&nbsp;"postId"&nbsp;from&nbsp;"pnct_PictureLike"&nbsp;where&nbsp;"userId"&nbsp;=&nbsp;?))&nbsp;AND&nbsp;((select&nbsp;max(created)&nbsp;from&nbsp;"pnct_PictureLike"&nbsp;where&nbsp;"userId"&nbsp;=&nbsp;?&nbsp;and&nbsp;"postId"&nbsp;=&nbsp;t.id)&nbsp;<&nbsp;(select&nbsp;max(created)&nbsp;from&nbsp;"pnct_PictureLike"&nbsp;where&nbsp;"userId"&nbsp;=&nbsp;?&nbsp;and&nbsp;"postId"&nbsp;=&nbsp;?)))&nbsp;AND&nbsp;(t.image_original&nbsp;is&nbsp;not&nbsp;null&nbsp;OR&nbsp;t."pictureId"&nbsp;is&nbsp;not&nbsp;null)&nbsp;ORDER&nbsp;BY&nbsp;(select&nbsp;max(created)&nbsp;from&nbsp;"pnct_PictureLike"&nbsp;where&nbsp;"userId"&nbsp;=&nbsp;?&nbsp;and&nbsp;"postId"&nbsp;=&nbsp;t.id)&nbsp;DESC&nbsp;LIMIT&nbsp;?
8 |30,617<br/>7.17/sec<br/>1.00/call<br/>0.68% |365,693.96&nbsp;ms<br/>85ms/sec<br/>11ms/call<br/>2.44% |183,792<br/>43.05/sec<br/>6.00/call<br/>1.33% |576,876,715&nbsp;blks<br/>135.13K&nbsp;blks/sec<br/>18.85K&nbsp;blks/call<br/>6.89% |875,052&nbsp;blks<br/>204.96&nbsp;blks/sec<br/>28.58&nbsp;blks/call<br/>0.25% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.07% |11&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.09% |5,279.04&nbsp;ms<br/>1ms/sec<br/>0s/call<br/>0.10% |0.12&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.10% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
9 |189<br/>0.04/sec<br/>1.00/call<br/>0.00% |261,946.25&nbsp;ms<br/>61ms/sec<br/>1.385s/call<br/>1.74% |1,818<br/>0.43/sec<br/>9.62/call<br/>0.01% |33,538,734&nbsp;blks<br/>7.86K&nbsp;blks/sec<br/>177.46K&nbsp;blks/call<br/>0.40% |3,486,834&nbsp;blks<br/>816.73&nbsp;blks/sec<br/>18.45K&nbsp;blks/call<br/>0.98% |37&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.20&nbsp;blks/call<br/>1.39% |194&nbsp;blks<br/>0.05&nbsp;blks/sec<br/>1.03&nbsp;blks/call<br/>1.52% |229,220.69&nbsp;ms<br/>53ms/sec<br/>1.212s/call<br/>4.24% |1.88&nbsp;ms<br/>0s/sec<br/>0s/call<br/>1.59% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_NetworkFollowBoard"&nbsp;"t"&nbsp;WHERE&nbsp;"boardId"=?&nbsp;ORDER&nbsp;BY&nbsp;id&nbsp;DESC&nbsp;LIMIT&nbsp;?&nbsp;OFFSET&nbsp;?
10 |40,256<br/>9.43/sec<br/>1.00/call<br/>0.89% |218,819.05&nbsp;ms<br/>51ms/sec<br/>5ms/call<br/>1.46% |72,890<br/>17.07/sec<br/>1.81/call<br/>0.53% |104,421&nbsp;blks<br/>24.46&nbsp;blks/sec<br/>2.59&nbsp;blks/call<br/>0.00% |98,807&nbsp;blks<br/>23.14&nbsp;blks/sec<br/>2.45&nbsp;blks/call<br/>0.03% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.02% |217,982.91&nbsp;ms<br/>51ms/sec<br/>5ms/call<br/>4.03% |0.07&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.06% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"fragments"."picture_id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"fragments"."index"&nbsp;AS&nbsp;"t1_c1",&nbsp;"fragments"."top"&nbsp;AS&nbsp;"t1_c2",&nbsp;"fragments"."bottom"&nbsp;AS&nbsp;"t1_c3",&nbsp;"fragments"."caption"&nbsp;AS&nbsp;"t1_c4"&nbsp;FROM&nbsp;"pnct_PictureFragment"&nbsp;"fragments"&nbsp;WHERE&nbsp;("fragments"."picture_id"=?)
11 |2,332<br/>0.55/sec<br/>1.00/call<br/>0.05% |200,769.18&nbsp;ms<br/>47ms/sec<br/>86ms/call<br/>1.34% |2,332<br/>0.55/sec<br/>1.00/call<br/>0.02% |13,041&nbsp;blks<br/>3.05&nbsp;blks/sec<br/>5.59&nbsp;blks/call<br/>0.00% |99,325&nbsp;blks<br/>23.27&nbsp;blks/sec<br/>42.59&nbsp;blks/call<br/>0.03% |951&nbsp;blks<br/>0.22&nbsp;blks/sec<br/>0.41&nbsp;blks/call<br/>35.62% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |200,384.20&nbsp;ms<br/>46ms/sec<br/>85ms/call<br/>3.70% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |select&nbsp;sum(cnt)&nbsp;as&nbsp;cnt&nbsp;from&nbsp;post_view&nbsp;where&nbsp;post_id&nbsp;=&nbsp;?
12 |347,414<br/>81.38/sec<br/>1.00/call<br/>7.69% |162,558.12&nbsp;ms<br/>38ms/sec<br/>0s/call<br/>1.08% |671,988<br/>157.40/sec<br/>1.93/call<br/>4.88% |2,552,861&nbsp;blks<br/>597.96&nbsp;blks/sec<br/>7.35&nbsp;blks/call<br/>0.03% |726,211&nbsp;blks<br/>170.10&nbsp;blks/sec<br/>2.09&nbsp;blks/call<br/>0.20% |6&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.22% |17&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.13% |152,279.04&nbsp;ms<br/>35ms/sec<br/>0s/call<br/>2.81% |0.22&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.18% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"avatarBin"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"avatarBin"."owner"&nbsp;AS&nbsp;"t1_c1",&nbsp;"avatarBin"."status"&nbsp;AS&nbsp;"t1_c2",&nbsp;"avatarBin"."created"&nbsp;AS&nbsp;"t1_c3",&nbsp;"files"."id"&nbsp;AS&nbsp;"t2_c0",&nbsp;"files"."binId"&nbsp;AS&nbsp;"t2_c1",&nbsp;"files"."name"&nbsp;AS&nbsp;"t2_c2",&nbsp;"files"."hash"&nbsp;AS&nbsp;"t2_c3",&nbsp;"files"."extension"&nbsp;AS&nbsp;"t2_c4",&nbsp;"files"."created"&nbsp;AS&nbsp;"t2_c5",&nbsp;"files"."size"&nbsp;AS&nbsp;"t2_c6",&nbsp;"files"."width"&nbsp;AS&nbsp;"t2_c7",&nbsp;"files"."height"&nbsp;AS&nbsp;"t2_c8",&nbsp;"files"."md5"&nbsp;AS&nbsp;"t2_c9"&nbsp;FROM&nbsp;"pnct_StorageBin"&nbsp;"avatarBin"&nbsp;LEFT&nbsp;OUTER&nbsp;JOIN&nbsp;"pnct_StorageFile"&nbsp;"files"&nbsp;ON&nbsp;("files"."binId"="avatarBin"."id")&nbsp;WHERE&nbsp;("avatarBin"."id"=?)
13 |1,612<br/>0.38/sec<br/>1.00/call<br/>0.04% |147,750.97&nbsp;ms<br/>34ms/sec<br/>91ms/call<br/>0.98% |12,428<br/>2.91/sec<br/>7.71/call<br/>0.09% |77,542,840&nbsp;blks<br/>18.17K&nbsp;blks/sec<br/>48.11K&nbsp;blks/call<br/>0.93% |3,765,611&nbsp;blks<br/>882.02&nbsp;blks/sec<br/>2.34K&nbsp;blks/call<br/>1.06% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.07% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.02% |49,054.25&nbsp;ms<br/>11ms/sec<br/>30ms/call<br/>0.91% |0.06&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.05% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
14 |3,123<br/>0.73/sec<br/>1.00/call<br/>0.07% |128,954.29&nbsp;ms<br/>30ms/sec<br/>41ms/call<br/>0.86% |63,694<br/>14.92/sec<br/>20.40/call<br/>0.46% |79,301,383&nbsp;blks<br/>18.58K&nbsp;blks/sec<br/>25.40K&nbsp;blks/call<br/>0.95% |778,974&nbsp;blks<br/>182.46&nbsp;blks/sec<br/>249.43&nbsp;blks/call<br/>0.22% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |10,707.70&nbsp;ms<br/>2ms/sec<br/>3ms/call<br/>0.20% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
15 |2,204<br/>0.52/sec<br/>1.00/call<br/>0.05% |106,844.35&nbsp;ms<br/>25ms/sec<br/>48ms/call<br/>0.71% |17,043<br/>3.99/sec<br/>7.73/call<br/>0.12% |84,047,512&nbsp;blks<br/>19.69K&nbsp;blks/sec<br/>38.14K&nbsp;blks/call<br/>1.00% |1,172,565&nbsp;blks<br/>274.65&nbsp;blks/sec<br/>532.02&nbsp;blks/call<br/>0.33% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |28&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>0.22% |17,125.79&nbsp;ms<br/>4ms/sec<br/>7ms/call<br/>0.32% |0.36&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.30% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
16 |1,150<br/>0.27/sec<br/>1.00/call<br/>0.03% |98,845.14&nbsp;ms<br/>23ms/sec<br/>85ms/call<br/>0.66% |8,911<br/>2.09/sec<br/>7.75/call<br/>0.06% |48,193,167&nbsp;blks<br/>11.29K&nbsp;blks/sec<br/>41.91K&nbsp;blks/call<br/>0.58% |751,762&nbsp;blks<br/>176.09&nbsp;blks/sec<br/>653.71&nbsp;blks/call<br/>0.21% |1&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.04% |18&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.02&nbsp;blks/call<br/>0.14% |17,031.55&nbsp;ms<br/>3ms/sec<br/>14ms/call<br/>0.31% |0.19&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.16% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
17 |732<br/>0.17/sec<br/>1.00/call<br/>0.02% |97,819.79&nbsp;ms<br/>22ms/sec<br/>133ms/call<br/>0.65% |18,687<br/>4.38/sec<br/>25.53/call<br/>0.14% |22,900,266&nbsp;blks<br/>5.37K&nbsp;blks/sec<br/>31.29K&nbsp;blks/call<br/>0.27% |3,012,358&nbsp;blks<br/>705.59&nbsp;blks/sec<br/>4.12K&nbsp;blks/call<br/>0.85% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.07% |163&nbsp;blks<br/>0.04&nbsp;blks/sec<br/>0.22&nbsp;blks/call<br/>1.28% |64,852.34&nbsp;ms<br/>15ms/sec<br/>88ms/call<br/>1.20% |1.56&nbsp;ms<br/>0s/sec<br/>0s/call<br/>1.32% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
18 |608<br/>0.14/sec<br/>1.00/call<br/>0.01% |88,067.63&nbsp;ms<br/>20ms/sec<br/>144ms/call<br/>0.59% |14,808<br/>3.47/sec<br/>24.36/call<br/>0.11% |15,377,755&nbsp;blks<br/>3.61K&nbsp;blks/sec<br/>25.30K&nbsp;blks/call<br/>0.18% |4,544,384&nbsp;blks<br/>1.07K&nbsp;blks/sec<br/>7.48K&nbsp;blks/call<br/>1.28% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |7&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>0.05% |60,391.46&nbsp;ms<br/>14ms/sec<br/>99ms/call<br/>1.12% |0.10&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.09% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
19 |158,929<br/>37.23/sec<br/>1.00/call<br/>3.52% |84,830.49&nbsp;ms<br/>19ms/sec<br/>0s/call<br/>0.56% |84,094<br/>19.70/sec<br/>0.53/call<br/>0.61% |532,227&nbsp;blks<br/>124.66&nbsp;blks/sec<br/>3.35&nbsp;blks/call<br/>0.01% |221,571&nbsp;blks<br/>51.90&nbsp;blks/sec<br/>1.39&nbsp;blks/call<br/>0.06% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |9&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.07% |81,275.47&nbsp;ms<br/>19ms/sec<br/>0s/call<br/>1.50% |0.16&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.14% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_PicturePost"&nbsp;"t"&nbsp;WHERE&nbsp;t."parentId"&nbsp;=&nbsp;?&nbsp;ORDER&nbsp;BY&nbsp;t.id&nbsp;DESC&nbsp;LIMIT&nbsp;?
20 |1,175<br/>0.28/sec<br/>1.00/call<br/>0.03% |73,410.21&nbsp;ms<br/>17ms/sec<br/>62ms/call<br/>0.49% |19,418<br/>4.55/sec<br/>16.53/call<br/>0.14% |63,375,396&nbsp;blks<br/>14.85K&nbsp;blks/sec<br/>53.94K&nbsp;blks/call<br/>0.76% |671,851&nbsp;blks<br/>157.37&nbsp;blks/sec<br/>571.79&nbsp;blks/call<br/>0.19% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |9,297.29&nbsp;ms<br/>2ms/sec<br/>7ms/call<br/>0.17% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
21 |130,713<br/>30.62/sec<br/>1.00/call<br/>2.89% |69,295.27&nbsp;ms<br/>16ms/sec<br/>0s/call<br/>0.46% |459,651<br/>107.66/sec<br/>3.52/call<br/>3.33% |502,460&nbsp;blks<br/>117.69&nbsp;blks/sec<br/>3.84&nbsp;blks/call<br/>0.01% |234,379&nbsp;blks<br/>54.90&nbsp;blks/sec<br/>1.79&nbsp;blks/call<br/>0.07% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |15&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.12% |66,354.43&nbsp;ms<br/>15ms/sec<br/>0s/call<br/>1.23% |0.27&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.23% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"files"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"files"."binId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"files"."name"&nbsp;AS&nbsp;"t1_c2",&nbsp;"files"."hash"&nbsp;AS&nbsp;"t1_c3",&nbsp;"files"."extension"&nbsp;AS&nbsp;"t1_c4",&nbsp;"files"."created"&nbsp;AS&nbsp;"t1_c5",&nbsp;"files"."size"&nbsp;AS&nbsp;"t1_c6",&nbsp;"files"."width"&nbsp;AS&nbsp;"t1_c7",&nbsp;"files"."height"&nbsp;AS&nbsp;"t1_c8",&nbsp;"files"."md5"&nbsp;AS&nbsp;"t1_c9"&nbsp;FROM&nbsp;"pnct_StorageFile"&nbsp;"files"&nbsp;WHERE&nbsp;("files"."binId"=?)
22 |158,926<br/>37.23/sec<br/>1.00/call<br/>3.52% |66,041.87&nbsp;ms<br/>15ms/sec<br/>0s/call<br/>0.44% |605,818<br/>141.90/sec<br/>3.81/call<br/>4.40% |386,133&nbsp;blks<br/>90.44&nbsp;blks/sec<br/>2.43&nbsp;blks/call<br/>0.00% |321,499&nbsp;blks<br/>75.31&nbsp;blks/sec<br/>2.02&nbsp;blks/call<br/>0.09% |1,153&nbsp;blks<br/>0.27&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>43.18% |14&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.11% |62,674.45&nbsp;ms<br/>14ms/sec<br/>0s/call<br/>1.16% |0.23&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.20% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;begin_time,&nbsp;value&nbsp;from&nbsp;post_subtitles&nbsp;where&nbsp;post_id&nbsp;=&nbsp;?&nbsp;and&nbsp;language&nbsp;=&nbsp;?&nbsp;order&nbsp;by&nbsp;begin_time,&nbsp;id;
23 |7,351<br/>1.72/sec<br/>1.00/call<br/>0.16% |64,698.98&nbsp;ms<br/>15ms/sec<br/>8ms/call<br/>0.43% |140,892<br/>33.00/sec<br/>19.17/call<br/>1.02% |12,052&nbsp;blks<br/>2.82&nbsp;blks/sec<br/>1.64&nbsp;blks/call<br/>0.00% |121,860&nbsp;blks<br/>28.54&nbsp;blks/sec<br/>16.58&nbsp;blks/call<br/>0.03% |8&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.30% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.02% |64,212.90&nbsp;ms<br/>15ms/sec<br/>8ms/call<br/>1.19% |0.02&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.02% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_NetworkFollowUser"&nbsp;"t"&nbsp;WHERE&nbsp;"userId"=&nbsp;?
24 |1,556<br/>0.36/sec<br/>1.00/call<br/>0.03% |61,827.80&nbsp;ms<br/>14ms/sec<br/>39ms/call<br/>0.41% |7,035,602<br/>1.65K/sec<br/>4.53K/call<br/>51.04% |4,157,867&nbsp;blks<br/>973.90&nbsp;blks/sec<br/>2.68K&nbsp;blks/call<br/>0.05% |2,558,734&nbsp;blks<br/>599.34&nbsp;blks/sec<br/>1.65K&nbsp;blks/call<br/>0.72% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |59&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.04&nbsp;blks/call<br/>0.46% |47,080.70&nbsp;ms<br/>11ms/sec<br/>30ms/call<br/>0.87% |0.68&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.58% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |select&nbsp;&nbsp;message&nbsp;as&nbsp;target_title,&nbsp;&nbsp;?&nbsp;||&nbsp;p.id::text&nbsp;as&nbsp;target_urlfrom&nbsp;"pnct_PicturePost"&nbsp;pwhere&nbsp;id&nbsp;between&nbsp;?&nbsp;and&nbsp;?&nbsp;and&nbsp;created&nbsp;<&nbsp;date_trunc(?,&nbsp;now())limit&nbsp;?;&nbsp;/*&nbsp;this&nbsp;is&nbsp;only&nbsp;for&nbsp;safety&nbsp;*/
25 |1,890,281<br/>442.76/sec<br/>1.00/call<br/>41.81% |60,359.37&nbsp;ms<br/>14ms/sec<br/>0s/call<br/>0.40% |1,890,280<br/>442.76/sec<br/>1.00/call<br/>13.71% |13,766,650&nbsp;blks<br/>3.23K&nbsp;blks/sec<br/>7.28&nbsp;blks/call<br/>0.16% |37,615&nbsp;blks<br/>8.81&nbsp;blks/sec<br/>0.02&nbsp;blks/call<br/>0.01% |21&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.79% |1&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.01% |1,699.45&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.03% |0.01&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.01% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"user"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"user"."email"&nbsp;AS&nbsp;"t1_c1",&nbsp;"user"."password"&nbsp;AS&nbsp;"t1_c2",&nbsp;"user"."salt"&nbsp;AS&nbsp;"t1_c3",&nbsp;"user"."changePassword"&nbsp;AS&nbsp;"t1_c4",&nbsp;"user"."role"&nbsp;AS&nbsp;"t1_c5",&nbsp;"user"."created"&nbsp;AS&nbsp;"t1_c6",&nbsp;"user"."firstName"&nbsp;AS&nbsp;"t1_c7",&nbsp;"user"."lastName"&nbsp;AS&nbsp;"t1_c8",&nbsp;"user"."username"&nbsp;AS&nbsp;"t1_c9",&nbsp;"user"."avatar"&nbsp;AS&nbsp;"t1_c10",&nbsp;"user"."timeZone"&nbsp;AS&nbsp;"t1_c11",&nbsp;"user"."ip"&nbsp;AS&nbsp;"t1_c12",&nbsp;"user"."language"&nbsp;AS&nbsp;"t1_c13",&nbsp;"user"."about"&nbsp;AS&nbsp;"t1_c14",&nbsp;"user"."birthday"&nbsp;AS&nbsp;"t1_c15",&nbsp;"user"."source"&nbsp;AS&nbsp;"t1_c16",&nbsp;"user"."newsletter"&nbsp;AS&nbsp;"t1_c17",&nbsp;"user"."is_male"&nbsp;AS&nbsp;"t1_c18",&nbsp;"user"."access"&nbsp;AS&nbsp;"t1_c19",&nbsp;"user"."inviter_email"&nbsp;AS&nbsp;"t1_c20",&nbsp;"user"."allow_u2b"&nbsp;AS&nbsp;"t1_c21",&nbsp;"user"."country"&nbsp;AS&nbsp;"t1_c22",&nbsp;"user"."city"&nbsp;AS&nbsp;"t1_c23",&nbsp;"user"."mailru_id"&nbsp;AS&nbsp;"t1_c24",&nbsp;"user"."facebook_id"&nbsp;AS&nbsp;"t1_c25",&nbsp;"user"."post_notify_mode"&nbsp;AS&nbsp;"t1_c26",&nbsp;"user"."friends_check_date"&nbsp;AS&nbsp;"t1_c27",&nbsp;"user"."is_editor"&nbsp;AS&nbsp;"t1_c28",&nbsp;"user"."main_page"&nbsp;AS&nbsp;"t1_c29",&nbsp;"user"."notified_about_search"&nbsp;AS&nbsp;"t1_c30",&nbsp;"user"."biz"&nbsp;AS&nbsp;"t1_c31",&nbsp;"user"."biz_about"&nbsp;AS&nbsp;"t1_c32",&nbsp;"user"."followers"&nbsp;AS&nbsp;"t1_c33",&nbsp;"user"."censor_trust_level"&nbsp;AS&nbsp;"t1_c34",&nbsp;"user"."odnoklassniki_id"&nbsp;AS&nbsp;"t1_c35",&nbsp;"user"."vk_id"&nbsp;AS&nbsp;"t1_c36",&nbsp;"user"."confirmed_18plus_policy"&nbsp;AS&nbsp;"t1_c37",&nbsp;"user"."misc"&nbsp;AS&nbsp;"t1_c38",&nbsp;"user"."img_src"&nbsp;AS&nbsp;"t1_c39",&nbsp;"user"."img_w"&nbsp;AS&nbsp;"t1_c40",&nbsp;"user"."img_h"&nbsp;AS&nbsp;"t1_c41",&nbsp;"user"."google_id"&nbsp;AS&nbsp;"t1_c42",&nbsp;"user"."invite_all_requested"&nbsp;AS&nbsp;"t1_c43",&nbsp;"user"."new_email"&nbsp;AS&nbsp;"t1_c44",&nbsp;"user"."forbid_invitations"&nbsp;AS&nbsp;"t1_c45"&nbsp;FROM&nbsp;"pnct_User"&nbsp;"user"&nbsp;WHERE&nbsp;("user"."id"=?)
26 |76,758<br/>17.98/sec<br/>1.00/call<br/>1.70% |58,477.00&nbsp;ms<br/>13ms/sec<br/>0s/call<br/>0.39% |64,785<br/>15.17/sec<br/>0.84/call<br/>0.47% |266,957&nbsp;blks<br/>62.53&nbsp;blks/sec<br/>3.48&nbsp;blks/call<br/>0.00% |107,196&nbsp;blks<br/>25.11&nbsp;blks/sec<br/>1.40&nbsp;blks/call<br/>0.03% |9&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.34% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.02% |56,873.27&nbsp;ms<br/>13ms/sec<br/>0s/call<br/>1.05% |0.02&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.02% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |select&nbsp;q,&nbsp;result&nbsp;from&nbsp;translation_proxy.cache&nbsp;where&nbsp;source&nbsp;=&nbsp;?&nbsp;and&nbsp;target&nbsp;=&nbsp;?&nbsp;and&nbsp;md5(q)&nbsp;in&nbsp;(md5(?))
27 |1,473<br/>0.35/sec<br/>1.00/call<br/>0.03% |51,628.98&nbsp;ms<br/>12ms/sec<br/>35ms/call<br/>0.34% |32,785<br/>7.68/sec<br/>22.26/call<br/>0.24% |117,979&nbsp;blks<br/>27.63&nbsp;blks/sec<br/>80.09&nbsp;blks/call<br/>0.00% |75,779&nbsp;blks<br/>17.75&nbsp;blks/sec<br/>51.45&nbsp;blks/call<br/>0.02% |4&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.15% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |50,974.97&nbsp;ms<br/>11ms/sec<br/>34ms/call<br/>0.94% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |select&nbsp;q,&nbsp;result&nbsp;from&nbsp;translation_proxy.cache&nbsp;where&nbsp;source&nbsp;=&nbsp;?&nbsp;and&nbsp;target&nbsp;=&nbsp;?&nbsp;and&nbsp;md5(q)&nbsp;in&nbsp;(md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?))
28 |6,754<br/>1.58/sec<br/>1.00/call<br/>0.15% |45,396.86&nbsp;ms<br/>10ms/sec<br/>6ms/call<br/>0.30% |36,992<br/>8.66/sec<br/>5.48/call<br/>0.27% |208,872&nbsp;blks<br/>48.92&nbsp;blks/sec<br/>30.93&nbsp;blks/call<br/>0.00% |83,943&nbsp;blks<br/>19.66&nbsp;blks/sec<br/>12.43&nbsp;blks/call<br/>0.02% |32&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>1.20% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.02% |44,644.94&nbsp;ms<br/>10ms/sec<br/>6ms/call<br/>0.83% |0.03&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.02% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |select&nbsp;q,&nbsp;result&nbsp;from&nbsp;translation_proxy.cache&nbsp;where&nbsp;source&nbsp;=&nbsp;?&nbsp;and&nbsp;target&nbsp;=&nbsp;?&nbsp;and&nbsp;md5(q)&nbsp;in&nbsp;(md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?))
29 |17,749<br/>4.16/sec<br/>1.00/call<br/>0.39% |37,171.25&nbsp;ms<br/>8ms/sec<br/>2ms/call<br/>0.25% |17,749<br/>4.16/sec<br/>1.00/call<br/>0.13% |885,406&nbsp;blks<br/>207.39&nbsp;blks/sec<br/>49.88&nbsp;blks/call<br/>0.01% |22,662&nbsp;blks<br/>5.31&nbsp;blks/sec<br/>1.28&nbsp;blks/call<br/>0.01% |32&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>1.20% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |11,215.11&nbsp;ms<br/>2ms/sec<br/>0s/call<br/>0.21% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;COUNT(?)&nbsp;FROM&nbsp;"pnct_network_follow"&nbsp;"t"&nbsp;WHERE&nbsp;user_id&nbsp;=&nbsp;?&nbsp;and&nbsp;follow_used&nbsp;is&nbsp;null&nbsp;and&nbsp;use_requested&nbsp;is&nbsp;null&nbsp;and&nbsp;created&nbsp;>&nbsp;?&nbsp;and&nbsp;provider&nbsp;in&nbsp;(?,&nbsp;?,&nbsp;?)
30 |2,259<br/>0.53/sec<br/>1.00/call<br/>0.05% |32,782.52&nbsp;ms<br/>7ms/sec<br/>14ms/call<br/>0.22% |18,072<br/>4.23/sec<br/>8.00/call<br/>0.13% |46,350,546&nbsp;blks<br/>10.86K&nbsp;blks/sec<br/>20.52K&nbsp;blks/call<br/>0.55% |167,507&nbsp;blks<br/>39.24&nbsp;blks/sec<br/>74.15&nbsp;blks/call<br/>0.05% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |1,913.68&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.04% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
31 |2,166<br/>0.51/sec<br/>1.00/call<br/>0.05% |31,833.48&nbsp;ms<br/>7ms/sec<br/>14ms/call<br/>0.21% |17,328<br/>4.06/sec<br/>8.00/call<br/>0.13% |43,664,816&nbsp;blks<br/>10.23K&nbsp;blks/sec<br/>20.16K&nbsp;blks/call<br/>0.52% |322,906&nbsp;blks<br/>75.63&nbsp;blks/sec<br/>149.08&nbsp;blks/call<br/>0.09% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |2,139.72&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.04% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
32 |9,852<br/>2.31/sec<br/>1.00/call<br/>0.22% |30,437.91&nbsp;ms<br/>7ms/sec<br/>3ms/call<br/>0.20% |99,613<br/>23.33/sec<br/>10.11/call<br/>0.72% |57,215&nbsp;blks<br/>13.40&nbsp;blks/sec<br/>5.81&nbsp;blks/call<br/>0.00% |76,423&nbsp;blks<br/>17.90&nbsp;blks/sec<br/>7.76&nbsp;blks/call<br/>0.02% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.07% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |29,995.96&nbsp;ms<br/>7ms/sec<br/>3ms/call<br/>0.55% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_NetworkEvent"&nbsp;"t"&nbsp;WHERE&nbsp;"userId"=?&nbsp;ORDER&nbsp;BY&nbsp;created&nbsp;DESC&nbsp;LIMIT&nbsp;?
33 |444<br/>0.10/sec<br/>1.00/call<br/>0.01% |29,675.17&nbsp;ms<br/>6ms/sec<br/>66ms/call<br/>0.20% |11,274<br/>2.64/sec<br/>25.39/call<br/>0.08% |9,695,362&nbsp;blks<br/>2.28K&nbsp;blks/sec<br/>21.84K&nbsp;blks/call<br/>0.12% |1,531,959&nbsp;blks<br/>358.83&nbsp;blks/sec<br/>3.46K&nbsp;blks/call<br/>0.43% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |132&nbsp;blks<br/>0.03&nbsp;blks/sec<br/>0.30&nbsp;blks/call<br/>1.03% |19,423.07&nbsp;ms<br/>4ms/sec<br/>43ms/call<br/>0.36% |1.30&nbsp;ms<br/>0s/sec<br/>0s/call<br/>1.10% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....
34 |1,223,834<br/>286.66/sec<br/>1.00/call<br/>27.07% |29,526.70&nbsp;ms<br/>6ms/sec<br/>0s/call<br/>0.20% |78,423<br/>18.37/sec<br/>0.06/call<br/>0.57% |3,628,586&nbsp;blks<br/>849.93&nbsp;blks/sec<br/>2.96&nbsp;blks/call<br/>0.04% |121,880&nbsp;blks<br/>28.55&nbsp;blks/sec<br/>0.10&nbsp;blks/call<br/>0.03% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |4&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.03% |20,699.81&nbsp;ms<br/>4ms/sec<br/>0s/call<br/>0.38% |0.11&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.09% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"picture"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"picture"."bin"&nbsp;AS&nbsp;"t1_c1",&nbsp;"picture"."userId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"picture"."height"&nbsp;AS&nbsp;"t1_c3",&nbsp;"picture"."created"&nbsp;AS&nbsp;"t1_c4"&nbsp;FROM&nbsp;"pnct_Picture"&nbsp;"picture"&nbsp;WHERE&nbsp;("picture"."id"=?)
35 |912<br/>0.21/sec<br/>1.00/call<br/>0.02% |28,349.01&nbsp;ms<br/>6ms/sec<br/>31ms/call<br/>0.19% |28,008<br/>6.56/sec<br/>30.71/call<br/>0.20% |138,671&nbsp;blks<br/>32.48&nbsp;blks/sec<br/>152.05&nbsp;blks/call<br/>0.00% |51,886&nbsp;blks<br/>12.15&nbsp;blks/sec<br/>56.89&nbsp;blks/call<br/>0.01% |1&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.04% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |27,860.15&nbsp;ms<br/>6ms/sec<br/>30ms/call<br/>0.51% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |select&nbsp;q,&nbsp;result&nbsp;from&nbsp;translation_proxy.cache&nbsp;where&nbsp;source&nbsp;=&nbsp;?&nbsp;and&nbsp;target&nbsp;=&nbsp;?&nbsp;and&nbsp;md5(q)&nbsp;in&nbsp;(md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?))
36 |9,852<br/>2.31/sec<br/>1.00/call<br/>0.22% |25,942.61&nbsp;ms<br/>6ms/sec<br/>2ms/call<br/>0.17% |9,852<br/>2.31/sec<br/>1.00/call<br/>0.07% |510,626&nbsp;blks<br/>119.60&nbsp;blks/sec<br/>51.83&nbsp;blks/call<br/>0.01% |210,655&nbsp;blks<br/>49.34&nbsp;blks/sec<br/>21.38&nbsp;blks/call<br/>0.06% |254&nbsp;blks<br/>0.06&nbsp;blks/sec<br/>0.03&nbsp;blks/call<br/>9.51% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.02% |21,922.38&nbsp;ms<br/>5ms/sec<br/>2ms/call<br/>0.41% |0.03&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.03% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;COUNT(?)&nbsp;FROM&nbsp;"pnct_NetworkEvent"&nbsp;"t"&nbsp;WHERE&nbsp;"userId"=?
37 |761<br/>0.18/sec<br/>1.00/call<br/>0.02% |25,408.19&nbsp;ms<br/>5ms/sec<br/>33ms/call<br/>0.17% |16,997<br/>3.98/sec<br/>22.34/call<br/>0.12% |59,370&nbsp;blks<br/>13.91&nbsp;blks/sec<br/>78.02&nbsp;blks/call<br/>0.00% |37,535&nbsp;blks<br/>8.79&nbsp;blks/sec<br/>49.32&nbsp;blks/call<br/>0.01% |1&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.04% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.02% |25,105.88&nbsp;ms<br/>5ms/sec<br/>32ms/call<br/>0.46% |0.02&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.02% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |select&nbsp;q,&nbsp;result&nbsp;from&nbsp;translation_proxy.cache&nbsp;where&nbsp;source&nbsp;=&nbsp;?&nbsp;and&nbsp;target&nbsp;=&nbsp;?&nbsp;and&nbsp;md5(q)&nbsp;in&nbsp;(md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?))
38 |777<br/>0.18/sec<br/>1.00/call<br/>0.02% |24,919.74&nbsp;ms<br/>5ms/sec<br/>32ms/call<br/>0.17% |23,319<br/>5.46/sec<br/>30.01/call<br/>0.17% |104,434&nbsp;blks<br/>24.46&nbsp;blks/sec<br/>134.41&nbsp;blks/call<br/>0.00% |49,654&nbsp;blks<br/>11.63&nbsp;blks/sec<br/>63.90&nbsp;blks/call<br/>0.01% |3&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.11% |1&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.01% |24,566.15&nbsp;ms<br/>5ms/sec<br/>31ms/call<br/>0.45% |0.03&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.03% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |select&nbsp;q,&nbsp;result&nbsp;from&nbsp;translation_proxy.cache&nbsp;where&nbsp;source&nbsp;=&nbsp;?&nbsp;and&nbsp;target&nbsp;=&nbsp;?&nbsp;and&nbsp;md5(q)&nbsp;in&nbsp;(md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?))
39 |710<br/>0.17/sec<br/>1.00/call<br/>0.02% |24,821.84&nbsp;ms<br/>5ms/sec<br/>34ms/call<br/>0.17% |21,853<br/>5.12/sec<br/>30.78/call<br/>0.16% |92,972&nbsp;blks<br/>21.78&nbsp;blks/sec<br/>130.95&nbsp;blks/call<br/>0.00% |45,901&nbsp;blks<br/>10.75&nbsp;blks/sec<br/>64.65&nbsp;blks/call<br/>0.01% |1&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.04% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |24,432.82&nbsp;ms<br/>5ms/sec<br/>34ms/call<br/>0.45% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |select&nbsp;q,&nbsp;result&nbsp;from&nbsp;translation_proxy.cache&nbsp;where&nbsp;source&nbsp;=&nbsp;?&nbsp;and&nbsp;target&nbsp;=&nbsp;?&nbsp;and&nbsp;md5(q)&nbsp;in&nbsp;(md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?))
40 |770<br/>0.18/sec<br/>1.00/call<br/>0.02% |24,388.93&nbsp;ms<br/>5ms/sec<br/>31ms/call<br/>0.16% |23,351<br/>5.47/sec<br/>30.33/call<br/>0.17% |109,468&nbsp;blks<br/>25.64&nbsp;blks/sec<br/>142.17&nbsp;blks/call<br/>0.00% |47,058&nbsp;blks<br/>11.02&nbsp;blks/sec<br/>61.11&nbsp;blks/call<br/>0.01% |4&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>0.15% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |23,964.68&nbsp;ms<br/>5ms/sec<br/>31ms/call<br/>0.44% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |select&nbsp;q,&nbsp;result&nbsp;from&nbsp;translation_proxy.cache&nbsp;where&nbsp;source&nbsp;=&nbsp;?&nbsp;and&nbsp;target&nbsp;=&nbsp;?&nbsp;and&nbsp;md5(q)&nbsp;in&nbsp;(md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?))
41 |775<br/>0.18/sec<br/>1.00/call<br/>0.02% |23,728.46&nbsp;ms<br/>5ms/sec<br/>30ms/call<br/>0.16% |24,938<br/>5.84/sec<br/>32.18/call<br/>0.18% |113,878&nbsp;blks<br/>26.67&nbsp;blks/sec<br/>146.94&nbsp;blks/call<br/>0.00% |51,116&nbsp;blks<br/>11.97&nbsp;blks/sec<br/>65.96&nbsp;blks/call<br/>0.01% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.07% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |23,353.17&nbsp;ms<br/>5ms/sec<br/>30ms/call<br/>0.43% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |select&nbsp;q,&nbsp;result&nbsp;from&nbsp;translation_proxy.cache&nbsp;where&nbsp;source&nbsp;=&nbsp;?&nbsp;and&nbsp;target&nbsp;=&nbsp;?&nbsp;and&nbsp;md5(q)&nbsp;in&nbsp;(md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?))
42 |792<br/>0.19/sec<br/>1.00/call<br/>0.02% |22,789.01&nbsp;ms<br/>5ms/sec<br/>28ms/call<br/>0.15% |25,841<br/>6.05/sec<br/>32.63/call<br/>0.19% |120,579&nbsp;blks<br/>28.24&nbsp;blks/sec<br/>152.25&nbsp;blks/call<br/>0.00% |51,592&nbsp;blks<br/>12.08&nbsp;blks/sec<br/>65.14&nbsp;blks/call<br/>0.01% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.07% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |22,415.36&nbsp;ms<br/>5ms/sec<br/>28ms/call<br/>0.41% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |select&nbsp;q,&nbsp;result&nbsp;from&nbsp;translation_proxy.cache&nbsp;where&nbsp;source&nbsp;=&nbsp;?&nbsp;and&nbsp;target&nbsp;=&nbsp;?&nbsp;and&nbsp;md5(q)&nbsp;in&nbsp;(md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?))
43 |678<br/>0.16/sec<br/>1.00/call<br/>0.01% |21,702.82&nbsp;ms<br/>5ms/sec<br/>32ms/call<br/>0.14% |20,741<br/>4.86/sec<br/>30.59/call<br/>0.15% |85,328&nbsp;blks<br/>19.99&nbsp;blks/sec<br/>125.85&nbsp;blks/call<br/>0.00% |43,787&nbsp;blks<br/>10.26&nbsp;blks/sec<br/>64.58&nbsp;blks/call<br/>0.01% |1&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.04% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |21,340.88&nbsp;ms<br/>4ms/sec<br/>31ms/call<br/>0.39% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |select&nbsp;q,&nbsp;result&nbsp;from&nbsp;translation_proxy.cache&nbsp;where&nbsp;source&nbsp;=&nbsp;?&nbsp;and&nbsp;target&nbsp;=&nbsp;?&nbsp;and&nbsp;md5(q)&nbsp;in&nbsp;(md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?))
44 |227,710<br/>53.34/sec<br/>1.00/call<br/>5.04% |20,847.08&nbsp;ms<br/>4ms/sec<br/>0s/call<br/>0.14% |227,308<br/>53.24/sec<br/>1.00/call<br/>1.65% |1,445,459&nbsp;blks<br/>338.57&nbsp;blks/sec<br/>6.35&nbsp;blks/call<br/>0.02% |381,265&nbsp;blks<br/>89.30&nbsp;blks/sec<br/>1.67&nbsp;blks/call<br/>0.11% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.07% |9&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.07% |13,027.66&nbsp;ms<br/>3ms/sec<br/>0s/call<br/>0.24% |0.26&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.22% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_AnyPost"&nbsp;"t"&nbsp;WHERE&nbsp;"t"."id"=?&nbsp;LIMIT&nbsp;?
45 |661<br/>0.15/sec<br/>1.00/call<br/>0.01% |20,605.80&nbsp;ms<br/>4ms/sec<br/>31ms/call<br/>0.14% |4,924<br/>1.15/sec<br/>7.45/call<br/>0.04% |2,809,827&nbsp;blks<br/>658.15&nbsp;blks/sec<br/>4.26K&nbsp;blks/call<br/>0.03% |1,303,017&nbsp;blks<br/>305.21&nbsp;blks/sec<br/>1.98K&nbsp;blks/call<br/>0.37% |1&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.04% |47&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.07&nbsp;blks/call<br/>0.37% |15,363.43&nbsp;ms<br/>3ms/sec<br/>23ms/call<br/>0.28% |0.46&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.39% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_PicturePost"&nbsp;"t"&nbsp;WHERE&nbsp;(id&nbsp;in&nbsp;(select&nbsp;"postId"&nbsp;from&nbsp;"pnct_PictureLike"&nbsp;where&nbsp;"userId"&nbsp;=&nbsp;?))&nbsp;AND&nbsp;(t.image_original&nbsp;is&nbsp;not&nbsp;null&nbsp;OR&nbsp;t."pictureId"&nbsp;is&nbsp;not&nbsp;null)&nbsp;ORDER&nbsp;BY&nbsp;(select&nbsp;max(created)&nbsp;from&nbsp;"pnct_PictureLike"&nbsp;where&nbsp;"userId"&nbsp;=&nbsp;?&nbsp;and&nbsp;"postId"&nbsp;=&nbsp;t.id)&nbsp;DESC&nbsp;LIMIT&nbsp;?
46 |746<br/>0.17/sec<br/>1.00/call<br/>0.02% |20,364.19&nbsp;ms<br/>4ms/sec<br/>27ms/call<br/>0.14% |23,357<br/>5.47/sec<br/>31.31/call<br/>0.17% |125,605&nbsp;blks<br/>29.42&nbsp;blks/sec<br/>168.37&nbsp;blks/call<br/>0.00% |40,941&nbsp;blks<br/>9.59&nbsp;blks/sec<br/>54.88&nbsp;blks/call<br/>0.01% |7&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>0.26% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.02% |19,999.95&nbsp;ms<br/>4ms/sec<br/>26ms/call<br/>0.37% |0.03&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.03% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |select&nbsp;q,&nbsp;result&nbsp;from&nbsp;translation_proxy.cache&nbsp;where&nbsp;source&nbsp;=&nbsp;?&nbsp;and&nbsp;target&nbsp;=&nbsp;?&nbsp;and&nbsp;md5(q)&nbsp;in&nbsp;(md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?))
47 |20,415<br/>4.78/sec<br/>1.00/call<br/>0.45% |18,445.20&nbsp;ms<br/>4ms/sec<br/>0s/call<br/>0.12% |128,711<br/>30.15/sec<br/>6.30/call<br/>0.93% |154,243&nbsp;blks<br/>36.13&nbsp;blks/sec<br/>7.56&nbsp;blks/call<br/>0.00% |76,540&nbsp;blks<br/>17.93&nbsp;blks/sec<br/>3.75&nbsp;blks/call<br/>0.02% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |1&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.01% |17,771.88&nbsp;ms<br/>4ms/sec<br/>0s/call<br/>0.33% |0.03&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.02% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;*&nbsp;FROM&nbsp;"pnct_StorageFile"&nbsp;"t"&nbsp;WHERE&nbsp;"binId"&nbsp;in&nbsp;(?,?)
48 |9,865<br/>2.31/sec<br/>1.00/call<br/>0.22% |17,649.27&nbsp;ms<br/>4ms/sec<br/>1ms/call<br/>0.12% |9,865<br/>2.31/sec<br/>1.00/call<br/>0.07% |13,357,256&nbsp;blks<br/>3.13K&nbsp;blks/sec<br/>1.36K&nbsp;blks/call<br/>0.16% |324,719&nbsp;blks<br/>76.06&nbsp;blks/sec<br/>32.92&nbsp;blks/call<br/>0.09% |2&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.07% |1&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.01% |10,351.04&nbsp;ms<br/>2ms/sec<br/>1ms/call<br/>0.19% |0.02&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.01% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;COUNT(?)&nbsp;FROM&nbsp;"pnct_AnyPost"&nbsp;"t"&nbsp;WHERE&nbsp;"userId"=?
49 |3,655<br/>0.86/sec<br/>1.00/call<br/>0.08% |16,752.08&nbsp;ms<br/>3ms/sec<br/>4ms/call<br/>0.11% |28,211<br/>6.61/sec<br/>7.72/call<br/>0.20% |161,582&nbsp;blks<br/>37.85&nbsp;blks/sec<br/>44.21&nbsp;blks/call<br/>0.00% |51,333&nbsp;blks<br/>12.02&nbsp;blks/sec<br/>14.04&nbsp;blks/call<br/>0.01% |27&nbsp;blks<br/>0.01&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>1.01% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |16,279.89&nbsp;ms<br/>3ms/sec<br/>4ms/call<br/>0.30% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |select&nbsp;q,&nbsp;result&nbsp;from&nbsp;translation_proxy.cache&nbsp;where&nbsp;source&nbsp;=&nbsp;?&nbsp;and&nbsp;target&nbsp;=&nbsp;?&nbsp;and&nbsp;md5(q)&nbsp;in&nbsp;(md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?),&nbsp;md5(?))
50 |214<br/>0.05/sec<br/>1.00/call<br/>0.00% |15,059.36&nbsp;ms<br/>3ms/sec<br/>70ms/call<br/>0.10% |4,413<br/>1.03/sec<br/>20.62/call<br/>0.03% |11,081,476&nbsp;blks<br/>2.60K&nbsp;blks/sec<br/>51.79K&nbsp;blks/call<br/>0.13% |507,649&nbsp;blks<br/>118.91&nbsp;blks/sec<br/>2.38K&nbsp;blks/call<br/>0.14% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |6,995.48&nbsp;ms<br/>1ms/sec<br/>32ms/call<br/>0.13% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |0.00&nbsp;ms<br/>0s/sec<br/>0s/call<br/>0.00% |SELECT&nbsp;"t"."id"&nbsp;AS&nbsp;"t0_c0",&nbsp;"t"."pictureId"&nbsp;AS&nbsp;"t0_c1",&nbsp;"t"."parentId"&nbsp;AS&nbsp;"t0_c2",&nbsp;"t"."userId"&nbsp;AS&nbsp;"t0_c3",&nbsp;"t"."boardId"&nbsp;AS&nbsp;"t0_c4",&nbsp;"t"."source"&nbsp;AS&nbsp;"t0_c5",&nbsp;"t"."sourceDomain"&nbsp;AS&nbsp;"t0_c6",&nbsp;"t"."channel"&nbsp;AS&nbsp;"t0_c7",&nbsp;"t"."message"&nbsp;AS&nbsp;"t0_c8",&nbsp;"t"."likes"&nbsp;AS&nbsp;"t0_c9",&nbsp;"t"."reposts"&nbsp;AS&nbsp;"t0_c10",&nbsp;"t"."comments"&nbsp;AS&nbsp;"t0_c11",&nbsp;"t"."created"&nbsp;AS&nbsp;"t0_c12",&nbsp;"t"."newsletter"&nbsp;AS&nbsp;"t0_c13",&nbsp;"t"."nl_complete"&nbsp;AS&nbsp;"t0_c14",&nbsp;"t"."nl_last_id"&nbsp;AS&nbsp;"t0_c15",&nbsp;"t"."changed"&nbsp;AS&nbsp;"t0_c16",&nbsp;"t"."tsvector"&nbsp;AS&nbsp;"t0_c17",&nbsp;"t"."creation_method"&nbsp;AS&nbsp;"t0_c18",&nbsp;"t"."pushed_to_facebook"&nbsp;AS&nbsp;"t0_c19",&nbsp;"t"."pushed_to_mailru"&nbsp;AS&nbsp;"t0_c20",&nbsp;"t"."pushed_to_odnoklassniki"&nbsp;AS&nbsp;"t0_c21",&nbsp;"t"."merchandise"&nbsp;AS&nbsp;"t0_c22",&nbsp;"t"."sourceOriginal"&nbsp;AS&nbsp;"t0_c23",&nbsp;"t"."actor_age"&nbsp;AS&nbsp;"t0_c24",&nbsp;"t"."actor_is_male"&nbsp;AS&nbsp;"t0_c25",&nbsp;"t"."iframely_url"&nbsp;AS&nbsp;"t0_c26",&nbsp;"t"."source_original"&nbsp;AS&nbsp;"t0_c27",&nbsp;"t"."redirect_checked"&nbsp;AS&nbsp;"t0_c28",&nbsp;"t"."image_original"&nbsp;AS&nbsp;"t0_c29",&nbsp;"t"."image_original_md5"&nbsp;AS&nbsp;"t0_c30",&nbsp;"t"."image_original_width"&nbsp;AS&nbsp;"t0_c31",&nbsp;"t"."image_original_height"&nbsp;AS&nbsp;"t0_c32",&nbsp;"t"."alt_body"&nbsp;AS&nbsp;"t0_c33",&nbsp;"t"."card_img_style"&nbsp;AS&nbsp;"t0_c34",&nbsp;"t"."card_msg_style"&nbsp;AS&nbsp;"t0_c35",&nbsp;"t"."is_private"&nbsp;AS&nbsp;"t0_c36",&nbsp;"t"."misc"&nbsp;AS&nbsp;"t0_c37",&nbsp;"board"."id"&nbsp;AS&nbsp;"t1_c0",&nbsp;"board"."userId"&nbsp;AS&nbsp;"t1_c1",&nbsp;"board"."categoryId"&nbsp;AS&nbsp;"t1_c2",&nbsp;"board"."title"&nbsp;AS&nbsp;"t1_c3",&nbsp;"board"."url"&nbsp;AS&nbsp;"t1_c4",&nbsp;"board"."description"&nbsp;AS&nbsp;"t1_c5",&nbsp;"board"."access"&nbsp;AS&nbsp;"t1_c6",&nbsp;"board"."cover"&nbsp;AS&nbsp;"t1_c7",&nbsp;"board"."sortOrder"&nbsp;AS&nbsp;"t1_c8",&nbsp;"board"."avatar"&nbsp;AS&nbsp;"t1_c9",&nbsp;"board"."php_modifier"&nbsp;AS&nbsp;"t1_c10",&nbsp;"board"."recommended"&nbsp;AS&nbsp;"t1_c11",&nbsp;"board"."target_gender"&nbsp;AS&nbsp;"t1_c12",&nbsp;"board"."target_age_from"&nbsp;AS&nbsp;"t1_c13",&nbsp;"board"."target_age_to"&nbsp;AS&nbsp;"t1_c14",&nbsp;"board"."created"&nbsp;AS&nbsp;"t1_c15",&nbsp;"board"."commentsForbidden"&nbsp;AS&nbsp;"t1_c16",&nbsp;"board"."target_set_manually"&nbsp;AS&nbsp;"t1_c17",&nbsp;"board"."followers"&nbsp;AS&nbsp;"t1_c18",&nbsp;"board"."posts"&nbsp;AS&nbsp;"t1_c19",&nbsp;"board"."last_post_added"&nbsp;AS&nbsp;"t1_c20",&nbsp;"board"."for_subscribers_only"&nbsp;AS&nbsp;"t1_c21",&nbsp;"board"."editors_choice"&nbsp;AS&nbsp;"t1_c22",&nbsp;"board"."cover_img_src"&nbsp;AS&nbsp;"t1_c23",&nbsp;"board"....


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L001"></a>
[Table of contents](#postgres-checkup_top)
# L001 Table sizes #

## Observations ##


### Master (`db2`) ###
Table | Rows | &#9660;&nbsp;Total size | Table size | Index(es) Size | TOAST Size
------|------|------------|------------|----------------|------------
*** TOTAL *** | ~3B | 613 GB (100.00%) | 407 GB (100.00%) | 205 GB (100.00%) | 1077 MB (100.00%)
post_subtitles | ~280M | 129 GB (21.03%) | 111 GB (27.33%) | 18 GB (8.65%) | 488 kB (0.04%)
bot_visit | ~716M | 92 GB (15.07%) | 58 GB (14.13%) | 35 GB (17.01%) | 8192 bytes (0.00%)
post_view | ~1B | 88 GB (14.40%) | 44 GB (10.86%) | 44 GB (21.48%) | 8192 bytes (0.00%)
pnct_network_follow | ~266M | 86 GB (13.97%) | 39 GB (9.54%) | 47 GB (22.85%) | 56 kB (0.01%)
pnct_PicturePost | ~58M | 52 GB (8.42%) | 32 GB (7.86%) | 19 GB (9.43%) | 326 MB (30.23%)
click | ~260M | 51 GB (8.24%) | 39 GB (9.65%) | 11 GB (5.48%) | 160 kB (0.01%)
pnct_NetworkEvent | ~148M | 23 GB (3.76%) | 16 GB (3.82%) | 7710 MB (3.67%) | 8192 bytes (0.00%)
user_visits | ~54M | 19 GB (3.11%) | 14 GB (3.51%) | 4872 MB (2.32%) | 8192 bytes (0.00%)
action | ~39M | 15 GB (2.45%) | 14 GB (3.48%) | 857 MB (0.41%) | 8192 bytes (0.00%)
search_log | ~100M | 10 GB (1.68%) | 8377 MB (2.01%) | 2151 MB (1.02%) | 56 kB (0.01%)
translation_proxy.cache | ~20M | 8279 MB (1.32%) | 6321 MB (1.52%) | 1956 MB (0.93%) | 1984 kB (0.18%)
pnct_StorageFile | ~44M | 7628 MB (1.21%) | 5730 MB (1.37%) | 1898 MB (0.90%) | 8192 bytes (0.00%)
pnct_User | ~7M | 5194 MB (0.83%) | 2462 MB (0.59%) | 2726 MB (1.30%) | 5592 kB (0.51%)
archive.pnct_PicturePost | ~6M | 4019 MB (0.64%) | 3453 MB (0.83%) | 491 MB (0.23%) | 76 MB (7.03%)
pnct_NetworkFollowUser | ~26M | 3382 MB (0.54%) | 1493 MB (0.36%) | 1889 MB (0.90%) | <no value>
pnct_PictureLike | ~19M | 2446 MB (0.39%) | 1232 MB (0.30%) | 1214 MB (0.58%) | 8192 bytes (0.00%)
pnct_NetworkFollowBoard | ~17M | 2203 MB (0.35%) | 957 MB (0.23%) | 1245 MB (0.59%) | <no value>
pnct_AnyPost | ~2M | 2125 MB (0.34%) | 1388 MB (0.33%) | 712 MB (0.34%) | 25 MB (2.32%)
google_translate.cache_orig | ~6M | 2116 MB (0.34%) | 1648 MB (0.40%) | 468 MB (0.22%) | 8192 bytes (0.00%)
conveyor_failed | ~2M | 1834 MB (0.29%) | 1662 MB (0.40%) | 172 MB (0.08%) | 8192 bytes (0.00%)
pnct_Board | ~1M | 1077 MB (0.17%) | 261 MB (0.06%) | 813 MB (0.39%) | 2648 kB (0.24%)
pnct_StorageBin | ~13M | 1069 MB (0.17%) | 784 MB (0.19%) | 285 MB (0.14%) | 8192 bytes (0.00%)
archive.log | ~6M | 1010 MB (0.16%) | 539 MB (0.13%) | 471 MB (0.22%) | 8192 bytes (0.00%)
job_move_old_images | ~9M | 949 MB (0.15%) | 639 MB (0.15%) | 310 MB (0.15%) | 8192 bytes (0.00%)
pnct_PictureFragment | ~12M | 944 MB (0.15%) | 590 MB (0.14%) | 354 MB (0.17%) | 8192 bytes (0.00%)
pnct_Picture | ~9M | 821 MB (0.13%) | 618 MB (0.15%) | 203 MB (0.10%) | <no value>
job_move_old_binimages | ~3M | 733 MB (0.12%) | 329 MB (0.08%) | 404 MB (0.19%) | 8192 bytes (0.00%)
archive.pnct_AnyPost | ~914k | 612 MB (0.10%) | 538 MB (0.13%) | 45 MB (0.02%) | 29 MB (2.70%)
token | ~2M | 500 MB (0.08%) | 371 MB (0.09%) | 128 MB (0.06%) | 8192 bytes (0.00%)
postgresql_dba.sample_query | ~15k | 475 MB (0.08%) | 5680 kB (0.00%) | 344 kB (0.00%) | 469 MB (43.51%)
pnct_Hash | ~1M | 196 MB (0.03%) | 116 MB (0.03%) | 81 MB (0.04%) | <no value>
postfix.list_4_mar_2018 | ~2M | 189 MB (0.03%) | 128 MB (0.03%) | 61 MB (0.03%) | 8192 bytes (0.00%)
pnct_NetworkInvite | ~1M | 175 MB (0.03%) | 146 MB (0.03%) | 30 MB (0.01%) | 8192 bytes (0.00%)
spammers20181125.pnct_network_follow | ~2M | 158 MB (0.03%) | 158 MB (0.04%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
proxy_content | ~28k | 131 MB (0.02%) | 7344 kB (0.00%) | 688 kB (0.00%) | 124 MB (11.47%)
pnct_UserNotify | ~2M | 110 MB (0.02%) | 77 MB (0.02%) | 33 MB (0.02%) | <no value>
postfix.to_upsubscribe_20180305 | ~2M | 88 MB (0.01%) | 88 MB (0.02%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
jobs_translate.mv_messages_new2lock | ~467k | 88 MB (0.01%) | 78 MB (0.02%) | 10 MB (0.00%) | 8192 bytes (0.00%)
jobs_translate.cache_20170526_1 | ~277k | 81 MB (0.01%) | 81 MB (0.02%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
archive.antispam_snapshot_board_20140723 | ~483k | 70 MB (0.01%) | 69 MB (0.02%) | 0 bytes (0.00%) | 1032 kB (0.09%)
tmp.male_boards_latest_post | ~126k | 67 MB (0.01%) | 60 MB (0.01%) | 6240 kB (0.00%) | 56 kB (0.01%)
pnct_unsubscribe_mail | ~349k | 58 MB (0.01%) | 28 MB (0.01%) | 30 MB (0.01%) | 8192 bytes (0.00%)
pnct_PictureComment | ~286k | 57 MB (0.01%) | 45 MB (0.01%) | 13 MB (0.01%) | 8192 bytes (0.00%)
jobs_translate.cache_backup | ~268k | 52 MB (0.01%) | 52 MB (0.01%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
user_directory | ~288k | 44 MB (0.01%) | 36 MB (0.01%) | 8888 kB (0.00%) | 8192 bytes (0.00%)
board_directory | ~317k | 41 MB (0.01%) | 32 MB (0.01%) | 9776 kB (0.00%) | 72 kB (0.01%)
host | ~222k | 37 MB (0.01%) | 16 MB (0.00%) | 21 MB (0.01%) | <no value>
tmp.male_boards | ~126k | 36 MB (0.01%) | 26 MB (0.01%) | 0 bytes (0.00%) | 11 MB (1.00%)
ml.posts_fresh | ~12k | 31 MB (0.00%) | 31 MB (0.01%) | 0 bytes (0.00%) | 296 kB (0.03%)
pnct_Video | ~348k | 28 MB (0.00%) | 20 MB (0.00%) | 7656 kB (0.00%) | 8192 bytes (0.00%)
pnct_AnyBoard | ~26k | 26 MB (0.00%) | 6840 kB (0.00%) | 19 MB (0.01%) | 72 kB (0.01%)
postgresql_dba.normalyzed_query | ~12k | 22 MB (0.00%) | 21 MB (0.00%) | 280 kB (0.00%) | 816 kB (0.07%)
_gt_calc_title | ~333k | 22 MB (0.00%) | 22 MB (0.01%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
archive.pnct_Board | ~86k | 17 MB (0.00%) | 15 MB (0.00%) | 2040 kB (0.00%) | 288 kB (0.03%)
archive.pnct_User | ~49k | 16 MB (0.00%) | 16 MB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
tmp.hosts_views | ~235k | 16 MB (0.00%) | 16 MB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
archive.banned_posts_20150105 | ~33k | 15 MB (0.00%) | 15 MB (0.00%) | 0 bytes (0.00%) | 280 kB (0.03%)
tmp.hosts_views_lastmonth | ~235k | 15 MB (0.00%) | 15 MB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
tmp.checktranslate | ~181k | 15 MB (0.00%) | 15 MB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
ml.posts_fresh_predict_m | ~12k | 14 MB (0.00%) | 14 MB (0.00%) | 0 bytes (0.00%) | 352 kB (0.03%)
ml.posts_fresh_predict_fplus | ~12k | 14 MB (0.00%) | 14 MB (0.00%) | 0 bytes (0.00%) | 352 kB (0.03%)
_gt_calc_description | ~59k | 13 MB (0.00%) | 11 MB (0.00%) | 0 bytes (0.00%) | 1792 kB (0.16%)
tmp_is_male_all | ~190k | 9696 kB (0.00%) | 9688 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
tmp.msai_posts3 | ~12k | 8760 kB (0.00%) | 6736 kB (0.00%) | 2016 kB (0.00%) | 8192 bytes (0.00%)
antispam.money_fraud201801_followuser | ~136k | 8032 kB (0.00%) | 8032 kB (0.00%) | 0 bytes (0.00%) | <no value>
conveyor | ~8k | 7200 kB (0.00%) | 1152 kB (0.00%) | 6040 kB (0.00%) | 8192 bytes (0.00%)
ml.kusochek_learndata | ~5k | 6888 kB (0.00%) | 6640 kB (0.00%) | 168 kB (0.00%) | 80 kB (0.01%)
news_daily_edit | ~15k | 6816 kB (0.00%) | 5952 kB (0.00%) | 856 kB (0.00%) | 8192 bytes (0.00%)
_save_google_translate | ~27k | 4984 kB (0.00%) | 4976 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
archive.pnct_PictureComment | ~16k | 4864 kB (0.00%) | 4856 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
jobs_translate.cache_20170522 | ~17k | 4744 kB (0.00%) | 4736 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
tmp_small_images | ~6k | 3728 kB (0.00%) | 3648 kB (0.00%) | 0 bytes (0.00%) | 80 kB (0.01%)
hm_promo | ~32k | 2824 kB (0.00%) | 2104 kB (0.00%) | 712 kB (0.00%) | 8192 bytes (0.00%)
tmp.erotic_posts_20170907 | ~5k | 2432 kB (0.00%) | 2424 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
archive.spam_20140721_posts | ~6k | 2328 kB (0.00%) | 2320 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
_popular_search_queries_current_week | ~28k | 2056 kB (0.00%) | 2048 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
_tmp_fill_b_selector | ~10k | 1928 kB (0.00%) | 1864 kB (0.00%) | 0 bytes (0.00%) | 64 kB (0.01%)
archive.issue_3141_posts_wo_domain | ~2k | 1912 kB (0.00%) | 1760 kB (0.00%) | 0 bytes (0.00%) | 152 kB (0.01%)
editors_audit | ~22k | 1832 kB (0.00%) | 1328 kB (0.00%) | 504 kB (0.00%) | <no value>
tmp.erotic_posts_20170910 | ~4k | 1760 kB (0.00%) | 1752 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
tmp.erotic_posts_20170905 | ~4k | 1704 kB (0.00%) | 1696 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
jobs_translate.mv_titles_new2lock | ~18k | 1608 kB (0.00%) | 1184 kB (0.00%) | 416 kB (0.00%) | 8192 bytes (0.00%)
delme2 | ~2k | 1192 kB (0.00%) | 1136 kB (0.00%) | 0 bytes (0.00%) | 56 kB (0.01%)
archive.kino_i_tpm__pirate_video | ~18k | 1072 kB (0.00%) | 1072 kB (0.00%) | 0 bytes (0.00%) | <no value>
support539899.post | ~3k | 1048 kB (0.00%) | 992 kB (0.00%) | 0 bytes (0.00%) | 56 kB (0.01%)
pnct_NetworkRequest | ~7k | 976 kB (0.00%) | 504 kB (0.00%) | 464 kB (0.00%) | 8192 bytes (0.00%)
tmp.img_cannot_save | ~7k | 816 kB (0.00%) | 504 kB (0.00%) | 312 kB (0.00%) | <no value>
pnct_Autosourcer | ~1k | 568 kB (0.00%) | 472 kB (0.00%) | 88 kB (0.00%) | 8192 bytes (0.00%)
pnct_CmsPage | ~109 | 552 kB (0.00%) | 80 kB (0.00%) | 16 kB (0.00%) | 456 kB (0.04%)
antispam.money_fraud20180202_followuser | ~308 | 536 kB (0.00%) | 528 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
archive.pnct_AnyBoard | ~2k | 496 kB (0.00%) | 440 kB (0.00%) | 0 bytes (0.00%) | 56 kB (0.01%)
tmp.erotic_posts_20170911 | ~595 | 328 kB (0.00%) | 272 kB (0.00%) | 0 bytes (0.00%) | 56 kB (0.01%)
_pg_stat_statements_20171206_012800 | ~497 | 296 kB (0.00%) | 288 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
archive.spam_20140722_posts | ~761 | 280 kB (0.00%) | 272 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
news_all | ~523 | 256 kB (0.00%) | 184 kB (0.00%) | 64 kB (0.00%) | 8192 bytes (0.00%)
archive.spam_snapshot_20141025 | ~387 | 240 kB (0.00%) | 232 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
tmp.host_blacklisted_20170920 | ~3k | 240 kB (0.00%) | 232 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
tmp.host_blacklisted_snapshot_20170827 | ~2k | 216 kB (0.00%) | 208 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
sqitch.events | ~420 | 216 kB (0.00%) | 160 kB (0.00%) | 48 kB (0.00%) | 8192 bytes (0.00%)
archive.banned_boards_20150105 | ~529 | 208 kB (0.00%) | 136 kB (0.00%) | 0 bytes (0.00%) | 72 kB (0.01%)
pnct_PictureReport | ~1k | 192 kB (0.00%) | 136 kB (0.00%) | 48 kB (0.00%) | 8192 bytes (0.00%)
pnct_CmsArticle | ~1 | 168 kB (0.00%) | 40 kB (0.00%) | 16 kB (0.00%) | 112 kB (0.01%)
archive.spam_20140722_posts_2 | ~381 | 144 kB (0.00%) | 136 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
archive.board_restore_20160504_antiporn_epicfail | ~312 | 144 kB (0.00%) | 88 kB (0.00%) | 0 bytes (0.00%) | 56 kB (0.01%)
pghero_space_stats | ~448 | 144 kB (0.00%) | 72 kB (0.00%) | 64 kB (0.00%) | 8192 bytes (0.00%)
tmp.hosts | ~1000 | 144 kB (0.00%) | 136 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
jobs_translate.result_try2 | ~593 | 128 kB (0.00%) | 120 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
postgresql_dba.report | ~5 | 128 kB (0.00%) | 40 kB (0.00%) | 80 kB (0.00%) | 8192 bytes (0.00%)
tmp.host_yellowlisted | ~828 | 128 kB (0.00%) | 80 kB (0.00%) | 40 kB (0.00%) | 8192 bytes (0.00%)
ml.result_full | ~300 | 120 kB (0.00%) | 112 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
archive.spam_20140725_posts_issue_937 | ~43 | 120 kB (0.00%) | 112 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
tmp.events | ~158 | 120 kB (0.00%) | 80 kB (0.00%) | 32 kB (0.00%) | 8192 bytes (0.00%)
ml.kusochek_learndata_fplus_linregr | ~1 | 112 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 72 kB (0.01%)
ml.kusochek_learndata_fplus_linregr2 | ~1 | 112 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 72 kB (0.01%)
topic | ~537 | 112 kB (0.00%) | 72 kB (0.00%) | 32 kB (0.00%) | 8192 bytes (0.00%)
ml.kusochek_learndata_all_linregr | ~1 | 112 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 72 kB (0.01%)
u2b_schedule | ~119 | 112 kB (0.00%) | 72 kB (0.00%) | 32 kB (0.00%) | 8192 bytes (0.00%)
pnct_PictureCommentReport | ~571 | 112 kB (0.00%) | 72 kB (0.00%) | 32 kB (0.00%) | 8192 bytes (0.00%)
ml.kusochek_learndata_linregr2 | ~1 | 112 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 72 kB (0.01%)
ml.kusochek_learndata_m_linregr | ~1 | 112 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 72 kB (0.01%)
ml.kusochek_learndata_linregr | ~1 | 112 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 72 kB (0.01%)
pnct_BoardUser | ~704 | 104 kB (0.00%) | 72 kB (0.00%) | 32 kB (0.00%) | <no value>
archive.abuse_snapshot_20150208 | ~195 | 104 kB (0.00%) | 96 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
pghero_query_stats | ~100 | 104 kB (0.00%) | 64 kB (0.00%) | 32 kB (0.00%) | 8192 bytes (0.00%)
archive.spam_20140727_posts_1 | ~230 | 104 kB (0.00%) | 96 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
ml.result_fplus_glm | ~200 | 96 kB (0.00%) | 88 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
ml.result_fplus | ~200 | 96 kB (0.00%) | 88 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
ml.kusochek_learndata_fplus_svm_random | ~2 | 96 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 56 kB (0.01%)
jobs_translate.character_entity | ~252 | 88 kB (0.00%) | 48 kB (0.00%) | 32 kB (0.00%) | 8192 bytes (0.00%)
pnct_I18N | ~98 | 88 kB (0.00%) | 48 kB (0.00%) | 32 kB (0.00%) | 8192 bytes (0.00%)
sqitch.changes | ~111 | 88 kB (0.00%) | 64 kB (0.00%) | 16 kB (0.00%) | 8192 bytes (0.00%)
ban_0diet_20151002 | ~492 | 88 kB (0.00%) | 80 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
ml.kusochek_top_words | ~50 | 80 kB (0.00%) | 40 kB (0.00%) | 32 kB (0.00%) | 8192 bytes (0.00%)
news_vremyadengi_edit | ~10 | 80 kB (0.00%) | 40 kB (0.00%) | 32 kB (0.00%) | 8192 bytes (0.00%)
sqitch.projects | ~1 | 80 kB (0.00%) | 40 kB (0.00%) | 32 kB (0.00%) | 8192 bytes (0.00%)
tmp.projects | ~1 | 80 kB (0.00%) | 40 kB (0.00%) | 32 kB (0.00%) | 8192 bytes (0.00%)
archive.users_banned_0diet_20150829 | ~105 | 72 kB (0.00%) | 64 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
tmp.changes | ~16 | 64 kB (0.00%) | 40 kB (0.00%) | 16 kB (0.00%) | 8192 bytes (0.00%)
jobs_translate.last_id_board | ~1 | 64 kB (0.00%) | 64 kB (0.00%) | 0 bytes (0.00%) | <no value>
pnct_CloudStorageBin | ~40 | 64 kB (0.00%) | 40 kB (0.00%) | 16 kB (0.00%) | 8192 bytes (0.00%)
pnct_Ads | ~25 | 64 kB (0.00%) | 40 kB (0.00%) | 16 kB (0.00%) | 8192 bytes (0.00%)
proxy_config | ~8 | 64 kB (0.00%) | 40 kB (0.00%) | 16 kB (0.00%) | 8192 bytes (0.00%)
editor | ~11 | 64 kB (0.00%) | 40 kB (0.00%) | 16 kB (0.00%) | 8192 bytes (0.00%)
adtest | ~4 | 64 kB (0.00%) | 40 kB (0.00%) | 16 kB (0.00%) | 8192 bytes (0.00%)
postgresql_dba.project | ~1 | 64 kB (0.00%) | 40 kB (0.00%) | 16 kB (0.00%) | 8192 bytes (0.00%)
pnct_PictureCategory | ~43 | 64 kB (0.00%) | 40 kB (0.00%) | 16 kB (0.00%) | 8192 bytes (0.00%)
translation_proxy.authcache | ~3 | 64 kB (0.00%) | 40 kB (0.00%) | 16 kB (0.00%) | 8192 bytes (0.00%)
antispam.money_fraud201801 | ~10 | 56 kB (0.00%) | 48 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
pnct_GiftMenu | ~6 | 56 kB (0.00%) | 40 kB (0.00%) | 16 kB (0.00%) | <no value>
archive.likes_userecho_273376 | ~150 | 56 kB (0.00%) | 48 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
archive.abuse_snapshot_20141001 | ~20 | 56 kB (0.00%) | 48 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
jobs_translate.current_last_id | ~1 | 56 kB (0.00%) | 56 kB (0.00%) | 0 bytes (0.00%) | <no value>
ml.kusochek_learndata_fplus_glm_summary | ~1 | 48 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
tmp.pnct_User | ~1 | 48 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
support539899.board | ~8 | 48 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
ml.kusochek_learndata_fplus_linregr2_summary | ~1 | 48 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
ml.kusochek_learndata_fplus_linregr_summary | ~1 | 48 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
ml.kusochek_learndata_fplus_svm | ~1 | 48 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
tmp.pnct_Board | ~5 | 48 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
ml.kusochek_learndata_fplus_svm_summary | ~1 | 48 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
ml.kusochek_tsvector_top_words | ~50 | 48 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
archive.abuse_board_20150208 | ~1 | 48 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
ml.kusochek_learndata_linregr2_summary | ~1 | 48 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
ml.kusochek_learndata_linregr_summary | ~1 | 48 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
ban_20151004 | ~12 | 48 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
ml.kusochek_learndata_m_linregr_summary | ~1 | 48 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
tmp.porn_posts_20170905 | ~10 | 48 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
ml.kusochek_learndata_all_linregr_summary | ~1 | 48 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
ml.kusochek_learndata_fplus_glm | ~1 | 48 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
kcache_avail | ~1 | 40 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | <no value>
madlib.migrationhistory | ~1 | 40 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | <no value>
antispam.followuser_20160312_2 | ~1 | 40 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | <no value>
antispam.followuser_20160312 | ~5 | 40 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | <no value>
ml.v_result_fplus_glm_out | ~1 | 40 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | <no value>
ml.v_result_fplus_new_out | ~1 | 40 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | <no value>
ml.v_result_fplus_old_out | ~1 | 40 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | <no value>
ml.v_result_full_new_out | ~1 | 40 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | <no value>
ml.v_result_full_old_out | ~1 | 40 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | <no value>
antispam.followuser_20160305 | ~12 | 40 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | <no value>
jobs_translate.current_last_id_board | ~1 | 40 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | <no value>
jobs_translate.last_id | ~1 | 40 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | <no value>
antispam.followuser_20160302 | ~21 | 40 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | <no value>
_tmp_facebook_contacts | ~40 | 40 kB (0.00%) | 40 kB (0.00%) | 0 bytes (0.00%) | <no value>
pnct_ThemeImage | ~0 | 24 kB (0.00%) | 0 bytes (0.00%) | 16 kB (0.00%) | 8192 bytes (0.00%)
sqitch.tags | ~0 | 24 kB (0.00%) | 0 bytes (0.00%) | 16 kB (0.00%) | 8192 bytes (0.00%)
jobs_translate.mv_messages_unique_rus_tbl | ~0 | 24 kB (0.00%) | 0 bytes (0.00%) | 16 kB (0.00%) | 8192 bytes (0.00%)
tmp.tags | ~0 | 24 kB (0.00%) | 0 bytes (0.00%) | 16 kB (0.00%) | 8192 bytes (0.00%)
pnct_CmsBlock | ~0 | 16 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%) | 8192 bytes (0.00%)
tmp.dependencies | ~0 | 16 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%) | 8192 bytes (0.00%)
pnct_MailQueue | ~0 | 16 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%) | 8192 bytes (0.00%)
sqitch.dependencies | ~0 | 16 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%) | 8192 bytes (0.00%)
pnct_ThemeColorScheme | ~0 | 16 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%) | 8192 bytes (0.00%)
tmp.board_posts | ~0 | 16 kB (0.00%) | 16 kB (0.00%) | 0 bytes (0.00%) | <no value>
basic_auth.tokens | ~0 | 16 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%) | 8192 bytes (0.00%)
pnct_Location | ~0 | 16 kB (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%) | 8192 bytes (0.00%)
archive.spam_20140728_posts_1 | ~0 | 8192 bytes (0.00%) | 0 bytes (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%)
pnct_Gift | ~0 | 8192 bytes (0.00%) | 0 bytes (0.00%) | 8192 bytes (0.00%) | <no value>
anna_preds | ~0 | 0 bytes (0.00%) | 0 bytes (0.00%) | 0 bytes (0.00%) | <no value>
antispam.followuser_20160309 | ~0 | 0 bytes (0.00%) | 0 bytes (0.00%) | 0 bytes (0.00%) | <no value>


## Conclusions ##


## Recommendations ##
