# A005 Extensions #

## Observations ##


### Master (`db2`) ###
&#9660;&nbsp;Database | Extension name | Installed version | Default version | Is old
---------|----------------|-------------------|-----------------|--------
_4vkusa_ru | pg_repack | 1.4.2 | 1.4.4 | OLD
_4vkusa_ru | pg_stat_statements | 1.2 | 1.4 | OLD
_4vkusa_ru | plpgsql | 1.0 | 1.0 | <no value>
dba | pg_stat_statements | 1.4 | 1.4 | <no value>
dba | plpgsql | 1.0 | 1.0 | <no value>
demotivator_prikolisti_com | pg_repack | 1.4.1 | 1.4.4 | OLD
demotivator_prikolisti_com | pg_stat_statements | 1.2 | 1.4 | OLD
demotivator_prikolisti_com | plpgsql | 1.0 | 1.0 | <no value>
fffail_ru | pg_repack | 1.4.1 | 1.4.4 | OLD
fffail_ru | pg_stat_statements | 1.2 | 1.4 | OLD
fffail_ru | plpgsql | 1.0 | 1.0 | <no value>
foto_prikolisti_com | pg_repack | 1.4.1 | 1.4.4 | OLD
foto_prikolisti_com | pg_stat_statements | 1.2 | 1.4 | OLD
foto_prikolisti_com | plpgsql | 1.0 | 1.0 | <no value>
fotominuta_ru | plpgsql | 1.0 | 1.0 | <no value>
kotomail_ru | pg_repack | 1.4.1 | 1.4.4 | OLD
kotomail_ru | pg_stat_statements | 1.2 | 1.4 | OLD
kotomail_ru | plpgsql | 1.0 | 1.0 | <no value>
nevada | pg_repack | 1.4.2 | 1.4.4 | OLD
nevada | pg_stat_statements | 1.4 | 1.4 | <no value>
nevada | plpgsql | 1.0 | 1.0 | <no value>
nevada1 | pg_stat_statements | 1.2 | 1.4 | OLD
nevada1 | plpgsql | 1.0 | 1.0 | <no value>
nevada2 | pg_repack | 1.4.1 | 1.4.4 | OLD
nevada2 | pg_stat_statements | 1.2 | 1.4 | OLD
nevada2 | plpgsql | 1.0 | 1.0 | <no value>
nevada3 | pg_repack | 1.4.1 | 1.4.4 | OLD
nevada3 | pg_stat_statements | 1.2 | 1.4 | OLD
nevada3 | plpgsql | 1.0 | 1.0 | <no value>
nevada4 | pg_repack | 1.4.1 | 1.4.4 | OLD
nevada4 | pg_stat_statements | 1.2 | 1.4 | OLD
nevada4 | plpgsql | 1.0 | 1.0 | <no value>
nevada5 | pg_repack | 1.4.1 | 1.4.4 | OLD
nevada5 | pg_stat_statements | 1.2 | 1.4 | OLD
nevada5 | plpgsql | 1.0 | 1.0 | <no value>
nevada6 | pg_repack | 1.4.1 | 1.4.4 | OLD
nevada6 | pg_stat_statements | 1.2 | 1.4 | OLD
nevada6 | plpgsql | 1.0 | 1.0 | <no value>
nevada7 | pg_stat_statements | 1.2 | 1.4 | OLD
nevada7 | plpgsql | 1.0 | 1.0 | <no value>
nevada8 | pg_repack | 1.4.1 | 1.4.4 | OLD
nevada8 | pg_stat_statements | 1.2 | 1.4 | OLD
nevada8 | plpgsql | 1.0 | 1.0 | <no value>
nombox_co | pg_stat_statements | 1.2 | 1.4 | OLD
nombox_co | plpgsql | 1.0 | 1.0 | <no value>
postfix | plpgsql | 1.0 | 1.0 | <no value>
postgresmen_ru | plpgsql | 1.0 | 1.0 | <no value>
postila_ru | btree_gin | 1.0 | 1.0 | <no value>
postila_ru | dblink | 1.1 | 1.2 | OLD
postila_ru | file_fdw | 1.0 | 1.0 | <no value>
postila_ru | hstore | 1.4 | 1.4 | <no value>
postila_ru | pageinspect | 1.5 | 1.5 | <no value>
postila_ru | pg_buffercache | 1.0 | 1.2 | OLD
postila_ru | pg_prewarm | 1.1 | 1.1 | <no value>
postila_ru | pg_repack | 1.4.4 | 1.4.4 | <no value>
postila_ru | pg_stat_kcache | 2.1.1 | 2.1.1 | <no value>
postila_ru | pg_stat_statements | 1.4 | 1.4 | <no value>
postila_ru | pgcrypto | 1.1 | 1.3 | OLD
postila_ru | pgstattuple | 1.2 | 1.4 | OLD
postila_ru | plpgsql | 1.0 | 1.0 | <no value>
postila_ru | plpython2u | 1.0 | 1.0 | <no value>
postila_ru | plsh | 2 | 2 | <no value>
postila_ru | postgres_fdw | 1.0 | 1.0 | <no value>
postila_ru | rum | 1.2 | 1.3 | OLD
postila_ru | tablefunc | 1.0 | 1.0 | <no value>
postila_ru | uuid-ossp | 1.0 | 1.1 | OLD
prikolisti_com | pg_stat_statements | 1.2 | 1.4 | OLD
prikolisti_com | plpgsql | 1.0 | 1.0 | <no value>
redash | plpgsql | 1.0 | 1.0 | <no value>
startupturbo_com | pg_stat_statements | 1.2 | 1.4 | OLD
startupturbo_com | plpgsql | 1.0 | 1.0 | <no value>
startupturbo_com_old | plpgsql | 1.0 | 1.0 | <no value>
strakhovm_ru | plpgsql | 1.0 | 1.0 | <no value>
svoi_simplead_ru | plpgsql | 1.0 | 1.0 | <no value>
test | pg_repack | 1.4.4 | 1.4.4 | <no value>
test | pg_stat_statements | 1.4 | 1.4 | <no value>
test | pgstattuple | 1.4 | 1.4 | <no value>
test | plpgsql | 1.0 | 1.0 | <no value>
test2 | plpgsql | 1.0 | 1.0 | <no value>
tks_f | plpgsql | 1.0 | 1.0 | <no value>
tpl_drupal | plpgsql | 1.0 | 1.0 | <no value>
vegas | pg_repack | 1.4.2 | 1.4.4 | OLD
vegas | pg_stat_statements | 1.2 | 1.4 | OLD
vegas | pgstattuple | 1.4 | 1.4 | <no value>
vegas | plpgsql | 1.0 | 1.0 | <no value>
vividcortex | plpgsql | 1.0 | 1.0 | <no value>



## Conclusions ##


## Recommendations ##
