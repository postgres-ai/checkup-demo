# A004 Cluster information #

## Observations ##


### Master (`db2`) ###

 Indicator | Value
-----------|-------
Postgres Version | PostgreSQL&nbsp;9.6.11&nbsp;on&nbsp;x86_64-pc-linux-gnu&nbsp;(Ubuntu&nbsp;9.6.11-1.pgdg16.04+1),&nbsp;compiled&nbsp;by&nbsp;gcc&nbsp;(Ubuntu&nbsp;5.4.0-6ubuntu1~16.04.10)&nbsp;5.4.0&nbsp;20160609,&nbsp;64-bit
Config file | /etc/postgresql/9.6/main/postgresql.conf
Role | Master
Replicas | async/streaming:&nbsp;5.9.142.204
Started At | 2019-01-22&nbsp;22:42:26+00
Uptime | 20&nbsp;days&nbsp;15:55:52
Checkpoints | 20206
Forced Checkpoints | 5.0%
Checkpoint MB/sec | 0.140521
Database Name | postila_ru
Database Size | 653&nbsp;GB
Stats Since | 2018-12-05&nbsp;17:44:47+00
Stats Age | 68&nbsp;days&nbsp;20:53:32
Installed Extensions | btree_gin&nbsp;1.0,&nbsp;dblink&nbsp;1.1,&nbsp;file_fdw&nbsp;1.0,&nbsp;hstore&nbsp;1.4,&nbsp;pageinspect&nbsp;1.5pg_buffercache&nbsp;1.0,&nbsp;pg_prewarm&nbsp;1.1,&nbsp;pg_repack&nbsp;1.4.4,&nbsp;pg_stat_kcache&nbsp;2.1.1,&nbsp;pg_stat_statements&nbsp;1.4pgcrypto&nbsp;1.1,&nbsp;pgstattuple&nbsp;1.2,&nbsp;plpgsql&nbsp;1.0,&nbsp;plpython2u&nbsp;1.0,&nbsp;plsh&nbsp;2postgres_fdw&nbsp;1.0,&nbsp;rum&nbsp;1.2,&nbsp;tablefunc&nbsp;1.0,&nbsp;uuid-ossp&nbsp;1.0
Cache Effectiveness | 94.79%
Successful Commits | 99.97%
Conflicts | 0
Temp Files: total size | 525&nbsp;GB
Temp Files: total number of files | 1197
Temp Files: avg file size | 449&nbsp;MB
Deadlocks | 0

#### Databases sizes ####
Database | &#9660;&nbsp;Size
---------|------
postila_ru | 652.97 GiB
vegas | 85.87 GiB
nevada8 | 12.30 GiB
nevada5 | 12.28 GiB
nevada6 | 12.25 GiB
nevada7 | 12.25 GiB
nevada2 | 12.24 GiB
nevada4 | 12.22 GiB
nevada3 | 12.22 GiB
nevada1 | 12.19 GiB
nevada | 10.25 GiB
foto_prikolisti_com | 9.53 GiB
kotomail_ru | 9.06 GiB
prikolisti_com | 8.85 GiB
demotivator_prikolisti_com | 7.22 GiB
fffail_ru | 5.64 GiB
_4vkusa_ru | 5.57 GiB
postfix | 2.01 GiB
redash | 1.25 GiB
test | 1.16 GiB
fotominuta_ru | 829.22 MiB
startupturbo_com | 707.79 MiB
nombox_co | 438.10 MiB
strakhovm_ru | 281.36 MiB
postgresmen_ru | 134.48 MiB
svoi_simplead_ru | 134.00 MiB
startupturbo_com_old | 118.29 MiB
tks_f | 77.46 MiB
tpl_drupal | 34.15 MiB
postgres | 7.21 MiB
test2 | 7.18 MiB
template1 | 7.18 MiB
dba | 7.18 MiB
vividcortex | 7.18 MiB
template0 | 6.94 MiB

### Replica servers: ###
  
#### Replica (`db3`) ####
    
 Indicator | Value
-----------|-------
Postgres Version | PostgreSQL&nbsp;9.6.11&nbsp;on&nbsp;x86_64-pc-linux-gnu&nbsp;(Ubuntu&nbsp;9.6.11-1.pgdg16.04+1),&nbsp;compiled&nbsp;by&nbsp;gcc&nbsp;(Ubuntu&nbsp;5.4.0-6ubuntu1~16.04.10)&nbsp;5.4.0&nbsp;20160609,&nbsp;64-bit
Config file | /etc/postgresql/9.6/main/postgresql.conf
Role | Replica&nbsp;(delay:&nbsp;00:00:01;&nbsp;paused:&nbsp;false)
Replicas | 
Started At | 2019-01-25&nbsp;21:36:09+00
Uptime | 17&nbsp;days&nbsp;17:01:22
Checkpoints | 18004
Forced Checkpoints | 46.0%
Checkpoint MB/sec | 0.037865
Database Name | postila_ru
Database Size | 653&nbsp;GB
Stats Since | 2019-01-25&nbsp;21:36:14+00
Stats Age | 17&nbsp;days&nbsp;17:01:17
Installed Extensions | btree_gin&nbsp;1.0,&nbsp;dblink&nbsp;1.1,&nbsp;file_fdw&nbsp;1.0,&nbsp;hstore&nbsp;1.4,&nbsp;pageinspect&nbsp;1.5pg_buffercache&nbsp;1.0,&nbsp;pg_prewarm&nbsp;1.1,&nbsp;pg_repack&nbsp;1.4.4,&nbsp;pg_stat_kcache&nbsp;2.1.1,&nbsp;pg_stat_statements&nbsp;1.4pgcrypto&nbsp;1.1,&nbsp;pgstattuple&nbsp;1.2,&nbsp;plpgsql&nbsp;1.0,&nbsp;plpython2u&nbsp;1.0,&nbsp;plsh&nbsp;2postgres_fdw&nbsp;1.0,&nbsp;rum&nbsp;1.2,&nbsp;tablefunc&nbsp;1.0,&nbsp;uuid-ossp&nbsp;1.0
Cache Effectiveness | 96.55%
Successful Commits | 100.00%
Conflicts | 251
Temp Files: total size | 0&nbsp;bytes
Temp Files: total number of files | 0
Temp Files: avg file size | 
Deadlocks | 0


## Conclusions ##


## Recommendations ##

