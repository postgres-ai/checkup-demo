# A008 Disk usage and file system type
Output of `df -TPh` (follows symlinks)

## Observations ##


### Master (`db2`) ###
Name | FS Type | Size | Available | Use | Used | Mount Point | Path | Device
-----|---------|------|-----------|-----|------|-------------|------|-------
PGDATA | ext4 | 5.4T | 4.0T | 1.1T | 22% | /var/lib/postgresql | /var/lib/postgresql/9.6/main | /dev/mapper/cachedev0 
WAL directory | ext4 | 5.4T | 4.0T | 1.1T | 22% | /var/lib/postgresql | /var/lib/postgresql/9.6/main/pg_xlog | /dev/mapper/cachedev0 
stats_temp_directory | tmpfs | 32G | 32G | 3.1M | 1% | /dev/shm | /dev/shm | tmpfs 
log_directory | ext4 | 5.4T | 4.0T | 1.1T | 22% | /var/lib/postgresql | /var/lib/postgresql/9.6/main/pg_log | /dev/mapper/cachedev0 





### Replica servers: ###
  
#### Replica (`db3`) ####
Name | FS Type | Size | Available | Use | Used | Mount Point | Path | Device
-----|---------|------|-----------|-----|------|-------------|------|-------
PGDATA | ext4 | 5.4T | 4.0T | 1.1T | 22% | /var/lib/postgresql | /var/lib/postgresql/9.6/main | /dev/mapper/cachedev0  
WAL directory | ext4 | 5.4T | 4.0T | 1.1T | 22% | /var/lib/postgresql | /var/lib/postgresql/9.6/main/pg_xlog | /dev/mapper/cachedev0  
stats_temp_directory | tmpfs | 32G | 32G | 420K | 1% | /dev/shm | /dev/shm | tmpfs  
log_directory | ext4 | 5.4T | 4.0T | 1.1T | 22% | /var/lib/postgresql | /var/lib/postgresql/9.6/main/pg_log | /dev/mapper/cachedev0  





## Conclusions ##

## Recommendations ##

